<!-- (c) https://github.com/MontiCore/monticore -->
 [![Maintainability](https://api.codeclimate.com/v1/badges/fb2dbd6a81d1295abd97/maintainability)](https://codeclimate.com/github/EmbeddedMontiArc/OCL_EMA2Java/maintainability)
  [![Build Status](https://travis-ci.org/EmbeddedMontiArc/OCL_EMA2Java.svg?branch=master)](https://travis-ci.org/EmbeddedMontiArc/OCL_EMA2Java)
  [![Build Status](https://circleci.com/gh/EmbeddedMontiArc/OCL_EMA2Java/tree/master.svg?style=shield&circle-token=:circle-token)](https://circleci.com/gh/EmbeddedMontiArc/OCL_EMA2Java/tree/master)
[![Coverage Status](https://coveralls.io/repos/github/EmbeddedMontiArc/OCL_EMA2Java/badge.svg?branch=master)](https://coveralls.io/github/EmbeddedMontiArc/OCL_EMA2Java?branch=master)
[![PPTX-Docu](https://img.shields.io/badge/PPTX--Docu-2018--05--22-brightgreen.svg)](https://github.com/EmbeddedMontiArc/Documentation/blob/master/reposlides/18.05.22.Docu.OCL_EMA2Java.pdf)



# OCL_EMA2Java

A Generator generating executable java code from ocl files with specific EMA extensions
