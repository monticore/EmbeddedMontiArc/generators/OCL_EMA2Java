/* (c) https://github.com/MontiCore/monticore */
package conventions;

import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.symboltable.*;

import java.lang.*;

import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_cv2 {
@SuppressWarnings("unchecked")
public static boolean _oclCV2(ComponentSymbol cmp) {
boolean _oclResult = true;
try {
_oclResult &= (Character.isUpperCase(cmp.getName().charAt(0)));
boolean _oclForAll0 = true;
for (Symbol sym : cmp.getFormalTypeParameters()) {
_oclForAll0 &= (Character.isUpperCase(sym.getName().charAt(0)));
if (!_oclForAll0) {
break;
}
}
_oclResult &= (_oclForAll0);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentSymbol cmp : OCLEMAHelper.<ComponentSymbol>getAllSymbolInstances(symtab, ComponentSymbol.KIND)) {
_oclResult &= _oclCV2(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
