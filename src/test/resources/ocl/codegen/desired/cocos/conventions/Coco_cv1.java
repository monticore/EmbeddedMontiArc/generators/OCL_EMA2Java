/* (c) https://github.com/MontiCore/monticore */
package conventions;

import de.monticore.symboltable.*;

import java.lang.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_cv1 {
@SuppressWarnings("unchecked")
public static boolean _oclCV1(ComponentSymbol cmp) {
boolean _oclResult = true;
try {
boolean _oclForAll0 = true;
for (Symbol sym : cmp.getPorts()) {
_oclForAll0 &= (Character.isLowerCase(sym.getName().charAt(0)));
if (!_oclForAll0) {
break;
}
}
_oclResult &= (_oclForAll0);
boolean _oclForAll1 = true;
for (Symbol sym : cmp.getSubComponents()) {
_oclForAll1 &= (Character.isLowerCase(sym.getName().charAt(0)));
if (!_oclForAll1) {
break;
}
}
_oclResult &= (_oclForAll1);
boolean _oclForAll2 = true;
for (Symbol sym : cmp.getConfigParameters()) {
_oclForAll2 &= (Character.isLowerCase(sym.getName().charAt(0)));
if (!_oclForAll2) {
break;
}
}
_oclResult &= (_oclForAll2);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentSymbol cmp : OCLEMAHelper.<ComponentSymbol>getAllSymbolInstances(symtab, ComponentSymbol.KIND)) {
_oclResult &= _oclCV1(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
