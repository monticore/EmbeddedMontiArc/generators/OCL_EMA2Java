/* (c) https://github.com/MontiCore/monticore */
package conventions;

import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.symboltable.*;

import java.lang.*;

import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_cv5 {
@SuppressWarnings("unchecked")
public static boolean _oclCV5(ComponentSymbol cmp) {
boolean _oclResult = true;
try {
boolean _oclPrefix0Lifting = false;
try {
_oclPrefix0Lifting = cmp.isDecomposed();
} catch (Exception _oclPrefix0LiftingException) {
_oclPrefix0Lifting = false;
}
boolean _oclInfix0LeftLifting = false;
try {
_oclInfix0LeftLifting = !_oclPrefix0Lifting;
} catch (Exception _oclInfix0LeftLiftingException) {
_oclInfix0LeftLifting = false;
}
boolean _oclForAll0 = true;
for (PortSymbol port : cmp.getPorts()) {
boolean _oclInfix1LeftLifting = false;
try {
_oclInfix1LeftLifting = cmp.hasConnector(port.getName());
} catch (Exception _oclInfix1LeftLiftingException) {
_oclInfix1LeftLifting = false;
}
boolean _oclInfix1RightLifting = false;
try {
_oclInfix1RightLifting = cmp.hasConnectors(port.getName());
} catch (Exception _oclInfix1RightLiftingException) {
_oclInfix1RightLifting = false;
}
_oclForAll0 &= (_oclInfix1LeftLifting || _oclInfix1RightLifting);
if (!_oclForAll0) {
break;
}
}
boolean _oclInfix0RightLifting = false;
try {
_oclInfix0RightLifting = (_oclForAll0);
} catch (Exception _oclInfix0RightLiftingException) {
_oclInfix0RightLifting = false;
}
_oclResult &= ((_oclInfix0LeftLifting || _oclInfix0RightLifting));
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentSymbol cmp : OCLEMAHelper.<ComponentSymbol>getAllSymbolInstances(symtab, ComponentSymbol.KIND)) {
_oclResult &= _oclCV5(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
