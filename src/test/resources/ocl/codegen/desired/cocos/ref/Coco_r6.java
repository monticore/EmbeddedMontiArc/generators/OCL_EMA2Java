/* (c) https://github.com/MontiCore/monticore */
package ref;

import de.monticore.symboltable.*;

import java.lang.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_r6 {
@SuppressWarnings("unchecked")
public static boolean _oclR6(ConnectorSymbol conn) {
boolean _oclResult = true;
try {
boolean _oclInfix0LeftLifting = false;
try {
_oclInfix0LeftLifting = conn.getSourcePort() != null;
} catch (Exception _oclInfix0LeftLiftingException) {
_oclInfix0LeftLifting = false;
}
boolean _oclInfix0RightLifting = false;
try {
_oclInfix0RightLifting = conn.getTargetPort() != null;
} catch (Exception _oclInfix0RightLiftingException) {
_oclInfix0RightLifting = false;
}
_oclResult &= (_oclInfix0LeftLifting && _oclInfix0RightLifting);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ConnectorSymbol conn : OCLEMAHelper.<ConnectorSymbol>getAllSymbolInstances(symtab, ConnectorSymbol.KIND)) {
_oclResult &= _oclR6(conn);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
