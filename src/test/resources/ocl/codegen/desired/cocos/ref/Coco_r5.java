/* (c) https://github.com/MontiCore/monticore */
package ref;

import static de.se_rwth.commons.Splitters.DOT;
import de.monticore.symboltable.*;

import java.lang.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_r5 {
@SuppressWarnings("unchecked")
public static boolean _oclR5(ComponentSymbol cmp) {
boolean _oclResult = true;
try {
boolean _oclForAll0 = true;
for (ConnectorSymbol conn : cmp.getConnectors()) {
boolean _oclPrefix0Lifting = false;
try {
_oclPrefix0Lifting = DOT.splitToList(conn.getSource()).size() == 2;
} catch (Exception _oclPrefix0LiftingException) {
_oclPrefix0Lifting = false;
}
boolean _oclInfix0LeftLifting = false;
try {
_oclInfix0LeftLifting = !_oclPrefix0Lifting;
} catch (Exception _oclInfix0LeftLiftingException) {
_oclInfix0LeftLifting = false;
}
boolean _oclInfix0RightLifting = false;
try {
_oclInfix0RightLifting = cmp.getSubComponent(DOT.splitToList(conn.getSource()).get(0)).isPresent();
} catch (Exception _oclInfix0RightLiftingException) {
_oclInfix0RightLifting = false;
}
boolean _oclInfix1LeftLifting = false;
try {
_oclInfix1LeftLifting = (_oclInfix0LeftLifting || _oclInfix0RightLifting);
} catch (Exception _oclInfix1LeftLiftingException) {
_oclInfix1LeftLifting = false;
}
boolean _oclPrefix1Lifting = false;
try {
_oclPrefix1Lifting = DOT.splitToList(conn.getTarget()).size() == 2;
} catch (Exception _oclPrefix1LiftingException) {
_oclPrefix1Lifting = false;
}
boolean _oclInfix2LeftLifting = false;
try {
_oclInfix2LeftLifting = !_oclPrefix1Lifting;
} catch (Exception _oclInfix2LeftLiftingException) {
_oclInfix2LeftLifting = false;
}
boolean _oclInfix2RightLifting = false;
try {
_oclInfix2RightLifting = cmp.getSubComponent(DOT.splitToList(conn.getTarget()).get(0)).isPresent();
} catch (Exception _oclInfix2RightLiftingException) {
_oclInfix2RightLifting = false;
}
boolean _oclInfix1RightLifting = false;
try {
_oclInfix1RightLifting = (_oclInfix2LeftLifting || _oclInfix2RightLifting);
} catch (Exception _oclInfix1RightLiftingException) {
_oclInfix1RightLifting = false;
}
_oclForAll0 &= (_oclInfix1LeftLifting && _oclInfix1RightLifting);
if (!_oclForAll0) {
break;
}
}
_oclResult &= (_oclForAll0);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentSymbol cmp : OCLEMAHelper.<ComponentSymbol>getAllSymbolInstances(symtab, ComponentSymbol.KIND)) {
_oclResult &= _oclR5(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
