/* (c) https://github.com/MontiCore/monticore */
package ref;

import de.monticore.symboltable.*;

import java.lang.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_r7 {
@SuppressWarnings("unchecked")
public static boolean _oclR7(ComponentInstanceSymbol cmp) {
boolean _oclResult = true;
try {
boolean _oclForAll0 = true;
for (ConnectorSymbol conn : cmp.getSimpleConnectors()) {
boolean _oclExists0 = false;
for (PortSymbol port : cmp.getComponentType().getReferencedSymbol().getOutgoingPorts()) {
_oclExists0 |= (conn.getSource().equals(port.getName()));
if (_oclExists0) {
break;
}
}
_oclForAll0 &= (_oclExists0);
if (!_oclForAll0) {
break;
}
}
_oclResult &= (_oclForAll0);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentInstanceSymbol cmp : OCLEMAHelper.<ComponentInstanceSymbol>getAllSymbolInstances(symtab, ComponentInstanceSymbol.KIND)) {
_oclResult &= _oclR7(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
