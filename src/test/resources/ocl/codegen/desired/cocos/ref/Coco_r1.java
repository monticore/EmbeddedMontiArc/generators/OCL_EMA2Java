/* (c) https://github.com/MontiCore/monticore */
package ref;

import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.symboltable.*;

import java.util.*;
import java.lang.*;

import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_r1 {
@SuppressWarnings("unchecked")
public static boolean _oclR1(ComponentSymbol cmp) {
boolean _oclResult = true;
try {
boolean _oclLetIn0 = false;
{
List<ConnectorSymbol> connectors = OCLEMAHelper.getAllConnectors(cmp);
boolean _oclForAll0 = true;
for (PortSymbol port : cmp.getOutgoingPorts()) {
boolean _oclForAll1 = true;
for (ConnectorSymbol conn1 : connectors) {
boolean _oclForAll2 = true;
for (ConnectorSymbol conn2 : connectors) {
boolean _oclInfix0LeftLifting = false;
try {
_oclInfix0LeftLifting = conn1.getName().equals(port.getName());
} catch (Exception _oclInfix0LeftLiftingException) {
_oclInfix0LeftLifting = false;
}
boolean _oclInfix0RightLifting = false;
try {
_oclInfix0RightLifting = conn2.getName().equals(port.getName());
} catch (Exception _oclInfix0RightLiftingException) {
_oclInfix0RightLifting = false;
}
boolean _oclPrefix0Lifting = false;
try {
_oclPrefix0Lifting = (_oclInfix0LeftLifting && _oclInfix0RightLifting);
} catch (Exception _oclPrefix0LiftingException) {
_oclPrefix0Lifting = false;
}
boolean _oclInfix1LeftLifting = false;
try {
_oclInfix1LeftLifting = !_oclPrefix0Lifting;
} catch (Exception _oclInfix1LeftLiftingException) {
_oclInfix1LeftLifting = false;
}
boolean _oclInfix1RightLifting = false;
try {
_oclInfix1RightLifting = conn1 == conn2;
} catch (Exception _oclInfix1RightLiftingException) {
_oclInfix1RightLifting = false;
}
_oclForAll2 &= ((_oclInfix1LeftLifting || _oclInfix1RightLifting));
if (!_oclForAll2) {
break;
}
}
_oclForAll1 &= (_oclForAll2);
if (!_oclForAll1) {
break;
}
}
_oclForAll0 &= (_oclForAll1);
if (!_oclForAll0) {
break;
}
}
_oclLetIn0 = _oclForAll0;
}
_oclResult &= (_oclLetIn0);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentSymbol cmp : OCLEMAHelper.<ComponentSymbol>getAllSymbolInstances(symtab, ComponentSymbol.KIND)) {
_oclResult &= _oclR1(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
