/* (c) https://github.com/MontiCore/monticore */
package ref;

import de.monticore.symboltable.*;

import java.lang.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_r10 {
@SuppressWarnings("unchecked")
public static boolean _oclR10(ComponentInstanceSymbol cmp) {
boolean _oclResult = true;
try {
_oclResult &= (cmp.getConfigArguments().size() == cmp.getComponentType().getReferencedSymbol().getConfigParameters().size());
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentInstanceSymbol cmp : OCLEMAHelper.<ComponentInstanceSymbol>getAllSymbolInstances(symtab, ComponentInstanceSymbol.KIND)) {
_oclResult &= _oclR10(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
