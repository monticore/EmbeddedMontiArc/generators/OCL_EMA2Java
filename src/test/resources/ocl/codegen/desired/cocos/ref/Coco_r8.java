/* (c) https://github.com/MontiCore/monticore */
package ref;

import de.monticore.symboltable.*;
import de.monticore.symboltable.types.*;
import de.monticore.symboltable.types.references.*;

import java.lang.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_r8 {
    @SuppressWarnings("unchecked")
    public static boolean _oclR8(ConnectorSymbol conn) {
        boolean _oclResult = true;
        try {
            boolean _oclLetIn0 = false;
            {
                JTypeSymbol sourceType = conn.getSourcePort().getTypeReference().getReferencedSymbol();
                JTypeSymbol targetType = conn.getTargetPort().getTypeReference().getReferencedSymbol();
                boolean _oclInfix0LeftLifting = false;
                try {
                    _oclInfix0LeftLifting = sourceType == targetType;
                } catch (Exception _oclInfix0LeftLiftingException) {
                    _oclInfix0LeftLifting = false;
                }
                boolean _oclExists0 = false;
                for (JTypeReference ref : sourceType.getSuperTypes()) {
                    _oclExists0 |= (ref.getReferencedSymbol() == targetType);
                    if (_oclExists0) {
                        break;
                    }
                }
                boolean _oclInfix0RightLifting = false;
                try {
                    _oclInfix0RightLifting = (_oclExists0);
                } catch (Exception _oclInfix0RightLiftingException) {
                    _oclInfix0RightLifting = false;
                }
                _oclLetIn0 = _oclInfix0LeftLifting || _oclInfix0RightLifting;
            }
            _oclResult &= (_oclLetIn0);
        } catch (Exception _oclResultLiftingException) {
            _oclResult = false;
        }
        return _oclResult;
    }
    public static boolean check(GlobalScope symtab) {
        boolean _oclResult = true;
        try {
            for (ConnectorSymbol conn : OCLEMAHelper.<ConnectorSymbol>getAllSymbolInstances(symtab, ConnectorSymbol.KIND)) {
                _oclResult &= _oclR8(conn);
            }
        } catch (Exception _oclResultLiftingException) {
            _oclResult = false;
        }
        return _oclResult;
    }
}
