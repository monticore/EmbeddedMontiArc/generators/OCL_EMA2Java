/* (c) https://github.com/MontiCore/monticore */
package general;

import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.symboltable.*;

import java.util.*;
import java.lang.*;

import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_b1 {
    @SuppressWarnings("unchecked")
    public static boolean _oclB1(ComponentSymbol cmp) {
        boolean _oclResult = true;
        try {
            boolean _oclLetIn0 = false;
            {
                List<Symbol> symbols = OCLEMAHelper.getSymbols(cmp);
                boolean _oclForAll0 = true;
                for (Symbol sym1 : symbols) {
                    boolean _oclForAll1 = true;
                    for (Symbol sym2 : symbols) {
                        boolean _oclPrefix0Lifting = false;
                        try {
                            _oclPrefix0Lifting = sym1 != sym2;
                        } catch (Exception _oclPrefix0LiftingException) {
                            _oclPrefix0Lifting = false;
                        }
                        boolean _oclInfix0LeftLifting = false;
                        try {
                            _oclInfix0LeftLifting = !_oclPrefix0Lifting;
                        } catch (Exception _oclInfix0LeftLiftingException) {
                            _oclInfix0LeftLifting = false;
                        }
                        boolean _oclPrefix1Lifting = false;
                        try {
                            _oclPrefix1Lifting = sym1.getName().equals(sym2.getName());
                        } catch (Exception _oclPrefix1LiftingException) {
                            _oclPrefix1Lifting = false;
                        }
                        boolean _oclInfix0RightLifting = false;
                        try {
                            _oclInfix0RightLifting = !_oclPrefix1Lifting;
                        } catch (Exception _oclInfix0RightLiftingException) {
                            _oclInfix0RightLifting = false;
                        }
                        _oclForAll1 &= ((_oclInfix0LeftLifting || _oclInfix0RightLifting));
                        if (!_oclForAll1) {
                            break;
                        }
                    }
                    _oclForAll0 &= (_oclForAll1);
                    if (!_oclForAll0) {
                        break;
                    }
                }
                _oclLetIn0 = _oclForAll0;
            }
            _oclResult &= (_oclLetIn0);
        } catch (Exception _oclResultLiftingException) {
            _oclResult = false;
        }
        return _oclResult;
    }
    public static boolean check(GlobalScope symtab) {
        boolean _oclResult = true;
        try {
            for (ComponentSymbol cmp : OCLEMAHelper.<ComponentSymbol>getAllSymbolInstances(symtab, ComponentSymbol.KIND)) {
                _oclResult &= _oclB1(cmp);
            }
        } catch (Exception _oclResultLiftingException) {
            _oclResult = false;
        }
        return _oclResult;
    }
}
