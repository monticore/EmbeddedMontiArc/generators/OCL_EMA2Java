/* (c) https://github.com/MontiCore/monticore */
package connections;

import static de.se_rwth.commons.Splitters.DOT;
import de.monticore.symboltable.*;

import java.lang.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_co3 {
@SuppressWarnings("unchecked")
public static boolean _oclCO3(ComponentSymbol cmp) {
boolean _oclResult = true;
try {
boolean _oclForAll0 = true;
for (ConnectorSymbol conn : cmp.getConnectors()) {
boolean _oclPrefix0Lifting = false;
try {
_oclPrefix0Lifting = DOT.splitToList(conn.getSource()).size() == 1;
} catch (Exception _oclPrefix0LiftingException) {
_oclPrefix0Lifting = false;
}
boolean _oclInfix0LeftLifting = false;
try {
_oclInfix0LeftLifting = !_oclPrefix0Lifting;
} catch (Exception _oclInfix0LeftLiftingException) {
_oclInfix0LeftLifting = false;
}
boolean _oclInfix1LeftLifting = false;
try {
_oclInfix1LeftLifting = cmp.getIncomingPort(conn.getSource()).isPresent();
} catch (Exception _oclInfix1LeftLiftingException) {
_oclInfix1LeftLifting = false;
}
boolean _oclInfix1RightLifting = false;
try {
_oclInfix1RightLifting = cmp.getOutgoingPort(conn.getSource()).isPresent();
} catch (Exception _oclInfix1RightLiftingException) {
_oclInfix1RightLifting = false;
}
boolean _oclInfix2LeftLifting = false;
try {
_oclInfix2LeftLifting = _oclInfix1LeftLifting || _oclInfix1RightLifting;
} catch (Exception _oclInfix2LeftLiftingException) {
_oclInfix2LeftLifting = false;
}
boolean _oclInfix2RightLifting = false;
try {
_oclInfix2RightLifting = cmp.getSubComponent(conn.getSource()).isPresent();
} catch (Exception _oclInfix2RightLiftingException) {
_oclInfix2RightLifting = false;
}
boolean _oclInfix0RightLifting = false;
try {
_oclInfix0RightLifting = _oclInfix2LeftLifting || _oclInfix2RightLifting;
} catch (Exception _oclInfix0RightLiftingException) {
_oclInfix0RightLifting = false;
}
boolean _oclInfix3LeftLifting = false;
try {
_oclInfix3LeftLifting = (_oclInfix0LeftLifting || _oclInfix0RightLifting);
} catch (Exception _oclInfix3LeftLiftingException) {
_oclInfix3LeftLifting = false;
}
boolean _oclPrefix1Lifting = false;
try {
_oclPrefix1Lifting = DOT.splitToList(conn.getTarget()).size() == 1;
} catch (Exception _oclPrefix1LiftingException) {
_oclPrefix1Lifting = false;
}
boolean _oclInfix4LeftLifting = false;
try {
_oclInfix4LeftLifting = !_oclPrefix1Lifting;
} catch (Exception _oclInfix4LeftLiftingException) {
_oclInfix4LeftLifting = false;
}
boolean _oclInfix5LeftLifting = false;
try {
_oclInfix5LeftLifting = cmp.getIncomingPort(conn.getTarget()).isPresent();
} catch (Exception _oclInfix5LeftLiftingException) {
_oclInfix5LeftLifting = false;
}
boolean _oclInfix5RightLifting = false;
try {
_oclInfix5RightLifting = cmp.getOutgoingPort(conn.getTarget()).isPresent();
} catch (Exception _oclInfix5RightLiftingException) {
_oclInfix5RightLifting = false;
}
boolean _oclInfix6LeftLifting = false;
try {
_oclInfix6LeftLifting = _oclInfix5LeftLifting || _oclInfix5RightLifting;
} catch (Exception _oclInfix6LeftLiftingException) {
_oclInfix6LeftLifting = false;
}
boolean _oclInfix6RightLifting = false;
try {
_oclInfix6RightLifting = cmp.getSubComponent(conn.getTarget()).isPresent();
} catch (Exception _oclInfix6RightLiftingException) {
_oclInfix6RightLifting = false;
}
boolean _oclInfix4RightLifting = false;
try {
_oclInfix4RightLifting = _oclInfix6LeftLifting || _oclInfix6RightLifting;
} catch (Exception _oclInfix4RightLiftingException) {
_oclInfix4RightLifting = false;
}
boolean _oclInfix3RightLifting = false;
try {
_oclInfix3RightLifting = (_oclInfix4LeftLifting || _oclInfix4RightLifting);
} catch (Exception _oclInfix3RightLiftingException) {
_oclInfix3RightLifting = false;
}
_oclForAll0 &= (_oclInfix3LeftLifting && _oclInfix3RightLifting);
if (!_oclForAll0) {
break;
}
}
_oclResult &= (_oclForAll0);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentSymbol cmp : OCLEMAHelper.<ComponentSymbol>getAllSymbolInstances(symtab, ComponentSymbol.KIND)) {
_oclResult &= _oclCO3(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
