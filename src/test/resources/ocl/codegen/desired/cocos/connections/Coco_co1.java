/* (c) https://github.com/MontiCore/monticore */
package connections;

import static de.se_rwth.commons.Splitters.DOT;

import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.symboltable.*;

import java.lang.*;

import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_co1 {
@SuppressWarnings("unchecked")
public static boolean _oclCO1(ConnectorSymbol conn) {
boolean _oclResult = true;
try {
_oclResult &= (DOT.splitToList(conn.getSource()).size() < 3);
_oclResult &= (DOT.splitToList(conn.getTarget()).size() < 3);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ConnectorSymbol conn : OCLEMAHelper.<ConnectorSymbol>getAllSymbolInstances(symtab, ConnectorSymbol.KIND)) {
_oclResult &= _oclCO1(conn);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
