/* (c) https://github.com/MontiCore/monticore */
package connections;

import static de.se_rwth.commons.Splitters.DOT;
import de.monticore.symboltable.*;

import java.lang.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.lang.montiarc.montiarc._symboltable.*;

public class Coco_co2 {
@SuppressWarnings("unchecked")
public static boolean _oclCO2(ComponentInstanceSymbol cmp) {
boolean _oclResult = true;
try {
boolean _oclForAll0 = true;
for (ConnectorSymbol conn : cmp.getSimpleConnectors()) {
_oclForAll0 &= (DOT.splitToList(conn.getSource()).size() == 1);
if (!_oclForAll0) {
break;
}
}
_oclResult &= (_oclForAll0);
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
public static boolean check(GlobalScope symtab) {
boolean _oclResult = true;
try {
for (ComponentInstanceSymbol cmp : OCLEMAHelper.<ComponentInstanceSymbol>getAllSymbolInstances(symtab, ComponentInstanceSymbol.KIND)) {
_oclResult &= _oclCO2(cmp);
}
} catch (Exception _oclResultLiftingException) {
_oclResult = false;
}
return _oclResult;
}
}
