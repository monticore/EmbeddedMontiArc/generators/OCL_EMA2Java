/* (c) https://github.com/MontiCore/monticore */
package example;

conforms to nfp.TagPowerTagSchema;

tags TagPower for negative {

    tag sensors with PowerCmpInst= 2 W;
	tag sensors.control with PowerCmpInst = 2 W;
	tag sensors.gps1 with PowerCmpInst = 50 mW;
	tag sensors.gps2 with PowerCmpInst = 50 mW;
	tag sensors.temp1 with PowerCmpInst = 50 mW;
	tag sensors.humid1 with PowerCmpInst = 50 mW;

    tag Sensors with PowerCmp = 1 W;
    tag Sensors.Controller with PowerCmp = 50 mW;
    tag Sensors.GPS with PowerCmp = 1 W;
    tag Sensors.Temperature with PowerCmp = 100 mW;
    tag Sensors.Humidity with PowerCmp = 100 mW;

}
