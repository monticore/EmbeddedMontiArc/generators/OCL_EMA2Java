/* (c) https://github.com/MontiCore/monticore */
package example;

conforms to nfp.TagLatencyTagSchema;

tags TagLatency for negative {

    tag sensors with LatencyCmpInst = 1 s;
	tag sensors.control with LatencyCmpInst = 50 ms;
	tag sensors.gps1 with LatencyCmpInst = 950 ms;
	tag sensors.gps2 with LatencyCmpInst = 750 ms;
	tag sensors.temp1 with LatencyCmpInst = 55 ms;
	tag sensors.humid1 with LatencyCmpInst = 50 ms;

    tag Sensors with LatencyCmp = 5 s;
    tag Sensors.Controller with LatencyCmp = 50 ms;
    tag Sensors.GPS with LatencyCmp = 1 s;
    tag Sensors.Temperature with LatencyCmp = 100 ms;
    tag Sensors.Humidity with LatencyCmp = 100 ms;

}
