/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.montiarc;

import com.google.common.collect.Sets;
import de.monticore.io.paths.IterablePath;
import de.monticore.lang.ocl.LogConfig;
import de.monticore.lang.ocl.codegen.CheckEmbeddedMontiArc;
import de.monticore.symboltable.GlobalScope;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;

public class CheckEmbeddedMontiArcTest {
	
	protected final String MA_TEST_DIR = "src/test/resources/ma/";
	protected final String OCL_TEST_DIR = "src/test/resources/ocl/codegen/input/cocos/";


	@BeforeClass
	public static void init() {
		LogConfig.init();
	}

	private void test(String component, String oclModel, boolean compare) {
		Path maPath = Paths.get(MA_TEST_DIR);
		Path oclPath = Paths.get(OCL_TEST_DIR);
		Path oclModelPath = Paths.get(oclModel);
		GlobalScope scope = CheckEmbeddedMontiArc.createSymTab(maPath, component);
		
		// delete existing generated files
		Iterator<Path> delIt = IterablePath.fromPaths(Arrays.asList(oclPath), Sets.newLinkedHashSet(Arrays.asList("class", "java"))).getResolvedPaths();
		while(delIt.hasNext()) {
			Path delPath = delIt.next();
			delPath.toFile().delete();
		}
		
		boolean res = CheckEmbeddedMontiArc.checkMontiArcUsingOCL(scope, OCL_TEST_DIR, oclModel);
		assertEquals(compare, res);
	}


	@Ignore
	@Test
	public void testCO1() {
		test("validModels.connections.CO1", "connections/Coco_co1", true);
		test("invalidModels.connections.CO1", "connections/Coco_co1", false);
	}

	@Ignore
	@Test
	public void testCO2() {
		test("validModels.connections.CO2", "connections/Coco_co2", true);
		test("invalidModels.connections.CO2", "connections/Coco_co2", false);
	}

	@Ignore
	@Test
	public void testCO3() {
		test("validModels.connections.CO3", "connections/Coco_co3", true);
		test("invalidModels.connections.CO3", "connections/Coco_co3", false);
	}

	@Ignore
	@Test
	public void testCV1() {
		test("validModels.conventions.CV1", "conventions/Coco_cv1", true);
		test("invalidModels.conventions.CV1", "conventions/Coco_cv1", false);
	}

	@Ignore
	@Test
	public void testCV2() {
		test("validModels.conventions.CV2", "conventions/Coco_cv2", true);
		test("invalidModels.conventions.CV2", "conventions/Coco_cv2", false);
	}

	@Ignore
	@Test
	public void testCV3() {
		test("validModels.conventions.CV3", "conventions/Coco_cv3", true);
		test("invalidModels.conventions.CV3", "conventions/Coco_cv3", false);
	}

	@Ignore
	@Test
	public void testCV5() {
		test("validModels.conventions.CV5", "conventions/Coco_cv5", true);
		test("invalidModels.conventions.CV5", "conventions/Coco_cv5", false);
	}

	@Ignore
	@Test
	public void testB1() {
		test("validModels.general.B1", "general/Coco_b1", true);
		test("invalidModels.general.B1", "general/Coco_b1", false);
	}

	@Ignore
	@Test
	public void testR1() {
		test("validModels.ref.R1", "ref/Coco_r1", true);
		test("invalidModels.ref.R1", "ref/Coco_r1", false);
	}

	@Ignore
	@Test
	public void testR2() {
		test("validModels.ref.R2", "ref/Coco_r2", true);
		test("invalidModels.ref.R2", "ref/Coco_r2", false);
	}

	@Ignore
	@Test
	public void testR5() {
		test("validModels.ref.R5", "ref/Coco_r5", true);
		test("invalidModels.ref.R5", "ref/Coco_r5", false);
	}

	@Ignore
	@Test
	public void testR6() {
		test("validModels.ref.R6", "ref/Coco_r6", true);
		test("invalidModels.ref.R6", "ref/Coco_r6", false);
	}

	@Ignore
	@Test
	public void testR7() {
		test("validModels.ref.R7", "ref/Coco_r7", true);
		test("invalidModels.ref.R7", "ref/Coco_r7", false);
	}

	@Ignore
	@Test
	public void testR8() {
		//test("validModels.ref.R8", "ref/Coco_r8", true);
		test("invalidModels.ref.R8", "ref/Coco_r8", false);
	}

	@Ignore
	@Test
	public void testR9() {
		// invalid model causes an error in the parser. checking with an ocl constraint is redundant?
//		test("validModels.ref.R9", "ref/Coco_r9", true);
//		test("invalidModels.ref.R9", "ref/Coco_r9", false);
	}

	@Ignore
	@Test
	public void testR10() {
		test("validModels.ref.R10", "ref/Coco_r10", true);
		test("invalidModels.ref.R10", "ref/Coco_r10", false);
	}
}
