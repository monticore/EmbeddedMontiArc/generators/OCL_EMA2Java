/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.ocl.LogConfig;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;


/**
 * This tests the verification tool with consistency constraints
 */
public class VerifyToolTest {

    String workingDir = Paths.get(".").toAbsolutePath().normalize().toString();
    String parentDir = workingDir + "/src/test/resources/nfp/";
    String target = workingDir + "/target/gen/";
    String modelCmp = "model.weatherBalloon.WeatherBalloonSensors";

    @BeforeClass
    public static void init() {
        LogConfig.init();
    }

    @Test
    public void verifytool() {
        String[] args = new String[4];
        args[0] = parentDir;
        args[1] = modelCmp;
        args[2] = "tagDef.consistency.rule1_InstPower";
        args[3] = target;

        EMAOCLVerifyTool.main(args);
    }

    @Test
    public void VerifactorCallTest() {
        EMANFPVerificator verificator = new EMANFPVerificator();
        Boolean res = verificator.verificateNFP(Paths.get(parentDir), modelCmp, "tagDef.consistency.rule1_InstPower", Paths.get(target));
        HashMap<String, EMANFPWitness> witnesses = verificator.getWitnessMap();
    }

    @Test
    public void rule1_InstPower() {
        String modelCmp = "model.fas.DaimlerPublicDemonstrator_ADAS_v04";
        String modelCmp2 = "model.fas.DEMO_FAS";
        String modelCmp3 = "model.weatherBalloon.WeatherBalloonSensors";
        String ocl = "tagDef.consistency.rule1_InstPower";
        String[] args = new String[4];
        args[0] = parentDir;
        args[1] = modelCmp3;
        args[2] = ocl;
        args[3] = target;

        EMAOCLVerifyTool.main(args);
    }

    @Test
    public void rule2_CompPower() {
        String modelCmp = "model.fas.DaimlerPublicDemonstrator_ADAS_v04";
        String modelCmp2 = "model.fas.DEMO_FAS";
        String modelCmp3 = "model.weatherBalloon.WeatherBalloonSensors";
        String ocl = "tagDef.consistency.rule2_CompPower";
        String[] args = new String[4];
        args[0] = parentDir;
        args[1] = modelCmp3;
        args[2] = ocl;
        args[3] = target;

        EMAOCLVerifyTool.main(args);
    }


    @Test
    public void rule3_WCET_SingleCore() {
        String modelCmp = "model.fas.DaimlerPublicDemonstrator_ADAS_v04";
        String modelCmp2 = "model.fas.DEMO_FAS";
        String modelCmp3 = "model.weatherBalloon.WeatherBalloonSensors";
        String ocl = "tagDef.consistency.rule3_WCET_SingleCore";
        String[] args = new String[4];
        args[0] = parentDir;
        args[1] = modelCmp3;
        args[2] = ocl;
        args[3] = target;

        EMAOCLVerifyTool.main(args);
    }

    @Test
    public void rule4_WCET_MultiCore() {
        String modelCmp = "model.fas.DaimlerPublicDemonstrator_ADAS_v04";
        String modelCmp2 = "model.fas.DEMO_FAS";
        String modelCmp3 = "model.weatherBalloon.WeatherBalloonSensors";
        String ocl = "tagDef.consistency.rule4_WCET_MultiCore";
        String[] args = new String[4];
        args[0] = parentDir;
        args[1] = modelCmp3;
        args[2] = ocl;
        args[3] = target;

        EMAOCLVerifyTool.main(args);
    }

    @Test
    public void WB_test() {
        GlobalScope symtab = EMANFPVerificator.createModelSymTab(Paths.get(parentDir), "model.weatherBalloon.WeatherBalloonSensors");
        Optional<ComponentSymbol> cmp = symtab.<ComponentSymbol>resolve("model.weatherBalloon.WeatherBalloonSensors.Temperature", ComponentSymbol.KIND);
        Optional<ExpandedComponentInstanceSymbol> cmp2 = symtab.<ExpandedComponentInstanceSymbol>resolve("model.weatherBalloon.weatherBalloonSensors.gps1", ExpandedComponentInstanceSymbol.KIND);
        //  System.out.println(coll);
        System.out.println(cmp.get().getTags());
        System.out.println(cmp2.get().getTags());
    }

    @Test
    public void portsHaveSameType() {
        String modelCmp = "model.fas.DaimlerPublicDemonstrator_ADAS_v04";
        String modelCmp2 = "model.fas.DEMO_FAS";
        String modelCmp3 = "model.weatherBalloon.WeatherBalloonSensors";
        String ocl = "tagDef.consistency.connectedPortsHaveSameType";
        String[] args = new String[4];
        args[0] = parentDir;
        args[1] = modelCmp3;
        args[2] = ocl;
        args[3] = target;

        EMAOCLVerifyTool.main(args);
    }

    @Test
    public void WBwithViewTest() {
        String modelCmp = "model.weatherBalloon.WeatherBalloonSensors";
        String ocl = "tagDef.semantic.Power";
        String view = "view.wbView.Power1";
        String[] args = new String[5];
        args[0] = parentDir;
        args[1] = modelCmp;
        args[2] = ocl;
        args[3] = target;
        args[4] = view;

        EMAOCLVerifyTool.main(args);
    }

    @Test
    public void EMAStudioExampleNegativeTest() {
        String modelCmp = "example.negative.Sensors";
        String ocl = "example.rule2";
        String[] args = new String[4];


        args[0] = parentDir;
        args[1] = modelCmp;
        args[2] = ocl;
        args[3] = target;

        EMAOCLVerifyTool.main(args);
    }
}
