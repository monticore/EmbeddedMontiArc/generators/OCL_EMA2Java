/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import de.monticore.lang.ocl.LogConfig;
import org.junit.BeforeClass;
import org.junit.Test;


import java.nio.file.Paths;
import java.util.*;



/**
 * This test nfp semantic rules with vies and models
 */
public class EMANFPVerificatorTest {

    protected final String workingDir = Paths.get(".").toAbsolutePath().normalize().toString();
    protected final String parentDir = workingDir + "/src/test/resources/nfp/";
    protected final String target = workingDir + "/target/gen/";


    @BeforeClass
    public static void init() {
        LogConfig.init();
    }

    public HashMap<String, EMANFPWitness> test(String modelCmp, String viewCmp, String nfpOcl, Boolean expected) {
        EMANFPVerificator verificator = new EMANFPVerificator();
        Boolean res = verificator.verificateNFP(Paths.get(parentDir), modelCmp, viewCmp, nfpOcl, Paths.get(target));
        assertEquals(expected, res);
        return verificator.getWitnessMap();
    }

    public HashMap<String, EMANFPWitness> testConsistency(String modelCmp, String nfpOcl, Boolean expected) {
        EMANFPVerificator verificator = new EMANFPVerificator();
        Boolean res = verificator.verificateNFP(Paths.get(parentDir), modelCmp, nfpOcl, Paths.get(target));
        assertEquals(expected, res);
        return verificator.getWitnessMap();
    }



    @Test
    public void testWB_WCET() {
        HashMap<String, EMANFPWitness> witnesses;
        witnesses = test("model.weatherBalloon.WeatherBalloonSensors", "view.wbView.WCET1", "tagDef.semantic.WCET", true);

        assertTrue(!witnesses.isEmpty());

        EMANFPWitness singleWitness = witnesses.get("controlSignalsIndataSaveInternalOutweatherBalloonSensors");
        assertTrue(singleWitness.pathsWitness.size() > 0);

        System.out.println(singleWitness);
    }

    @Test
    public void testWB_Power() {

        HashMap<String, EMANFPWitness> witnesses;
        witnesses = test("model.weatherBalloon.WeatherBalloonSensors", "view.wbView.Power1", "tagDef.semantic.Power", true);

        assertTrue(!witnesses.isEmpty());
    }
}
