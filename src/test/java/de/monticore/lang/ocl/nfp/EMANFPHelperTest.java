/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;


import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.*;
import de.monticore.lang.ocl.LogConfig;
import de.monticore.symboltable.GlobalScope;
import org.jscience.physics.amount.Amount;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.measure.quantity.Duration;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Created by kt on 31.12.2016.
 */
public class EMANFPHelperTest {

    protected String TEST_DIR = "src/test/resources/nfp/";


    @BeforeClass
    public static void init() {
        LogConfig.init();
    }

    @Test
    public void testMinMax() {

        Path modelDirPath = Paths.get(TEST_DIR);
        GlobalScope modelSymTab = EMANFPVerificator.createModelSymTab(modelDirPath, "model.weatherBalloon.WeatherBalloonSensors");
        ExpandedComponentInstanceSymbol root = modelSymTab.<ExpandedComponentInstanceSymbol>resolve("model.weatherBalloon.weatherBalloonSensors", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        System.out.println(root);

        ExpandedComponentInstanceSymbol control = root.getSubComponent("control").get();

        assertEquals("0 s", NFPHelper.min(root.getTags(nfp.LatencyTagSchema.LatencyCmpInstSymbol.KIND), Amount.valueOf("0 s")).toString());
        assertEquals("50 ms", NFPHelper.min(control.getTags(nfp.LatencyTagSchema.LatencyCmpInstSymbol.KIND), Amount.valueOf("0 s")).toString());
        assertEquals("50 ms", NFPHelper.max(control.getTags(nfp.LatencyTagSchema.LatencyCmpInstSymbol.KIND), Amount.valueOf("0 s")).toString());

    }

    @Test
    public void testSum() {

        Amount<Duration> a1 = Amount.valueOf(100, Duration.UNIT);
        Amount<Duration> a2 = Amount.valueOf(200, Duration.UNIT);
        Amount<Duration> a3 = Amount.valueOf(300, Duration.UNIT);
        Collection<Amount> list = new LinkedList<>();

        assertEquals("0 s", NFPHelper.sum(list, "s").toString());

        list.add(a1);
        list.add(a2);
        list.add(a3);

        assertEquals("600 s", NFPHelper.sum(list,"s").toString());
    }

    @Test
    public void testPaths() {

        Path modelDirPath = Paths.get(TEST_DIR);
        GlobalScope modelSymTab = EMANFPVerificator.createModelSymTab(modelDirPath, "model.weatherBalloon.WeatherBalloonSensors");
        ExpandedComponentInstanceSymbol root = modelSymTab.<ExpandedComponentInstanceSymbol>resolve("model.weatherBalloon.weatherBalloonSensors", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        assertNotNull(root);
        PortSymbol a1 = root.getSubComponent("control").get().getPort("sensorRequest1").get();
        PortSymbol a2 = root.getSubComponent("gps1").get().getPort("sensorOut").get();

        List<List<ElementInstance>> pathList = NFPHelper.paths(root, a1, a2, 0);
        assertTrue(pathList.size() > 0);
    }

    @Test
    public void testGraphs() {

        Path modelDirPath = Paths.get(TEST_DIR);
        GlobalScope modelSymTab = EMANFPVerificator.createModelSymTab(modelDirPath, "model.weatherBalloon.WeatherBalloonSensors");
        ExpandedComponentInstanceSymbol root = modelSymTab.<ExpandedComponentInstanceSymbol>resolve("model.weatherBalloon.weatherBalloonSensors", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        assertNotNull(root);
        PortSymbol a1 = root.getSubComponent("control").get().getPort("sensorRequest1").get();
        PortSymbol a2 = root.getSubComponent("gps1").get().getPort("sensorOut").get();

        Set<ElementInstance> graphSet = NFPHelper.graph(root, a1, a2);
        assertTrue(graphSet.size() > 0);
    }


    @Test
    public void testDirectSubCompPaths () {
        Path modelDirPath = Paths.get(TEST_DIR);
        GlobalScope modelSymTab = EMANFPVerificator.createModelSymTab(modelDirPath, "model.weatherBalloon.WeatherBalloonSensors");
        ExpandedComponentInstanceSymbol root = modelSymTab.<ExpandedComponentInstanceSymbol>resolve("model.weatherBalloon.weatherBalloonSensors", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        assertNotNull(root);


        List<List<ElementInstance>> selectedPaths = NFPHelper.directSubComponentPaths(root);
        assertTrue(selectedPaths.size() > 0);
    }
}
