/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.codegen;


import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class OCLEMACoCosTest extends OCLEMAGeneratorTest {

	protected final String parentDir = "src/test/resources/";


	private void test(String inputFile, String desiredFile, String outputFile) {

		String oclDir = "ocl/codegen/input/cocos/" + inputFile;
		cleanup(parentDir + oclDir);
		run(parentDir, oclDir);

		File expectedFile = new File(parentDir + "ocl/codegen/desired/cocos/" + desiredFile);
		File generatedFile = new File("./target/" + "ocl/codegen/input/cocos/" + outputFile);
		if (!expectedFile.exists()) {
			fail("*** expected-File Not Found: " + expectedFile);
		}
		if (!generatedFile.exists()) {
			fail("*** desired-File Not Found: " + generatedFile);
		}
		boolean result = compare(expectedFile, generatedFile);
		assertTrue(result);
	}

	@Ignore
	@Test
	public void testB1() {
		test("general/Coco_b1", "general/Coco_b1.java", "general/Coco_b1.java");
	}

	@Ignore
	@Test
	public void testR1() {
		test("ref/Coco_r1", "ref/Coco_r1.java", "ref/Coco_r1.java");
	}

	@Ignore
	@Test
	public void testR2() {
		test("ref/Coco_r2", "ref/Coco_r2.java", "ref/Coco_r2.java");
	}
	
	@Ignore
	@Test
	public void testR5() {
		test("ref/Coco_r5", "ref/Coco_r5.java", "ref/Coco_r5.java");
	}

	@Ignore
	@Test
	public void testR6() {
		test("ref/Coco_r6", "ref/Coco_r6.java", "ref/Coco_r6.java");
	}

	@Ignore
	@Test
	public void testR7() {
		test("ref/Coco_r7", "ref/Coco_r7.java", "ref/Coco_r7.java");
	}

	@Ignore
	@Test
	public void testR8() {
		test("ref/Coco_r8", "ref/Coco_r8.java", "ref/Coco_r8.java");
	}

	@Ignore
	@Test
	public void testR9() {
		test("ref/Coco_r9", "ref/Coco_r9.java", "ref/Coco_r9.java");
	}

	@Ignore
	@Test
	public void testR10() {
		test("ref/Coco_r10", "ref/Coco_r10.java", "ref/Coco_r10.java");
	}

	@Ignore
	@Test
	public void testCO1() {
		test("connections/Coco_co1", "connections/Coco_co1.java", "connections/Coco_co1.java");
	}

	@Ignore
	@Test
	public void testCO2() {
		test("connections/Coco_co2", "connections/Coco_co2.java", "connections/Coco_co2.java");
	}

	@Ignore
	@Test
	public void testCO3() {
		test("connections/Coco_co3", "connections/Coco_co3.java", "connections/Coco_co3.java");
	}

	@Ignore
	@Test
	public void testCV1() {
		test("conventions/Coco_cv1", "conventions/Coco_cv1.java", "conventions/Coco_cv1.java");
	}

	@Ignore
	@Test
	public void testCV2() {
		test("conventions/Coco_cv2", "conventions/Coco_cv2.java", "conventions/Coco_cv2.java");
	}

	@Ignore
	@Test
	public void testCV3() {
		test("conventions/Coco_cv3", "conventions/Coco_cv3.java", "conventions/Coco_cv3.java");
	}

	@Ignore
	@Test
	public void testCV5() {
		test("conventions/Coco_cv5", "conventions/Coco_cv5.java", "conventions/Coco_cv5.java");
	}

}
