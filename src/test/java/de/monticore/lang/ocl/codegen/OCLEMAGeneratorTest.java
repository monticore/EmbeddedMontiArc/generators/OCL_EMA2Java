/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.codegen;

import com.google.common.collect.Sets;
import de.monticore.io.paths.IterablePath;
import de.monticore.java.javadsl._ast.ASTCompilationUnit;
import de.monticore.java.javadsl._parser.JavaDSLParser;
import de.monticore.lang.ocl.LogConfig;
import org.junit.BeforeClass;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Optional;

public class OCLEMAGeneratorTest {


	OCLEMA2Java generator = new OCLEMA2Java(true, false, false);

	@BeforeClass
	public static void init() {
		LogConfig.init();
	}

	public void cleanup(String outputDIR) {
		// Clean up old files
		System.out.println(Paths.get(outputDIR).getParent());
		Iterator<Path> delIt = IterablePath.fromPaths(Arrays.asList(Paths.get(outputDIR).getParent()), Sets.newLinkedHashSet(Arrays.asList("class", "java"))).getResolvedPaths();
		while(delIt.hasNext()) {
			Path delPath = delIt.next();
			delPath.toFile().delete();
		}
	}

	protected void run(String parentDirectory, String modelPath) {
		generator.generate(parentDirectory, modelPath);
	}

	protected boolean compare(File expectedFile, File generatedFile) {
		JavaDSLParser javaParser = new JavaDSLParser();
		try {
			Optional<ASTCompilationUnit> expectedAst = javaParser.parse(expectedFile.getAbsolutePath());
			Optional<ASTCompilationUnit> generatedAst = javaParser.parse(generatedFile.getAbsolutePath());
			if(expectedAst.isPresent() && generatedAst.isPresent()) {
				return expectedAst.get().deepEquals(generatedAst.get());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
}
