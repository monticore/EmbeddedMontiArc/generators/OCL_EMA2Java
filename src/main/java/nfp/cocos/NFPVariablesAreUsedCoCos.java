/* (c) https://github.com/MontiCore/monticore */
package nfp.cocos;


import de.monticore.symboltable.Scope;
import de.se_rwth.commons.logging.Log;
import ocl.monticoreocl.ocl._ast.ASTCompilationUnit;
import ocl.monticoreocl.ocl._ast.ASTOCLInvariant;
import ocl.monticoreocl.ocl._ast.ASTOCLLetinExpr;
import ocl.monticoreocl.ocl._cocos.OCLASTCompilationUnitCoCo;
import ocl.monticoreocl.ocl._symboltable.OCLVariableDeclarationSymbol;

import java.util.Optional;

/**
 * Created by Ferdinand Mehlan on 09.05.2017.
 */
public class NFPVariablesAreUsedCoCos implements OCLASTCompilationUnitCoCo {

    @Override
    public void check(ASTCompilationUnit astComp){
        Log.debug(this.getClass().toString(),"Cocos loaded succesfully through conf");

        // load invariant
        try {
            ASTOCLInvariant astInv = (ASTOCLInvariant)astComp.getOCLFile().getOclConstraints().get(0).getOCLRawConstraint();
            Scope spannedInvScope = astInv.getSpannedScope().get();

            if(astInv.getStatements().isEmpty() || !(astInv.getStatements().get(0) instanceof ASTOCLLetinExpr)) {
                Log.error("OCL Definition of NFP needs to be of form;\n" +
                        " ocl myNFPTag {\n" +
                        "    context ContextType par, ContextType2 par2, ... inv tagTest:\n" +
                        "            let\n" +
                        "                (...)\n" +
                        "            in\n" +
                        "                (...)\n" +
                        "}");
            }

            Optional<OCLVariableDeclarationSymbol> oclDecl = spannedInvScope.resolve("selectedGraph", OCLVariableDeclarationSymbol.KIND);
            if(!oclDecl.isPresent()) {
                oclDecl = spannedInvScope.resolve("selectedPaths", OCLVariableDeclarationSymbol.KIND);
            }
            if(!oclDecl.isPresent()) {
                Log.error("OCL Definition of NFP needs to use variable selectedGraph || selectedPaths to select all Elements of interest for witness!");
            }

            oclDecl = spannedInvScope.resolve("aggregatedValue", OCLVariableDeclarationSymbol.KIND);
            if(!oclDecl.isPresent()) {
                oclDecl = spannedInvScope.resolve("aggregatedValues", OCLVariableDeclarationSymbol.KIND);
            }
            if(!oclDecl.isPresent()) {
                Log.error("OCL Definition of NFP needs to use variable aggregatedValue || aggregatedValues to aggregate all Value(s)" +
                        " from the selected Elements of interest for witness!");
            }

            oclDecl = spannedInvScope.resolve("viewEff", OCLVariableDeclarationSymbol.KIND);
            if(!oclDecl.isPresent() || !oclDecl.get().getClassName().equals("ViewEffectorSymbol")) {
                oclDecl = spannedInvScope.resolve("viewConn", OCLVariableDeclarationSymbol.KIND);
                if(!oclDecl.isPresent() || !oclDecl.get().getClassName().equals("ViewConnectorSymbol")) {
                    Log.error("OCL Definition of NFP needs to use a variable for the ViewConnector named viewConn or ViewEffector viewEff!");
                }
            }

        } catch (Exception e) {
            Log.error("Error while checking NFPVariablesAreUsedCoCos: " + e.getMessage());
        }
    }
}
