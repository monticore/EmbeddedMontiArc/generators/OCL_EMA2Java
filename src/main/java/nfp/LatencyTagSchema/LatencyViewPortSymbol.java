/* (c) https://github.com/MontiCore/monticore */
/* generated by template templates.de.monticore.lang.montiarc.tagschema.ValuedTagType*/


package nfp.LatencyTagSchema;

import de.monticore.lang.montiarc.tagging._symboltable.TagKind;
import de.monticore.lang.montiarc.tagging._symboltable.TagSymbol;
import org.jscience.physics.amount.Amount;

import javax.measure.quantity.Duration;
import javax.measure.unit.Unit;

/**
 * Created by ValuedTagType.ftl
 */
public class LatencyViewPortSymbol extends TagSymbol {
  public static final LatencyPortKind KIND = LatencyPortKind.INSTANCE;

  public LatencyViewPortSymbol(Amount<Duration> value) {
    this(KIND, value);
  }

  public LatencyViewPortSymbol(Number number, Unit<Duration> unit) {
    this(KIND, number, unit);
  }

  protected LatencyViewPortSymbol(LatencyPortKind kind, Amount<Duration> value) {
    super(kind, value);
  }

  protected LatencyViewPortSymbol(LatencyPortKind kind, Number number, Unit<Duration> unit) {
    this(kind, number.toString().contains(".") ?
      Amount.valueOf(number.doubleValue(), unit) :
      Amount.valueOf(number.intValue(),
          unit));
  }

  public Amount<Duration> getValue() {
     return getValue(0);
  }

  public Number getNumber() {
    return getValue().getExactValue();
  }

  public Unit<Duration> getUnit() {
    return getValue().getUnit();
  }

  @Override
  public String toString() {
    return String.format("LatencyPort = %s %s",
      getNumber(), getUnit());
  }

  public static class LatencyPortKind extends TagKind {
    public static final LatencyPortKind INSTANCE = new LatencyPortKind();

    protected LatencyPortKind() {
    }
  }
}
