/* (c) https://github.com/MontiCore/monticore */
/* generated by template templates.de.monticore.lang.montiarc.tagschema.TagSchema*/


package nfp.PowerTagSchema;

import de.monticore.CommonModelingLanguage;
import de.monticore.lang.montiarc.tagging._symboltable.TagableModelingLanguage;
import de.monticore.symboltable.resolving.CommonResolvingFilter;
import nfp.PowerTagSchema.*;

/**
 * generated by TagSchema.ftl
 */
public class PowerTagSchema {

  protected static PowerTagSchema instance = null;

  protected PowerTagSchema() {}

  protected static PowerTagSchema getInstance() {
    if (instance == null) {
      instance = new PowerTagSchema();
    }
    return instance;
  }

  protected void doRegisterTagTypes(TagableModelingLanguage modelingLanguage) {
    // all ModelingLanguage instances are actually instances of CommonModelingLanguage
    if(modelingLanguage instanceof CommonModelingLanguage) {
      CommonModelingLanguage commonModelingLanguage = (CommonModelingLanguage)modelingLanguage;

      modelingLanguage.addTagSymbolCreator(new PowerCmpInstSymbolCreator());
      commonModelingLanguage.addResolver(CommonResolvingFilter.create(PowerCmpInstSymbol.KIND));
      modelingLanguage.addTagSymbolCreator(new PowerCmpSymbolCreator());
      commonModelingLanguage.addResolver(CommonResolvingFilter.create(PowerCmpSymbol.KIND));

    }
  }

  public static void registerTagTypes(TagableModelingLanguage modelingLanguage) {
    getInstance().doRegisterTagTypes(modelingLanguage);
  }
}
