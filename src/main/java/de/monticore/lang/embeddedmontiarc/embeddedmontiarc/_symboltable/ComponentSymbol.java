/* (c) https://github.com/MontiCore/monticore */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import de.monticore.lang.embeddedmontiarc.EmbeddedMontiArcConstants;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc.types.EMAVariable;
import de.monticore.lang.embeddedmontiarc.helper.SymbolPrinter;
import de.monticore.lang.embeddedmontiarc.helper.Timing;
import de.monticore.lang.montiarc.tagging._symboltable.TaggingScopeSpanningSymbol;
import de.monticore.lang.monticar.common2._ast.ASTParameter;
import de.monticore.lang.monticar.mcexpressions._ast.ASTExpression;
import de.monticore.lang.monticar.si._symboltable.ResolutionDeclarationSymbol;
import de.monticore.symboltable.ImportStatement;
import de.monticore.symboltable.SymbolKind;
import de.monticore.symboltable.Symbols;
import de.monticore.symboltable.modifiers.AccessModifier;
import de.monticore.symboltable.types.JFieldSymbol;
import de.monticore.symboltable.types.JTypeSymbol;
import de.monticore.symboltable.types.references.ActualTypeArgument;
import de.se_rwth.commons.logging.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.annotation.Nullable;

public class ComponentSymbol extends TaggingScopeSpanningSymbol implements ElementInstance {
    public static final ComponentKind KIND = new ComponentKind();
    private final List<EMAAComponentImplementationSymbol> implementations = new ArrayList();
    private final Map<String, Optional<String>> stereotype = new HashMap();
    private boolean isInnerComponent = false;
    private Optional<ComponentSymbolReference> superComponent = Optional.empty();
    private Timing timing;
    private boolean delayed;
    private Optional<ComponentSymbol> referencedComponent;
    private List<ImportStatement> imports;
    private List<ResolutionDeclarationSymbol> resolutionDeclarationSymbols;
    private List<EMAVariable> parameters;
    private List<ASTExpression> arguments;

    public ComponentSymbol(String name) {
        super(name, KIND);
        this.timing = EmbeddedMontiArcConstants.DEFAULT_TIME_PARADIGM;
        this.delayed = false;
        this.referencedComponent = Optional.empty();
        this.imports = new ArrayList();
        this.resolutionDeclarationSymbols = new ArrayList();
        this.parameters = new ArrayList();
        this.arguments = new ArrayList();
    }

    public ComponentSymbol(String name, SymbolKind kind) {
        super(name, kind);
        this.timing = EmbeddedMontiArcConstants.DEFAULT_TIME_PARADIGM;
        this.delayed = false;
        this.referencedComponent = Optional.empty();
        this.imports = new ArrayList();
        this.resolutionDeclarationSymbols = new ArrayList();
        this.parameters = new ArrayList();
        this.arguments = new ArrayList();
    }

    public Optional<ComponentSymbol> getReferencedComponent() {
        return this.referencedComponent;
    }

    public void setReferencedComponent(Optional<ComponentSymbol> referencedComponent) {
        this.referencedComponent = referencedComponent;
    }

    public void addConfigParameter(JFieldSymbol parameterType) {
        if(this.referencedComponent.isPresent()) {
            ((ComponentSymbol)this.referencedComponent.get()).addConfigParameter(parameterType);
        } else {
            Log.errorIfNull(parameterType);
            Preconditions.checkArgument(parameterType.isParameter(), "Only parameters can be added.");
            this.getMutableSpannedScope().add(parameterType);
        }

    }

    public Optional<ConnectorSymbol> getConnector(String target) {
        Iterator var2 = this.getConnectors().iterator();

        ConnectorSymbol con;
        do {
            if(!var2.hasNext()) {
                return Optional.empty();
            }

            con = (ConnectorSymbol)var2.next();
        } while(!con.getTarget().equals(target));

        return Optional.of(con);
    }

    public Collection<ConnectorSymbol> getConnectors() {
        Collection<ConnectorSymbol> c = ((ComponentSymbol)this.referencedComponent.orElse(this)).getSpannedScope().resolveLocally(ConnectorSymbol.KIND);
        return (Collection)c.stream().sorted((o1, o2) -> {
            return o1.getSourcePosition().compareTo(o2.getSourcePosition());
        }).collect(Collectors.toList());
    }

    public Collection<ConnectorSymbol> getConnectors(AccessModifier visibility) {
        return (Collection)this.getConnectors().stream().filter((c) -> {
            return c.getAccessModifier().includes(visibility);
        }).collect(Collectors.toList());
    }

    public boolean hasConnector(String receiver) {
        return this.getConnectors().stream().filter((c) -> {
            return c.getName().equals(receiver);
        }).findAny().isPresent();
    }

    public boolean hasConnectors(String sender) {
        return this.getConnectors().stream().filter((c) -> {
            return c.getSource().equals(sender);
        }).findAny().isPresent();
    }

    public void addImplementation(EMAAComponentImplementationSymbol impl) {
        ((ComponentSymbol)this.referencedComponent.orElse(this)).implementations.add(impl);
    }

    public List<EMAAComponentImplementationSymbol> getImplementations() {
        return ImmutableList.copyOf(((ComponentSymbol)this.referencedComponent.orElse(this)).implementations);
    }

    public Optional<EMAAComponentImplementationSymbol> getImplementation(String name) {
        return this.getImplementations().stream().filter((i) -> {
            return i.getName().equals(name);
        }).findFirst();
    }

    public Collection<EMAAComponentImplementationSymbol> getImplementations(AccessModifier visibility) {
        return (Collection)this.getImplementations().stream().filter((s) -> {
            return s.getAccessModifier().includes(visibility);
        }).collect(Collectors.toList());
    }

    public Collection<ComponentSymbol> getInnerComponents() {
        return ((ComponentSymbol)this.referencedComponent.orElse(this)).getSpannedScope().resolveLocally(KIND);
    }

    public Optional<ComponentSymbol> getInnerComponent(String name) {
        return this.getInnerComponents().stream().filter((c) -> {
            return c.getName().equals(name);
        }).findFirst();
    }

    public Collection<ComponentSymbol> getInnerComponents(AccessModifier visibility) {
        return (Collection)this.getInnerComponents().stream().filter((s) -> {
            return s.getAccessModifier().includes(visibility);
        }).collect(Collectors.toList());
    }

    public boolean isInnerComponent() {
        return ((ComponentSymbol)this.referencedComponent.orElse(this)).isInnerComponent;
    }

    public void setIsInnerComponent(boolean isInnerComponent) {
        ((ComponentSymbol)this.referencedComponent.orElse(this)).isInnerComponent = isInnerComponent;
    }

    public void addFormalTypeParameter(JTypeSymbol formalTypeParameter) {
        if(this.referencedComponent.isPresent()) {
            ((ComponentSymbol)this.referencedComponent.get()).addFormalTypeParameter(formalTypeParameter);
        } else {
            Preconditions.checkArgument(formalTypeParameter.isFormalTypeParameter());
            this.getMutableSpannedScope().add(formalTypeParameter);
        }

    }

    public List<JTypeSymbol> getFormalTypeParameters() {
        Collection<JTypeSymbol> resolvedTypes = ((ComponentSymbol)this.referencedComponent.orElse(this)).getSpannedScope().resolveLocally(JTypeSymbol.KIND);
        return (List)resolvedTypes.stream().filter(JTypeSymbol::isFormalTypeParameter).collect(Collectors.toList());
    }

    public boolean hasFormalTypeParameters() {
        return !this.getFormalTypeParameters().isEmpty();
    }

    public boolean hasConfigParameters() {
        return !this.getConfigParameters().isEmpty();
    }

    public boolean hasPorts() {
        return !this.getPorts().isEmpty();
    }

    public void addStereotype(String key, @Nullable String value) {
        if(value != null && value.isEmpty()) {
            value = null;
        }

        ((ComponentSymbol)this.referencedComponent.orElse(this)).stereotype.put(key, Optional.ofNullable(value));
    }

    public Collection<PortSymbol> getPorts() {
        Collection<PortSymbol> symbols = ((ComponentSymbol)this.referencedComponent.orElse(this)).getSpannedScope().resolveLocally(PortSymbol.KIND);
        return symbols;
    }

    public Collection<PortArraySymbol> getPortArrays() {
        Collection<PortArraySymbol> symbols = ((ComponentSymbol)this.referencedComponent.orElse(this)).getSpannedScope().resolveLocally(PortArraySymbol.KIND);
        return symbols;
    }

    public PortArraySymbol getPortArray(String name) {
        Log.debug(name, "Looking for Pas:");
        Iterator var2 = this.getPortArrays().iterator();

        PortArraySymbol pas;
        do {
            if(!var2.hasNext()) {
                return null;
            }

            pas = (PortArraySymbol)var2.next();
            Log.debug(pas.getName(), "Cur Pas:");
        } while(!pas.getName().equals(name));

        Log.debug(pas.getName(), "Found Pas");
        return pas;
    }

    public boolean isPortDependentOnResolutionDeclarationSymbol(String portName, String nameToDependentOn) {
        PortArraySymbol portArraySymbol = this.getPortArray(portName);
        Log.debug(portName, "PortName:");
        Log.debug(nameToDependentOn, "Expected NameToDependOn:");
        if(portArraySymbol.getNameSizeDependsOn().isPresent()) {
            Log.debug((String)portArraySymbol.getNameSizeDependsOn().get(), "Actual NameToDependOn:");
            if(((String)portArraySymbol.getNameSizeDependsOn().get()).equals(nameToDependentOn)) {
                return true;
            }
        }

        return false;
    }

    public List<ActualTypeArgument> getActualTypeArguments() {
        return null;
    }

    public Collection<PortSymbol> getIncomingPorts() {
        return (Collection)((ComponentSymbol)this.referencedComponent.orElse(this)).getSpannedScope().resolveLocally(PortSymbol.KIND).stream().filter((p) -> {
            return ((PortSymbol)p).isIncoming();
        }).collect(Collectors.toList());
    }

    public Optional<PortSymbol> getIncomingPort(String name) {
        return this.getIncomingPorts().stream().filter((p) -> {
            return p.getName().equals(name);
        }).findFirst();
    }

    public Collection<PortSymbol> getIncomingPorts(AccessModifier visibility) {
        return (Collection)this.getIncomingPorts().stream().filter((s) -> {
            return s.getAccessModifier().includes(visibility);
        }).collect(Collectors.toList());
    }

    public Collection<PortSymbol> getOutgoingPorts() {
        return (Collection)((ComponentSymbol)this.referencedComponent.orElse(this)).getSpannedScope().resolveLocally(PortSymbol.KIND).stream().filter((p) -> {
            return ((PortSymbol)p).isOutgoing();
        }).collect(Collectors.toList());
    }

    public List<PortSymbol> getAllIncomingPorts() {
        return ((ComponentSymbol)this.referencedComponent.orElse(this)).getAllPorts(true);
    }

    public Optional<PortSymbol> getOutgoingPort(String name) {
        return this.getOutgoingPorts().stream().filter((p) -> {
            return p.getName().equals(name);
        }).findFirst();
    }

    public Collection<PortSymbol> getOutgoingPorts(AccessModifier visibility) {
        return (Collection)this.getOutgoingPorts().stream().filter((s) -> {
            return s.getAccessModifier().includes(visibility);
        }).collect(Collectors.toList());
    }

    public List<PortSymbol> getAllOutgoingPorts() {
        return ((ComponentSymbol)this.referencedComponent.orElse(this)).getAllPorts(false);
    }

    protected List<PortSymbol> getAllPorts() {
        List<PortSymbol> result = new ArrayList();
        result.addAll(this.getPorts());
        Optional<ComponentSymbolReference> superCompOpt = this.getSuperComponent();
        if(superCompOpt.isPresent()) {
            Iterator var3 = ((ComponentSymbolReference)superCompOpt.get()).getAllPorts().iterator();

            while(var3.hasNext()) {
                PortSymbol superPort = (PortSymbol)var3.next();
                boolean alreadyAdded = false;
                Iterator var6 = result.iterator();

                while(var6.hasNext()) {
                    PortSymbol pToAdd = (PortSymbol)var6.next();
                    if(pToAdd.getName().equals(superPort.getName())) {
                        alreadyAdded = true;
                        break;
                    }
                }

                if(!alreadyAdded) {
                    result.add(superPort);
                }
            }
        }

        return result;
    }

    private List<PortSymbol> getAllPorts(boolean isIncoming) {
        return (List)this.getAllPorts().stream().filter((p) -> {
            return p.isIncoming() == isIncoming;
        }).collect(Collectors.toList());
    }

    public Optional<ComponentSymbolReference> getSuperComponent() {
        return this.referencedComponent.isPresent()?((ComponentSymbol)this.referencedComponent.get()).getSuperComponent():this.superComponent;
    }

    public void setSuperComponent(Optional<ComponentSymbolReference> superComponent) {
        ((ComponentSymbol)this.referencedComponent.orElse(this)).superComponent = superComponent;
    }

    public Collection<ComponentInstanceSymbol> getSubComponents() {
        return ((ComponentSymbol)this.referencedComponent.orElse(this)).getSpannedScope().resolveLocally(ComponentInstanceSymbol.KIND);
    }

    public Optional<ComponentInstanceSymbol> getSubComponent(String name) {
        return this.getSubComponents().stream().filter((p) -> {
            return p.getName().equals(name);
        }).findFirst();
    }

    public Collection<ComponentInstanceSymbol> getSubComponents(AccessModifier visibility) {
        return (Collection)this.getSubComponents().stream().filter((s) -> {
            return s.getAccessModifier().includes(visibility);
        }).collect(Collectors.toList());
    }

    public List<JFieldSymbol> getConfigParameters() {
        if(this.referencedComponent.isPresent()) {
            return ((ComponentSymbol)this.referencedComponent.get()).getConfigParameters();
        } else {
            Collection<JFieldSymbol> resolvedAttributes = this.getMutableSpannedScope().resolveLocally(JFieldSymbol.KIND);
            List<JFieldSymbol> parameters = Symbols.sortSymbolsByPosition((Collection)resolvedAttributes.stream().filter(JFieldSymbol::isParameter).collect(Collectors.toList()));
            return parameters;
        }
    }

    public Collection<JFieldSymbol> getConfigParameters(AccessModifier visibility) {
        return (Collection)this.getConfigParameters().stream().filter((s) -> {
            return s.getAccessModifier().includes(visibility);
        }).collect(Collectors.toList());
    }

    public void setDelayed(boolean delayed) {
        ((ComponentSymbol)this.referencedComponent.orElse(this)).delayed = delayed;
    }

    public boolean hasDelay() {
        return ((ComponentSymbol)this.referencedComponent.orElse(this)).delayed;
    }

    public void addStereotype(String key, Optional<String> optional) {
        ((ComponentSymbol)this.referencedComponent.orElse(this)).stereotype.put(key, optional);
    }

    public Map<String, Optional<String>> getStereotype() {
        return ((ComponentSymbol)this.referencedComponent.orElse(this)).stereotype;
    }

    public Timing getBehaviorKind() {
        return ((ComponentSymbol)this.referencedComponent.orElse(this)).timing;
    }

    public void setBehaviorKind(Timing behaviorKind) {
        ((ComponentSymbol)this.referencedComponent.orElse(this)).timing = behaviorKind;
        if(behaviorKind.isDelaying()) {
            ((ComponentSymbol)this.referencedComponent.orElse(this)).setDelayed(true);
        }

    }

    public boolean isDecomposed() {
        return !this.isAtomic();
    }

    public boolean isAtomic() {
        return this.getSubComponents().isEmpty();
    }

    public String toString() {
        return SymbolPrinter.printComponent(this);
    }

    public List<ImportStatement> getImports() {
        return this.imports;
    }

    public void setImports(List<ImportStatement> imports) {
        this.imports = imports;
    }

    public Optional<ResolutionDeclarationSymbol> getResolutionDeclarationSymbol(String name) {
        Iterator var2 = this.getResolutionDeclarationSymbols().iterator();

        ResolutionDeclarationSymbol symbol;
        do {
            if(!var2.hasNext()) {
                return Optional.empty();
            }

            symbol = (ResolutionDeclarationSymbol)var2.next();
        } while(!symbol.getNameToResolve().equals(name));

        return Optional.of(symbol);
    }

    public boolean hasResolutionDeclaration(String name) {
        Iterator var2 = this.resolutionDeclarationSymbols.iterator();

        ResolutionDeclarationSymbol resDeclSym;
        do {
            if(!var2.hasNext()) {
                return false;
            }

            resDeclSym = (ResolutionDeclarationSymbol)var2.next();
        } while(!resDeclSym.getNameToResolve().equals(name));

        return true;
    }

    public int howManyResolutionDeclarationSymbol() {
        return this.resolutionDeclarationSymbols.size();
    }

    public void addResolutionDeclarationSymbol(ResolutionDeclarationSymbol resolutionDeclarationSymbol) {
        if(this.hasResolutionDeclaration(resolutionDeclarationSymbol.getNameToResolve())) {
            Log.error("0x0S0001 Name " + resolutionDeclarationSymbol.getNameToResolve() + " to resolve is a duplicate");
        }

        this.resolutionDeclarationSymbols.add(resolutionDeclarationSymbol);
        Log.debug(this.getFullName(), "Added ResolutionDeclarationSymbol to ComponentSymbol with name:");
    }

    public List<ResolutionDeclarationSymbol> getResolutionDeclarationSymbols() {
        return this.resolutionDeclarationSymbols;
    }

    public static EMAComponentBuilder builder() {
        return EMAComponentBuilder.getInstance();
    }

    public void addParameter(ASTParameter astParameter) {
        if(this.referencedComponent.isPresent()) {
            ((ComponentSymbol)this.referencedComponent.get()).addParameter(astParameter);
        } else {
            EMAVariable param = new EMAVariable();
            param.setName(astParameter.getName());
            param.setType(astParameter.getType());
            this.parameters.add(param);
        }

    }

    public List<EMAVariable> getParameters() {
        return this.parameters;
    }

    public void setParameters(List<EMAVariable> parameters) {
        this.parameters = parameters;
    }

    public List<ASTExpression> getArguments() {
        return this.referencedComponent.isPresent()?((ComponentSymbol)this.referencedComponent.get()).getArguments():this.arguments;
    }

    public void addArgument(ASTExpression astExpression) {
        if(this.referencedComponent.isPresent()) {
            ((ComponentSymbol)this.referencedComponent.get()).addArgument(astExpression);
        } else {
            this.arguments.add(astExpression);
        }

    }

    public void setArguments(List<ASTExpression> arguments) {
        this.arguments = arguments;
    }

    public void addIncomingPort(PortSymbol symbol) {
    }
}
