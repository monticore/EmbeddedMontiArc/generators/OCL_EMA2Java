/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.codegen.visitors;

import de.monticore.lang.ocl.codegen.OCLHelper;
import de.monticore.lang.ocl.codegen.OCLVariableNaming;
import de.monticore.symboltable.Scope;
import de.monticore.types.TypesPrinter;
import de.se_rwth.commons.logging.Log;
import ocl.monticoreocl.ocl._ast.*;
import ocl.monticoreocl.ocl._symboltable.OCLVariableDeclarationSymbol;
import ocl.monticoreocl.ocl._visitor.OCLVisitor;

import java.util.Optional;

/**
 * Visitor that creates the EMA specific "declaration" output for ASTOCLNodes.
 *
 */
public class OCLEMA2JavaDeclarationVisitor extends OCL2JavaDeclarationVisitor{

	private StringBuilder sb;
	private OCLVariableNaming varNaming;
	private OCLEMA2JavaInplaceVisitor inplaceVisitor;
	private Boolean nfp;

	public OCLEMA2JavaDeclarationVisitor(StringBuilder sb, OCLVariableNaming varNaming, Boolean nfp, OCLEMA2JavaInplaceVisitor inplaceVisitor) {
		this.sb = sb;
		this.varNaming = varNaming;
		this.inplaceVisitor = inplaceVisitor;
		this.nfp = nfp;
		super.setSB(sb);
		super.setVarNaming(varNaming);
		super.setInplaceVisitor(inplaceVisitor);
	}
	
	private OCLVisitor realThis = this;

	@Override
	public OCLVisitor getRealThis() {
		return realThis;
	}


	@Override
	public void traverse(ASTOCLLetinExpr node) {
		super.traverse(node);

		if(nfp && node.enclosingScopeIsPresent()) {
			Scope enclScope = node.getEnclosingScope().get();
			Optional<OCLVariableDeclarationSymbol> oclDecl = enclScope.resolve("selectedGraph", OCLVariableDeclarationSymbol.KIND);
			if (!oclDecl.isPresent()) {
				oclDecl = enclScope.resolve("selectedVals", OCLVariableDeclarationSymbol.KIND);
			}
			Optional<OCLVariableDeclarationSymbol> viewEffDecl = enclScope.resolve("viewEff", OCLVariableDeclarationSymbol.KIND);
			Optional<OCLVariableDeclarationSymbol> viewConnDecl = enclScope.resolve("viewConn", OCLVariableDeclarationSymbol.KIND);
			String viewSymbolSring = "";
			if (viewEffDecl.isPresent()) {
				viewSymbolSring = "viewEff,";
			} else if (viewConnDecl.isPresent()) {
				viewSymbolSring = "viewConn,";
			}
			if (oclDecl.isPresent()) {
				ASTOCLInvariant invNode = (ASTOCLInvariant)enclScope.getSpanningSymbol().get().getAstNode().get();

				String contextDefs = "";
				for (ASTOCLContextDefinition context : invNode.getOCLClassContext().get().getContextDefinitions()) {
					contextDefs += context.getName().get() + ".getName()+";
				}
				contextDefs = contextDefs.substring(0, contextDefs.length()-1);
				String witnessString = "if (" + varNaming.getName(node) + ") {\n" +
						"positiveWitness.put(" + contextDefs + ", new EMANFPWitness("+oclDecl.get().getName()+", " + viewSymbolSring + " aggregatedValue, true));\n" +
						"} else {\n" +
						"negativeWitness.put(" + contextDefs + ", new EMANFPWitness("+oclDecl.get().getName()+", " + viewSymbolSring + " aggregatedValue, false));\n}\n";
				sb.insert(sb.length() - 2, witnessString);
			}
		}

	}

	@Override
	public void traverse(ASTOCLForallExpr node) {
		if(nfp && node.enclosingScopeIsPresent()) {
			Scope enclScope = node.getEnclosingScope().get();
			Optional<OCLVariableDeclarationSymbol> oclDecl = enclScope.resolve("selectedPaths", OCLVariableDeclarationSymbol.KIND);
			Optional<OCLVariableDeclarationSymbol> viewEffDecl = enclScope.resolve("viewEff", OCLVariableDeclarationSymbol.KIND);
			Optional<OCLVariableDeclarationSymbol> viewConnDecl = enclScope.resolve("viewConn", OCLVariableDeclarationSymbol.KIND);
			String viewSymbolSring = "";
			if (viewEffDecl.isPresent()) {
				viewSymbolSring = "viewEff,";
			} else if (viewConnDecl.isPresent()) {
				viewSymbolSring = "viewConn,";
			}

			if (oclDecl.isPresent()) {
				if(node.oCLForallExprIsPresent()) {
					node.getOCLForallExpr().get().accept(getRealThis());
				} else {
					sb.append("boolean ").append(varNaming.getName(node)).append(" = true;\n");

					ASTOCLInvariant invNode = (ASTOCLInvariant)enclScope.getSpanningSymbol().get().getAstNode().get();
					String contextDefs = "";
					for (ASTOCLContextDefinition context : invNode.getOCLClassContext().get().getContextDefinitions()) {
						contextDefs += context.getName().get() + ".getName()+";
					}
					contextDefs = contextDefs.substring(0, contextDefs.length()-1);
					String witnessString = "positiveWitness.put(" + contextDefs + ", new EMANFPWitness(selectedPaths, " + viewSymbolSring + " aggregatedValues, true));\n";
					sb.append(witnessString);

					ASTOCLInExpr inExpr = node.getOCLCollectionVarDeclaration().get().getOCLInExpr().get();

					String type="";
					String loopVar="";

					if(inExpr.oCLInWithOutTypeIsPresent()) {
						ASTOCLInWithOutType inWithOutType = inExpr.getOCLInWithOutType().get();
						inWithOutType.getExpression().get().accept(getRealThis());

						sb.append("ListIterator<");

						type = OCLHelper.getReturnType(inWithOutType.getExpression().get());
						if(!type.equals("")) {
							type = type.replaceFirst("(\\w*<)","");
							type = type.replaceAll("(>\\z)", "");
							sb.append(type).append("> ");
							OCLVariableDeclarationSymbol varDeclSymbol = new OCLVariableDeclarationSymbol(inWithOutType.getName());
							varDeclSymbol.setName(inWithOutType.getName());
							varDeclSymbol.setClassName(type);
							node.getEnclosingScope().get().getAsMutableScope().add(varDeclSymbol);
						} else {
							Log.error("No Type for loop variable found!");
						}

						loopVar = inWithOutType.getName();

						sb.append("itr_").append(loopVar).append(" = ");
						inWithOutType.getExpression().get().accept(inplaceVisitor);
						sb.append(".listIterator();\n");
						sb.append("while (itr_"+loopVar+".hasNext()) {\n");
						sb.append("int index_"+loopVar+" = itr_"+loopVar+".nextIndex();\n");
						sb.append(type).append(" ").append(loopVar).append(" = ");
						sb.append("itr_"+inWithOutType.getName()).append(".next();\n");

					} else if (inExpr.oCLInWithTypeIsPresent()) {
						ASTOCLInWithType inWithType = inExpr.getOCLInWithType().get();
						inWithType.getExpression().get().accept(getRealThis());


						sb.append("ListIterator<");
						if (inWithType.typeIsPresent()) {
							sb.append(TypesPrinter.printType(inWithType.getType().get()));
						} else if (inWithType.classNameIsPresent()) {
							sb.append(inWithType.getClassName().get());
						}
						sb.append("> ");
						loopVar = inWithType.getVarName();
						sb.append("itr_").append(loopVar).append(" = ");
						inWithType.getExpression().get().accept(inplaceVisitor);
						sb.append(".listIterator();\n");
						sb.append("while (itr_"+loopVar+".hasNext()) {\n");
						sb.append("int index_"+loopVar+" = itr_"+loopVar+".nextIndex();\n");
						sb.append(type).append(" ").append(loopVar).append(" = ");
						sb.append("itr_"+loopVar).append(".next();\n");
					}

					node.getOCLExpression().get().accept(getRealThis());

					sb.append(varNaming.getName(node)).append(" &= (");
					node.getOCLExpression().get().accept(inplaceVisitor);
					sb.append(");\n");
					sb.append("if (!").append(varNaming.getName(node)).append(") {\n");
					sb.append("negativeWitness.put("+contextDefs+", new EMANFPWitness(selectedPaths.get(index_"+loopVar+"), " + viewSymbolSring + " "+loopVar+", false));\n");
					sb.append("positiveWitness.remove("+contextDefs+");\n");
					sb.append("break;\n");
					sb.append("}\n");
					sb.append("}\n");

				}
			}
		} else {
			super.traverse(node);
		}
	}

	@Override
	public void traverse(ASTOCLExistsExpr node) {
		if(nfp && node.enclosingScopeIsPresent()) {
			Scope enclScope = node.getEnclosingScope().get();
			Optional<OCLVariableDeclarationSymbol> oclDecl = enclScope.resolve("selectedPaths", OCLVariableDeclarationSymbol.KIND);
			Optional<OCLVariableDeclarationSymbol> viewEffDecl = enclScope.resolve("viewEff", OCLVariableDeclarationSymbol.KIND);
			Optional<OCLVariableDeclarationSymbol> viewConnDecl = enclScope.resolve("viewConn", OCLVariableDeclarationSymbol.KIND);
			String viewSymbolSring = "";
			if (viewEffDecl.isPresent()) {
				viewSymbolSring = "viewEff,";
			} else if (viewConnDecl.isPresent()) {
				viewSymbolSring = "viewConn,";
			}
			if (oclDecl.isPresent()) {
				if(node.oCLExistsExprIsPresent()) {
					node.getOCLExistsExpr().get().accept(getRealThis());
				} else {
					sb.append("boolean ").append(varNaming.getName(node)).append(" = false;\n");

					ASTOCLInvariant invNode = (ASTOCLInvariant)enclScope.getSpanningSymbol().get().getAstNode().get();
					String contextDefs = "";
					for (ASTOCLContextDefinition context : invNode.getOCLClassContext().get().getContextDefinitions()) {
						contextDefs += context.getName().get() + ".getName()+";
					}
					contextDefs = contextDefs.substring(0, contextDefs.length()-1);
					String witnessString = "negativeWitness.put(" + contextDefs + ", new EMANFPWitness(selectedPaths, " + viewSymbolSring + " aggregatedValues, false));\n";
					sb.append(witnessString);

					ASTOCLInExpr inExpr = node.getOCLCollectionVarDeclaration().get().getOCLInExpr().get();

					String type="";
					String loopVar="";

					if(inExpr.oCLInWithOutTypeIsPresent()) {
						ASTOCLInWithOutType inWithOutType = inExpr.getOCLInWithOutType().get();
						inWithOutType.getExpression().get().accept(getRealThis());

						sb.append("ListIterator<");

						type = OCLHelper.getReturnType(inWithOutType.getExpression().get());
						if(!type.equals("")) {
							type = type.replaceFirst("(\\w*<)","");
							type = type.replaceAll("(>\\z)", "");
							sb.append(type).append("> ");
							OCLVariableDeclarationSymbol varDeclSymbol = new OCLVariableDeclarationSymbol(inWithOutType.getName());
							varDeclSymbol.setName(inWithOutType.getName());
							varDeclSymbol.setClassName(type);
							node.getEnclosingScope().get().getAsMutableScope().add(varDeclSymbol);
						} else {
							Log.error("No Type for loop variable found!");
						}

						loopVar = inWithOutType.getName();

						sb.append("itr_").append(loopVar).append(" = ");
						inWithOutType.getExpression().get().accept(inplaceVisitor);
						sb.append(".listIterator();\n");
						sb.append("while (itr_"+loopVar+".hasNext()) {\n");
						sb.append("int index_"+loopVar+" = itr_"+loopVar+".nextIndex();\n");
						sb.append(type).append(" ").append(loopVar).append(" = ");
						sb.append("itr_"+inWithOutType.getName()).append(".next();\n");

					} else if (inExpr.oCLInWithTypeIsPresent()) {
						ASTOCLInWithType inWithType = inExpr.getOCLInWithType().get();
						inWithType.getExpression().get().accept(getRealThis());


						sb.append("ListIterator<");
						if (inWithType.typeIsPresent()) {
							sb.append(TypesPrinter.printType(inWithType.getType().get()));
						} else if (inWithType.classNameIsPresent()) {
							sb.append(inWithType.getClassName().get());
						}
						sb.append("> ");
						loopVar = inWithType.getVarName();
						sb.append("itr_").append(loopVar).append(" = ");
						inWithType.getExpression().get().accept(inplaceVisitor);
						sb.append(".listIterator();\n");
						sb.append("while (itr_"+loopVar+".hasNext()) {\n");
						sb.append("int index_"+loopVar+" = itr_"+loopVar+".nextIndex();\n");
						sb.append(type).append(" ").append(loopVar).append(" = ");
						sb.append("itr_"+loopVar).append(".next();\n");
					}

					node.getOCLExpression().get().accept(getRealThis());

					sb.append(varNaming.getName(node)).append(" |= (");
					node.getOCLExpression().get().accept(inplaceVisitor);
					sb.append(");\n");
					sb.append("if (").append(varNaming.getName(node)).append(") {\n");
					sb.append("positiveWitness.put("+contextDefs+", new EMANFPWitness(selectedPaths.get(index_"+loopVar+"), " + viewSymbolSring + " "+loopVar+", true));\n");
					sb.append("negativeWitness.remove("+contextDefs+");\n");
					sb.append("break;\n");
					sb.append("}\n");
					sb.append("}\n");

				}
			}
		} else {
			super.traverse(node);
		}
	}
}
