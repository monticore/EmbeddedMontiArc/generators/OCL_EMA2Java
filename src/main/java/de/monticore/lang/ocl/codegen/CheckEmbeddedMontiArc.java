/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.codegen;

import com.google.common.collect.Sets;
import de.monticore.ModelingLanguageFamily;
import de.monticore.io.paths.IterablePath;
import de.monticore.io.paths.ModelPath;
import de.monticore.java.lang.JavaDSLLanguage;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.EmbeddedMontiArcLanguage;
import de.monticore.lang.ocl.codegen.compilation.CustomCompiler;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;

import javax.tools.ToolProvider;
import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

public class CheckEmbeddedMontiArc {
	
	public static void main(String args[]) {
		//System.out.println(Arrays.stream(args).collect(Collectors.joining(", ")));
		if (args.length != 3) {
			System.out.println("This function needs three parameter: the MontiArc model path, the name of the MA component and the OCL model path.");
			return;
		}

		Path maPath = Paths.get(args[0]);
		String cmpName = args[1];
		Path oclPath = Paths.get(args[2]);
		
		if(!maPath.toFile().exists()) {
			System.out.println("MontiArc directory does not exist!");
			return;
		}

		if(!oclPath.toFile().exists()) {
			System.out.println("OCL directory does not exist!");
			return;
		}
		
		// create MA symbol table
		GlobalScope scope = createSymTab(maPath, cmpName);
		
		// delete existing generated files
		Iterator<Path> delIt = IterablePath.fromPaths(Arrays.asList(oclPath), Sets.newLinkedHashSet(Arrays.asList("class", "java"))).getResolvedPaths();
		while(delIt.hasNext()) {
			Path oclModel = delIt.next();
			oclModel.toFile().delete();
		}
		
		// check constraints
		int unsatCounter = 0;
		Iterator<Path> oclIt = IterablePath.fromPaths(Arrays.asList(oclPath), "ocl").get();
		
		if(!oclIt.hasNext()) {
			System.out.println("OCL directory does not contain any .ocl files!");
			return;
		}
		
		System.out.println("-----------------------------------");
		while(oclIt.hasNext()) {
			Path oclModel = oclIt.next();
			System.out.print(String.format("Checking '%s': ", oclModel));
			if(checkMontiArcUsingOCL(scope, "src/text/resources/ocl/codegen/input", oclModel.toString())) {
				System.out.println("ok");
			} else {
				System.out.println("ocl constraint not satisfied");
				unsatCounter++;
			}
		}
		System.out.println("-----------------------------------");
		System.out.println(String.format("Result: Component %s failed to satisfy %d constraint.", cmpName, unsatCounter));
	}
	
	/**
	 * Check if the montiarc model satisfies the given ocl constraints
	 *
	 * @param scope Symboltable of the montiarc model
	 * @param parentDir dir to the ocl models"
	 * @param oclFile Path to the ocl file
	 */
	public static boolean checkMontiArcUsingOCL(GlobalScope scope, String parentDir, String oclFile) {
		// transform ocl model to java code
		OCLEMA2Java generator = new OCLEMA2Java(true, false, false);
		String constraintName = com.google.common.io.Files.getNameWithoutExtension(oclFile + ".ocl");
		//Path genOutputFile = Paths.get(parentDir + oclFile + ".java");
		Path genOutputFile = Paths.get("./target/" + oclFile + ".java");
		generator.generate(parentDir, oclFile);

		// compile java source code
		CustomCompiler compiler = new CustomCompiler(ToolProvider.getSystemJavaCompiler(), Thread.currentThread().getContextClassLoader());
		int compilationResult = compiler.run(null, null, null, new String[] {genOutputFile.toString()});
		
		if (compilationResult != 0) {
	    	System.out.println("Compilation of " + genOutputFile.getFileName().toString() + " failed.");
	    	return false;
	    }
		
		// load and instantiate compiled class
		boolean result = false;
		try {
			URL classUrl = Paths.get("./target/").toUri().toURL();
			URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {classUrl});

			String classLoaderString = Paths.get(oclFile).getParent().toString().replace(File.separatorChar, '.') + '.' + constraintName;
			if (classLoaderString.startsWith(".")) {classLoaderString = classLoaderString.substring(1);}
			Class<?> loadedClass = classLoader.loadClass(classLoaderString);
			Object classObj = loadedClass.newInstance();
			// execute check method
			Method method = classObj.getClass().getMethod("check", GlobalScope.class);
			Object res = method.invoke(classObj, scope);
			result = (boolean)res;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return result;
	}
	
	public static GlobalScope createSymTab(Path maPath, String cmpName) {
	    ModelingLanguageFamily fam = new ModelingLanguageFamily();
	    fam.addModelingLanguage(new EmbeddedMontiArcLanguage());
	    fam.addModelingLanguage(new JavaDSLLanguage());
	    final ModelPath maMp = new ModelPath(maPath, Paths.get("src/test/resources/defaultTypes"));
	    GlobalScope scope = new GlobalScope(maMp, fam);
	    
	    // resolve all referenced ComponentSymbols
	    Optional<ComponentSymbol> cmp = scope.<ComponentSymbol>resolve(cmpName, ComponentSymbol.KIND);
		resolveComponentInstances(cmp.get().getSpannedScope());
		
		return scope;
	}
	
	private static void resolveComponentInstances(Scope scope) {
		Collection<ComponentInstanceSymbol> instances = scope.resolveLocally(ComponentInstanceSymbol.KIND);
		
		for(ComponentInstanceSymbol instance : instances) {
			resolveComponentInstances(instance.getComponentType().getReferencedSymbol().getSpannedScope());
		}
	}
}
