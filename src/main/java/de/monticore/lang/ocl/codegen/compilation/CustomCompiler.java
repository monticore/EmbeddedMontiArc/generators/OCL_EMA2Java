/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.codegen.compilation;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.jar.Manifest;

import javax.lang.model.SourceVersion;
import javax.tools.*;

import com.google.common.base.Joiner;

public class CustomCompiler implements JavaCompiler {
	
	private final ClassLoader classLoader;
	private final JavaCompiler compiler;

	public CustomCompiler(JavaCompiler compiler, ClassLoader classLoader) {
		this.compiler = compiler;
		this.classLoader = classLoader;
	}
	
	protected static boolean usesSureFire(URLClassLoader cl) {
		for (URL url : cl.getURLs()) {
			if (url.getPath().contains("surefirebooter")) {
				return true;
			}
		}
		return false;
	}

	private String getClasspath() {
		List<String> classpaths = new ArrayList<String>();
		
		if (classLoader instanceof URLClassLoader) {
			URLClassLoader cl = (URLClassLoader) classLoader;
				if (usesSureFire(cl)) {
					// read classpath from manifest
					URL url = cl.findResource("META-INF/MANIFEST.MF");
					Manifest manifest;
					try {
						manifest = new Manifest(url.openStream());
						String classpath = manifest.getMainAttributes().getValue("Class-Path");
						for (String entry : classpath.split(" ")) {
							classpaths.add(new File(new URL(entry).getPath()).getAbsolutePath());
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					// iterate through classloader hierarchy
					ClassLoader c = classLoader;
					while (c instanceof URLClassLoader) {
						for (URL url : ((URLClassLoader)c).getURLs()) {
							classpaths.add(new File(url.getPath()).getAbsolutePath());
						}
						c = c.getParent();
					}
				}
		}

		return Joiner.on(File.pathSeparator).join(classpaths);
	}

	@Override
	public Set<SourceVersion> getSourceVersions() {
		return compiler.getSourceVersions();
	}

	@Override
	public StandardJavaFileManager getStandardFileManager(DiagnosticListener<? super JavaFileObject> diagnosticListener, Locale locale, Charset charset) {
		return compiler.getStandardFileManager(diagnosticListener, locale, charset);
	}

	@Override
	public CompilationTask getTask(Writer out, JavaFileManager fileManager, DiagnosticListener<? super JavaFileObject> diagnosticListener, Iterable<String> options, Iterable<String> classes, Iterable<? extends JavaFileObject> compilationUnits) {
		return compiler.getTask(out, fileManager, diagnosticListener, options, classes, compilationUnits);
	}

	@Override
	public int isSupportedOption(String option) {
		return compiler.isSupportedOption(option);
	}

	@Override
	public int run(InputStream in, OutputStream out, OutputStream err, String... arguments) {
		List<String> args = new ArrayList<String>();
		args.add("-classpath");
		args.add(getClasspath());
		for (String arg : arguments) {
			args.add(arg);
		}

		if (compiler == null) {
			System.out.print("compiler null");
		}
		if (args == null) {
			System.out.print("args null");
		}
		return compiler.run(in, out, err, args.toArray(new String[args.size()]));
	}
}
