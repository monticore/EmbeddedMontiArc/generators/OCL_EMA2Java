/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.codegen.visitors;

import de.monticore.lang.montiarc.literals2._ast.ASTLiteral;
import de.monticore.lang.ocl.codegen.OCLVariableNaming;
import de.monticore.symboltable.Scope;
import de.monticore.symboltable.types.JTypeSymbol;
import de.monticore.types.types._ast.ASTImportStatement;
import de.se_rwth.commons.Names;
import ocl.monticoreocl.ocl._ast.*;
import ocl.monticoreocl.ocl._visitor.OCLVisitor;

import java.util.Iterator;
import java.util.Optional;


/**
 * Visitor that creates the MA specific "inplace" output for ASTOCLNodes.
 *
 */
public class OCLEMA2JavaInplaceVisitor extends OCL2JavaInplaceVisitor {

	private StringBuilder sb;
	private OCLVariableNaming varNaming;
	private OCLEMA2JavaDeclarationVisitor declarationVisitor;
	private boolean embeddedmontiarc;
	private boolean embeddedmontiview;
	private boolean nfp;

	public OCLEMA2JavaInplaceVisitor(StringBuilder sb, boolean embeddedmontiarc, boolean embeddedmontiview, boolean nfp) {
		super(sb);
		this.sb = sb;
		varNaming = OCLVariableNaming.getInstance();
		declarationVisitor = new OCLEMA2JavaDeclarationVisitor(this.sb, varNaming, nfp, this);
		super.setDeclarationVisitor(declarationVisitor);
		this.embeddedmontiarc = embeddedmontiarc;
		this.embeddedmontiview = embeddedmontiview;
		this.nfp = nfp;
	}

	private OCLVisitor realThis = this;

	@Override
	public OCLVisitor getRealThis() {
		return realThis;
	}

	@Override
	public void traverse(ASTCompilationUnit node) {
		sb.append("package ").append(Names.getQualifiedName(node.getPackage())).append(";\n\n");

		for (ASTImportStatement astImportStatement : node.getImportStatements()) {
			String importString = Names.getQualifiedName(astImportStatement.getImportList());
			Scope scope = node.getOCLFile().getEnclosingScope().get();
			Optional<JTypeSymbol> javaType = scope.resolve(importString, JTypeSymbol.KIND);
			if (javaType.isPresent()) {
				sb.append("import static ");
			} else {
				sb.append("import ");
			}
			sb.append(importString);
			if(astImportStatement.isStar()) {
				sb.append(".*");
			}
			sb.append(";\n");
		}
		sb.append("import de.monticore.symboltable.*;\n");
		sb.append("import de.monticore.symboltable.types.*;\n");
		sb.append("import de.monticore.symboltable.types.references.*;\n");

		sb.append("import org.jscience.physics.amount.Amount;\n");
		sb.append("import javax.measure.unit.*;\n");
		sb.append("import javax.measure.quantity.*;\n");
		sb.append("import java.util.*;\n");
		sb.append("import java.lang.*;\n");

		if(nfp) {
			sb.append("import de.monticore.lang.ocl.nfp.EMANFPWitness;\n");
		}

		if(embeddedmontiarc || embeddedmontiview) {
			sb.append("import de.monticore.lang.ocl.codegen.OCLEMAHelper;\n");
			sb.append("import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.*;\n");
			sb.append("import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol;\n");
			sb.append("import de.monticore.lang.embeddedmontiview.embeddedmontiview._symboltable.*;\n");

			sb.append("\n");
		}

		node.getOCLFile().accept(getRealThis());
	}

	@Override
	public void traverse(ASTOCLFile node) {
		sb.append("public class ").append(node.getFileName()).append(" {\n");
		node.getOclConstraints().get(0).accept(getRealThis());
		sb.append("}");
	}

	@Override
	public void traverse(ASTOCLInvariant node) {

		if (nfp) {
			sb.append("\n" +
					"static HashMap<String,EMANFPWitness> positiveWitness = new HashMap<>();\n" +
					"static HashMap<String,EMANFPWitness> negativeWitness = new HashMap<>();\n" +
					"public static HashMap<String,EMANFPWitness> getWitnessMap() {\n" +
					"        HashMap<String, EMANFPWitness> witnesses = new HashMap<>();\n" +
					"        witnesses.putAll(positiveWitness);\n" +
					"        witnesses.putAll(negativeWitness);\n" +
					"        return witnesses;" +
					"}\n\n");
		}

		super.traverse(node);

		if(node.oCLClassContextIsPresent()) {
			if(embeddedmontiarc) {
				if(embeddedmontiview) {
					sb.append("public static boolean check(GlobalScope modelSymTab, GlobalScope viewSymTab) {\n");
				} else {
					sb.append("public static boolean check(GlobalScope symtab) {\n");
				}
				sb.append("boolean ").append(varNaming.getName(node)).append(" = true;\n");
				sb.append("try {\n");
				for(ASTOCLContextDefinition contextDef : node.getOCLClassContext().get().getContextDefinitions()) {
					sb.append("for (").append(contextDef.getClassName().toString()).append(" ");
					if(contextDef.nameIsPresent()) {
						sb.append(contextDef.getName().get());
					} else {
						sb.append(varNaming.getName(contextDef));
					}
					sb.append(" : ");
					if(embeddedmontiview) {
						sb.append("OCLEMAHelper.").append("<" + contextDef.getClassName().toString() + ">").append("getAllSymbolInstances(modelSymTab, viewSymTab, ").append(contextDef.getClassName().toString()).append(".KIND)) {\n");
					} else {
						sb.append("OCLEMAHelper.").append("<" + contextDef.getClassName().toString() + ">").append("getAllSymbolInstances(symtab, ").append(contextDef.getClassName().toString()).append(".KIND)) {\n");
					}
				}

				sb.append(varNaming.getName(node)).append(" &= _ocl");
				if(node.nameIsPresent()) {
					sb.append(node.getName().get());
				} else {
					//sb.append(varNaming.getName(node));
				}
				sb.append("(");
				Iterator<ASTOCLContextDefinition> contextDefinitions = node.getOCLClassContext().get().getContextDefinitions().iterator();
				while(contextDefinitions.hasNext()) {
					ASTOCLContextDefinition next = contextDefinitions.next();
					if(next.nameIsPresent()) {
						sb.append(next.getName().get());
					} else {
						sb.append(varNaming.getName(next));
					}
					if(contextDefinitions.hasNext()) {
						sb.append(", ");
					}
				}
				sb.append(");\n");
				for(ASTOCLContextDefinition contextDef : node.getOCLClassContext().get().getContextDefinitions()) {
					sb.append("}\n");
				}

				sb.append("} catch (Exception ").append(varNaming.getName(node)).append("LiftingException) {\n");
				sb.append(varNaming.getName(node)).append(" = false;\n");
				sb.append("}\n");

				if(nfp) {
					sb.append("if(").append(varNaming.getName(node)).append(") {\n" +
							"negativeWitness.clear();\n" +
							"}\n");
				}

				sb.append("return ").append(varNaming.getName(node)).append(";\n");
				sb.append("}\n");
			} else {
				//TODO
			}
		}
	}

}
