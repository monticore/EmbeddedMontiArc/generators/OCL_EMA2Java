/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.codegen;

import de.monticore.ModelingLanguageFamily;
import de.monticore.io.paths.ModelPath;
import de.monticore.java.lang.JavaDSLLanguage;


import de.monticore.lang.ocl.codegen.modifications.RewriteConfTransformation;
import de.monticore.lang.ocl.codegen.visitors.OCLEMA2JavaInplaceVisitor;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.ResolvingConfiguration;
import de.se_rwth.commons.Splitters;
import ocl.monticoreocl.ocl._ast.ASTCompilationUnit;

import de.se_rwth.commons.logging.Log;
import ocl.monticoreocl.ocl._symboltable.OCLLanguage;
import ocl.monticoreocl.ocl._symboltable.OCLSymbolTableCreator;
import org.antlr.v4.runtime.RecognitionException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;


public class OCLEMA2Java extends OCL2Java{

	private boolean embeddedmontiarc;
	private boolean embeddedmontiview;
	private boolean nfp;

	public OCLEMA2Java(boolean embeddedmontiarc, boolean embeddedmontiview, boolean nfp) {
		super();
		this.embeddedmontiarc = embeddedmontiarc;
		this.embeddedmontiview = embeddedmontiview;
		this.nfp = nfp;
	}

	public void generate(Path parentDir, String ocl, Path target) {

		List<String> oclQualifiedNames = Splitters.DOT.splitToList(ocl);
		String oclConstraintName = oclQualifiedNames.get(oclQualifiedNames.size()-1);
		Path rewriteFile =  Paths.get(target.toString() + File.separator + oclConstraintName + "_rwr.ocl");
		Path outputFile = Paths.get(target.toString() + File.separator + oclConstraintName + ".java");

		// making folders and files
		// then rewrite input file with conf
		try {
			Path tmp = outputFile.getParent();
			if (tmp != null) {
				Files.createDirectories(tmp);
			}
			if (Files.notExists(outputFile)) {
				Files.createFile(outputFile);
			}
			if (Files.notExists(rewriteFile)) {
				Files.createFile(rewriteFile);
			}
			Log.debug("Rewriting OCL File", this.getClass().getName());
			RewriteConfTransformation rewriter = new RewriteConfTransformation(parentDir, ocl, rewriteFile);
			rewriter.rewrite();
		} catch (IOException e) {
			Log.error(e.getMessage());
		}

		StringBuilder sb = new StringBuilder();
		Log.debug("Parsing rewritten OCL File", this.getClass().getName());
		// parse ocl file
		try {
			Optional<ASTCompilationUnit> root = parse(target, rewriteFile);
			if (root.isPresent()) {
				Log.debug("Generating Java code from OCL", this.getClass().getName());
				ASTCompilationUnit ast = root.get();

				// run pretransformations
				super.runPretransformations(ast);
				// generate java code with a visitor
				OCLEMA2JavaInplaceVisitor inplaceVisitor = new OCLEMA2JavaInplaceVisitor(sb, embeddedmontiarc, embeddedmontiview, nfp);
				ast.accept(inplaceVisitor);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// create java output file and write java source code
		try {


			Files.write(outputFile, sb.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		OCLVariableNaming.getInstance().reset();
	}

	public Optional<ASTCompilationUnit> parse(Path parentDir, Path oclFile) throws RecognitionException, IOException {
		ModelPath modelPath = new ModelPath(parentDir, Paths.get("src/main/resources/defaultTypes"), Paths.get(parentDir.toAbsolutePath().toString() + File.separator + "defaultTypes/"));
		OCLLanguage oclLanguage = new OCLLanguage();
		String oclConstraintName = oclFile.getFileName().toString().replaceAll(".ocl", "");
		Optional<ASTCompilationUnit> astCompilationUnit = oclLanguage.getModelLoader().loadModel(oclConstraintName, modelPath);

		ModelingLanguageFamily modelingLanguageFamily = new ModelingLanguageFamily();
		modelingLanguageFamily.addModelingLanguage(oclLanguage);
		modelingLanguageFamily.addModelingLanguage(new JavaDSLLanguage());
		GlobalScope globalScope = new GlobalScope(modelPath, modelingLanguageFamily);

		OCLSymbolTableCreator symbolTableCreator = oclLanguage.getSymbolTableCreator(
				new ResolvingConfiguration(), globalScope).get();

		astCompilationUnit.get().accept(symbolTableCreator);

		return astCompilationUnit;
	}

	public void generate(String parentDirectory, String modelPath) {
		Path outputFile = Paths.get("./target/" + modelPath + ".java");
		StringBuilder sb = new StringBuilder();
		Log.debug("Parsing OCL File", this.getClass().getName());
		// parse ocl file
		try {
			Optional<ASTCompilationUnit> root = parse(parentDirectory, modelPath);
			if (root.isPresent()) {
				ASTCompilationUnit ast = root.get();

				// run pretransformations
				super.runPretransformations(ast);
				// generate java code with a visitor
				OCLEMA2JavaInplaceVisitor inplaceVisitor = new OCLEMA2JavaInplaceVisitor(sb, embeddedmontiarc, embeddedmontiview, nfp);
				ast.accept(inplaceVisitor);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// create java output file and write java source code
		try {
			Log.debug("Generating Java code from OCL", this.getClass().getName());
			Path tmp = outputFile.getParent();
			if(tmp != null) {
				Files.createDirectories(tmp);
			}
			if (Files.notExists(outputFile)) {
				Files.createFile(outputFile);
			}
			Files.write(outputFile, sb.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		OCLVariableNaming.getInstance().reset();
	}

	public Optional<ASTCompilationUnit> parse(String parentDirectory, String modelPath)
			throws RecognitionException, IOException {
		RewriteConfTransformation rewriter = new RewriteConfTransformation(parentDirectory, modelPath);
		rewriter.rewrite();

		modelPath = modelPath + "_rwr";
		if(modelPath.startsWith("/"))
			modelPath = modelPath.substring(1);

		ModelPath parentModelPath = new ModelPath(Paths.get("./target/"), Paths.get(parentDirectory), Paths.get("src/main/resources/defaultTypes"));
		OCLLanguage oclLanguage = new OCLLanguage();
		Optional<ASTCompilationUnit> astCompilationUnit = oclLanguage.getModelLoader().loadModel(modelPath, parentModelPath);

		ModelingLanguageFamily modelingLanguageFamily = new ModelingLanguageFamily();
		modelingLanguageFamily.addModelingLanguage(oclLanguage);
		modelingLanguageFamily.addModelingLanguage(new JavaDSLLanguage());
		GlobalScope globalScope = new GlobalScope(parentModelPath, modelingLanguageFamily);

		OCLSymbolTableCreator symbolTableCreator = oclLanguage.getSymbolTableCreator(
				new ResolvingConfiguration(), globalScope).get();

		astCompilationUnit.get().accept(symbolTableCreator);

		return astCompilationUnit;
	}
}


