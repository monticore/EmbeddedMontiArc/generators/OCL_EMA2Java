/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.codegen;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.symboltable.Scope;
import de.monticore.symboltable.ScopeSpanningSymbol;
import de.monticore.symboltable.Symbol;
import de.monticore.symboltable.SymbolKind;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;


public class OCLEMAHelper extends OCLHelper{
	
	/**
	 * Get all symbol instances of a given ViewComponentSymbol
	 *
	 */
	public static <T extends Symbol> Collection<T> getAllSymbolInstances(Scope scope, SymbolKind kind) {
		// get all instances in local scope
		LinkedHashSet<T> instances = new LinkedHashSet<>(scope.resolveLocally(kind));
		
		// check all subscopes
		for(Scope sub : scope.getSubScopes()) {
			instances.addAll(getAllSymbolInstances(sub, kind));
		}
		
		return instances;
	}

	/**
	 * Get all symbol instances of a given ViewComponentSymbol
	 * and try to look in view if nothing found in model
	 *
	 */
	public static <T extends Symbol> Collection<T> getAllSymbolInstances(Scope modelScope, Scope viewScope, SymbolKind kind) {
		// get all instances in local scope
		LinkedHashSet<T> instances = new LinkedHashSet<>(modelScope.resolveLocally(kind));

		// check all subscopes
		for(Scope sub : modelScope.getSubScopes()) {
			instances.addAll(getAllSymbolInstances(sub, kind));
		}

		if (instances.isEmpty()) {
			instances = new LinkedHashSet<>(viewScope.resolveLocally(kind));

			// check all subscopes
			for(Scope sub : viewScope.getSubScopes()) {
				instances.addAll(getAllSymbolInstances(sub, kind));
			}
		}
		return instances;
	}

	/**
	 * Get all connectors and simple connectors of a given ViewComponentSymbol
	 *
	 */
	public static List<ConnectorSymbol> getAllConnectors(ComponentSymbol cmp) {
		List<ConnectorSymbol> connectors = new ArrayList<>();

		connectors.addAll(cmp.getConnectors());
		
		for(ComponentInstanceSymbol instance : cmp.getSubComponents()) {
			connectors.addAll(instance.getSimpleConnectors());
		}
		
		return connectors;
	}
	
	public static List<Symbol> getSymbols(ScopeSpanningSymbol symbol) {
		return symbol.getSpannedScope().getLocalSymbols().values().stream().flatMap(c -> c.stream()).collect(Collectors.toList());
	}

	/**
	 *
	 * @param symTab Symboltable of a MA model
	 * @return 	The root component instance of the model
	 */
	public static ExpandedComponentInstanceSymbol getRootCompInstance(Scope symTab) {
		Collection<ExpandedComponentInstanceSymbol> col = OCLEMAHelper.getAllSymbolInstances(symTab, ExpandedComponentInstanceSymbol.KIND);
		return col.iterator().next();
	}
}
