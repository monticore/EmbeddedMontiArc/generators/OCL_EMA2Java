/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;


import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.*;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;
import de.se_rwth.commons.logging.Log;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.Collection;
import java.util.Iterator;

public class NFPHelperGraphHelper {

    public static DirectedGraph<ElementInstance, DefaultEdge> buildGraph(ExpandedComponentInstanceSymbol rootCmp) {

        Scope spannedScope = rootCmp.getSpannedScope();
        DirectedGraph<ElementInstance, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);

        Collection allCompInst = OCLEMAHelper.<ExpandedComponentInstanceSymbol>getAllSymbolInstances(spannedScope, ExpandedComponentInstanceSymbol.KIND);
        allCompInst.add(rootCmp);


        // Add Vertices
        for (Iterator<ElementInstance> itr = allCompInst.iterator(); itr.hasNext();){
            ExpandedComponentInstanceSymbol cmp =  (ExpandedComponentInstanceSymbol) itr.next();
            Log.debug("Adding Vertex CmpInst " + cmp.getName(), "buildGraph");
            graph.addVertex(cmp);
            Collection allPorts = cmp.getPorts();
            for (Iterator<ElementInstance> portItr = allPorts.iterator(); portItr.hasNext();) {
                PortSymbol p = (PortSymbol) portItr.next();
                Log.debug("Adding Vertex PortInst " + p.getName(), "buildGraph");
                graph.addVertex(p);
            }
            Collection allConns = cmp.getConnectors();
            for (Iterator<ElementInstance> connItr = allConns.iterator(); connItr.hasNext();) {
                ConnectorSymbol c = (ConnectorSymbol) connItr.next();
                Log.debug("Adding Vertex ConnInst " + c.getName(), "buildGraph");
                graph.addVertex(c);
            }
        }

        // Add Edges
        for (Iterator<ElementInstance> itr = allCompInst.iterator(); itr.hasNext();){
            ExpandedComponentInstanceSymbol cmp =  (ExpandedComponentInstanceSymbol) itr.next();
            // if the cmp is atomic or as non connected sub comps
            if (cmp.getConnectors().isEmpty()) {
                // Add Edges form cmp to all outgoing ports of cmp
                Collection allPorts = cmp.getPorts();
                for (Iterator<ElementInstance> portItr = allPorts.iterator(); portItr.hasNext(); ) {
                    PortSymbol p = (PortSymbol) portItr.next();
                    if (p.isOutgoing()) {
                        Log.debug("Adding Edge CompInst -> PortInst " + cmp.getName() + " , " + p.getName(), "buildGraph");
                        graph.addEdge(cmp, p);
                    } else {
                        Log.debug("Adding Edge PortInst -> CompInst " + p.getName() + " , " + cmp.getName(), "buildGraph");
                        graph.addEdge(p, cmp);
                    }
                }
                // Add Edges form cmp to all ingoing ports of all subCmps
                Collection allSubCmps = cmp.getSubComponents();
                for (Iterator<ElementInstance> subsItr = allSubCmps.iterator(); subsItr.hasNext(); ) {
                    ExpandedComponentInstanceSymbol sub = (ExpandedComponentInstanceSymbol) subsItr.next();
                    Collection subPorts = sub.getPorts();
                    for (Iterator<ElementInstance> portItr = subPorts.iterator(); portItr.hasNext(); ) {
                        PortSymbol p = (PortSymbol) portItr.next();
                        if (p.isIncoming()) {
                            Log.debug("Adding Edge CompInst -> SubPortInst " + cmp.getName() + " , " + p.getName(), "buildGraph");
                            graph.addEdge(cmp, p);
                        } else {
                            Log.debug("Adding Edge SubPortInst -> CompInst " + p.getName() + " , " + cmp.getName(), "buildGraph");
                            graph.addEdge(p, cmp);
                        }
                    }
                }
            }
            // Add Edges from connector to target port and source port to connector
            Collection allConns = cmp.getConnectors();
            for (Iterator<ElementInstance> connItr = allConns.iterator(); connItr.hasNext(); ) {
                ConnectorSymbol con = (ConnectorSymbol) connItr.next();
                Log.debug("Adding Edge ConnInst -> PortInst " + con.getName() + " , " + con.getTarget(), "buildGraph");
                graph.addEdge(con, con.getTargetPort());
                Log.debug("Adding Edge PortInst -> ConnInst " + con.getSource() + " , " + con.getName(), "buildGraph");
                graph.addEdge(con.getSourcePort(), con);

            }
        }

        return graph;
    }

}
