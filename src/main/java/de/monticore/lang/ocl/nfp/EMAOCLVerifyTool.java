/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;


import de.se_rwth.commons.logging.Log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Created by kt on 03.07.2017.
 */
public class EMAOCLVerifyTool {

    public static void main(String[] args) {

        if (args.length < 4 || args.length > 5) {
            System.out.println("How to use:");
            System.out.println("input 4 variables");
            System.out.println("1: Parent directory as absolute path to the projects folder");
            System.out.println("2: Qualified name to model");
            System.out.println("3: Qualified name to ocl");
            System.out.println("4: Target directory as absolute path");
            System.out.println("Example:");
            System.out.println("java -jar oclVerifyTool \"C:\\path\\to\\my\\project\\\"" +
                    " \"model.weatherBalloon.WeatherBalloonSensors\"" +
                    " \"ocl.rule3_WCET_SingleCore\" " +
                    "\"C:\\path\\to\\target\\\" ");
            return;
        }

        String parentDir = args[0];
        System.out.println("ParentDir loaded as: " + parentDir);
        String modelCmp = args[1];
        System.out.println("Model loaded as: " + modelCmp);
        String ocl = args[2];
        System.out.println("OCL loaded as: " + ocl);
        String target = args[3];
        System.out.println("Target loaded as: " + target);

        Boolean res;
        EMANFPVerificator verificator = new EMANFPVerificator();
        if(args.length==5) {
            String viewCmp = args[4];
            System.out.println("View loaded as: " + viewCmp);
            res = verificator.verificateNFP(Paths.get(parentDir), modelCmp, viewCmp, ocl, Paths.get(target));
        }
        else {
            res = verificator.verificateNFP(Paths.get(parentDir), modelCmp, ocl, Paths.get(target));
        }
        HashMap<String, EMANFPWitness> witnesses = verificator.getWitnessMap();

        // 3 ocl beispiel paper
        // ocl zweites version

        System.out.println("The constraint is: " + res);

        try {
            if(witnesses.size() > 0) {
                Path witnessPath = Paths.get(target + File.separator + "witnesses_" + ocl + "_" + modelCmp);
                if (!witnessPath.toFile().exists()) {
                    Files.createDirectories(witnessPath);
                }


                StringBuilder sb = new StringBuilder();
                for (String key : witnesses.keySet()) {
                    EMANFPWitness witness = witnesses.get(key);
                    sb.append("key: ").append(key).append("\t\t\t isPositiv: ").append(witness.isPositive);
                    //if (witness.aggregatedValues.size() > ) {
                            sb.append("\t\t\t aggregatedValue: ").append(witness.aggregatedValues);
                    //} else if (witness.aggregatedValuesList.isPresent()) {
                    //    sb.append("\t\t\t aggregatedValue: ").append(witness.aggregatedValuesList.get());
                    //}
                    sb.append("\n");
                }
                Files.write(Paths.get(witnessPath + File.separator + "__WITNESS_OVERVIEW__.txt"), sb.toString().getBytes());

                for (String key : witnesses.keySet()) {
                    String witness = witnesses.get(key).toString();
                    //if (countLines(witness) > 4){
                    Files.write(Paths.get(witnessPath + File.separator + key + ".ema"), witness.getBytes());
                    //}
                }


                System.out.println("The witnesses are written to: target/witnesses_" + ocl + "_" + modelCmp);

            }
        } catch (IOException e) {
            Log.error(e.getMessage());
        }
    }


    public static int countLines(String str) {
        if(str == null || str.isEmpty())
        {
            return 0;
        }
        int lines = 1;
        int pos = 0;
        while ((pos = str.indexOf("\n", pos) + 1) != 0) {
            lines++;
        }
        return lines;
    }

}
