/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ElementInstance;
import org.jscience.physics.amount.Amount;

import java.util.*;

/**
 * Created by Ferdinand Mehlan on 14.05.2017.
 */
public class EMANFPWitness {

    Boolean isPositive;
    List<Amount> aggregatedValues = new LinkedList<>();
    Optional<ElementInstance> singleWitness = Optional.empty();
    Collection<? extends ElementInstance> graphWitness = new HashSet<>();
    List<List<ElementInstance>> pathsWitness = new LinkedList<>();

    public EMANFPWitness(ElementInstance singleWitness, Amount aggregatedValue, Boolean isPositive) {
        this.isPositive = isPositive;
        this.aggregatedValues.add(aggregatedValue);
        this.singleWitness = Optional.of(singleWitness);
    }

    public EMANFPWitness(Collection<? extends ElementInstance> graphWitness, Amount aggregatedValue, Boolean isPositive) {
        this.isPositive = isPositive;
        this.aggregatedValues.add(aggregatedValue);
        this.graphWitness = graphWitness;
    }

    public EMANFPWitness(List<List<ElementInstance>> pathsWitness, Amount aggregatedValue, Boolean isPositive) {
        this.isPositive = isPositive;
        this.aggregatedValues.add(aggregatedValue);
        this.pathsWitness = pathsWitness;
    }

    public EMANFPWitness(ElementInstance singleWitness, List<Amount> aggregatedValues, Boolean isPositive) {
        this.isPositive = isPositive;
        this.aggregatedValues = aggregatedValues;
        this.singleWitness = Optional.of(singleWitness);
    }

    public EMANFPWitness(Collection<? extends ElementInstance> graphWitness, List<Amount> aggregatedValues, Boolean isPositive) {
        this.isPositive = isPositive;
        this.aggregatedValues = aggregatedValues;
        this.graphWitness = graphWitness;
    }

    public EMANFPWitness(List<List<ElementInstance>> pathsWitness, List<Amount> aggregatedValues, Boolean isPositive) {
        this.isPositive = isPositive;
        this.aggregatedValues = aggregatedValues;
        this.pathsWitness = pathsWitness;
    }

    public String toString() {
        String res = "";

        res += "// ";
        res += isPositive ? "positive " : "negative ";
        res += "\n";

        if (aggregatedValues.size() == 1)
            res += "// aggregated value in model: " + aggregatedValues.get(0) + "\n\n";

        if (aggregatedValues.size() > 1)
            res += "// aggregated values in model: " + aggregatedValues + "\n\n";


        if (singleWitness.isPresent()) {
            ElementInstance witness = singleWitness.get();

            if (witness instanceof ComponentSymbol) {
                res += EMAWitnessPrinter.printComponent((ComponentSymbol) witness);
            }
        } else if (graphWitness.size() > 0) {
            res += EMAWitnessPrinter.printElementSet(graphWitness);
        } else if (pathsWitness.size() > 0) {

        }

        return res;
    }
}
