/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;


import de.monticore.ModelingLanguageFamily;
import de.monticore.io.paths.ModelPath;
import de.monticore.java.lang.JavaDSLLanguage;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.EmbeddedMontiArcLanguage;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiview.embeddedmontiview._symboltable.EmbeddedMontiViewLanguage;
import de.monticore.lang.embeddedmontiview.embeddedmontiview._symboltable.ViewComponentSymbol;
import de.monticore.lang.embeddedmontiview.embeddedmontiview._symboltable.ViewSymbol;
import de.monticore.lang.ocl.codegen.OCLEMA2Java;
import de.monticore.lang.ocl.codegen.compilation.CustomCompiler;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;
import de.se_rwth.commons.Splitters;
import de.se_rwth.commons.logging.Log;
import nfp.LatencyTagSchema.LatencyTagSchema;
import nfp.PowerTagSchema.PowerTagSchema;

import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by kt on 30.12.2016.
 */
public class EMANFPVerificator {

    private HashMap<String,EMANFPWitness> witnessMap = new HashMap();

    public HashMap<String,EMANFPWitness> getWitnessMap() {
        return witnessMap;
    }

    public boolean verificateNFP(Path parentDir, String modelCmp, String ocl, Path target) {
        // Parse model
        GlobalScope modelSymTab = createModelSymTab(parentDir, modelCmp);

        // transform ocl model to java code
        OCLEMA2Java generator = new OCLEMA2Java(true, true, true);
        List<String> oclQualifiedNames = Splitters.DOT.splitToList(ocl);
        String oclConstraintName = oclQualifiedNames.get(oclQualifiedNames.size()-1);
        Path oclJavaFile = Paths.get(target.toString() + File.separator + oclConstraintName + ".java");
        generator.generate(parentDir, ocl, target);

        // remove pakage information beacaus target doesnt neccesary have the right hierachy
        fixPackage(oclJavaFile);
        // compile java source code
        Log.debug("Compiling Java code", this.getClass().getName());
        if (ToolProvider.getSystemJavaCompiler()==null) {
            System.out.println("ERROR: system java compiler null");
        }
        if (Thread.currentThread().getContextClassLoader() == null) {
            System.out.println("ERROR: context loader null");
        }
        CustomCompiler compiler = new CustomCompiler(ToolProvider.getSystemJavaCompiler(), Thread.currentThread().getContextClassLoader());
        int compilationResult = compiler.run(null, null, null, new String[] {oclJavaFile.toString()});
        if (compilationResult != 0) {
            System.out.println("Compilation of " + oclJavaFile.getFileName().toString() + " failed.");
            return false;
        }

        // load and instantiate compiled class
        boolean result = false;

        try {
            URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {target.toUri().toURL()});

            Log.debug("Loading class ", this.getClass().getName());
            String classLoaderString = oclJavaFile.toFile().getName().replaceAll(".java", "");
            Class<?> loadedClass = classLoader.loadClass(classLoaderString);
            Log.debug("Loaded class: " + loadedClass, this.getClass().getName());
            Object classObj = loadedClass.newInstance();
            Log.debug("Instanciate class: " + classObj, this.getClass().getName());
            // execute check method
            Method checkMethod = classObj.getClass().getMethod("check", GlobalScope.class, GlobalScope.class);
            Object res = checkMethod.invoke(classObj, modelSymTab, modelSymTab);
            result = (boolean)res;
            Method witnessmethod = classObj.getClass().getMethod("getWitnessMap");
            Object res2 = witnessmethod.invoke(classObj);
            witnessMap = (HashMap<String,EMANFPWitness>)res2;
        } catch(Exception e) {
            Log.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    public boolean verificateNFP(Path parentDir, String modelCmp, String viewCmp, String ocl, Path target) {
        // Parse model
        GlobalScope modelSymTab = createModelSymTab(parentDir, modelCmp);
        GlobalScope viewSymTab = createViewSymTab(parentDir, viewCmp);

        // transform ocl model to java code
        OCLEMA2Java generator = new OCLEMA2Java(true, true, true);
        List<String> oclQualifiedNames = Splitters.DOT.splitToList(ocl);
        String oclConstraintName = oclQualifiedNames.get(oclQualifiedNames.size()-1);
        Path oclJavaFile = Paths.get(target.toString() + File.separator + oclConstraintName + ".java");
        generator.generate(parentDir, ocl, target);

        // remove pakage information beacaus target doesnt neccesary have the right hierachy
        fixPackage(oclJavaFile);
        // compile java source code
        Log.debug("Compiling Java code", this.getClass().getName());
        if (ToolProvider.getSystemJavaCompiler()==null) {
            System.out.println("ERROR: system java compiler null");
        }
        if (Thread.currentThread().getContextClassLoader() == null) {
            System.out.println("ERROR: context loader null");
        }
        CustomCompiler compiler = new CustomCompiler(ToolProvider.getSystemJavaCompiler(), Thread.currentThread().getContextClassLoader());
        int compilationResult = compiler.run(null, null, null, new String[] {oclJavaFile.toString()});
        if (compilationResult != 0) {
            System.out.println("Compilation of " + oclJavaFile.getFileName().toString() + " failed.");
            return false;
        }

        // load and instantiate compiled class
        boolean result = false;

        try {
            URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {target.toUri().toURL()});

            Log.debug("Loading class ", this.getClass().getName());
            String classLoaderString = oclJavaFile.toFile().getName().replaceAll(".java", "");
            Class<?> loadedClass = classLoader.loadClass(classLoaderString);
            Log.debug("Loaded class: " + loadedClass, this.getClass().getName());
            Object classObj = loadedClass.newInstance();
            Log.debug("Instanciate class: " + classObj, this.getClass().getName());
            // execute check method
            Method checkMethod = classObj.getClass().getMethod("check", GlobalScope.class, GlobalScope.class);
            Object res = checkMethod.invoke(classObj, modelSymTab, viewSymTab);
            result = (boolean)res;
            Method witnessmethod = classObj.getClass().getMethod("getWitnessMap");
            Object res2 = witnessmethod.invoke(classObj);
            witnessMap = (HashMap<String,EMANFPWitness>)res2;
        } catch(Exception e) {
            Log.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    // Removes first line
    private void fixPackage(Path output) {
        try {
            File file = output.toFile();
            Scanner scanner = new Scanner(file);
            ArrayList<String> coll = new ArrayList<String>();
            scanner.nextLine();
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                coll.add(line);
            }

            scanner.close();

            coll.remove(0);

            FileWriter writer = new FileWriter(file);
            for (String line : coll) {
                writer.write(line);
            }

            writer.close();
        } catch (IOException e) {
            Log.error(e.getMessage());
        }
    }

    public void printWitnessMap(String target, String modelCmp, String ocl){
        HashMap<String, EMANFPWitness> witnesses = getWitnessMap();

        try {
            if(witnesses.size() > 0) {
                Path witnessPath = Paths.get(target + File.separator + "witnesses_" + ocl + "_" + modelCmp);
                if (!witnessPath.toFile().exists()) {
                    Files.createDirectories(witnessPath);
                }


                StringBuilder sb = new StringBuilder();
                for (String key : witnesses.keySet()) {
                    EMANFPWitness witness = witnesses.get(key);
                    sb.append("key: ").append(key).append("\t\t\t isPositiv: ").append(witness.isPositive);
                    //if (witness.aggregatedValue.isPresent()) {
                        sb.append("\t\t\t aggregatedValue: ").append(witness.aggregatedValues);
                    //} else if (witness.aggregatedValuesList.isPresent()) {
                    //    sb.append("\t\t\t aggregatedValue: ").append(witness.aggregatedValuesList.get());
                    //}
                    sb.append("\n");
                }
                Files.write(Paths.get(witnessPath + File.separator + "__WITNESS_OVERVIEW__.txt"), sb.toString().getBytes());

                for (String key : witnesses.keySet()) {
                    String witness = witnesses.get(key).toString();
                    //if (countLines(witness) > 4){
                    Files.write(Paths.get(witnessPath + File.separator + key + ".ema"), witness.getBytes());
                    //}
                }


                System.out.println("The witnesses are written to: target/witnesses_" + ocl + "_" + modelCmp);

            }
        } catch (IOException e) {
            Log.error(e.getMessage());
        }
    }


    public static GlobalScope createModelSymTab(Path maPath, String cmpName) {
        ModelingLanguageFamily fam = new ModelingLanguageFamily();
        EmbeddedMontiArcLanguage montiArcLanguage = new EmbeddedMontiArcLanguage();

        // ToDo: register tagschemas here
        LatencyTagSchema.registerTagTypes(montiArcLanguage);
        PowerTagSchema.registerTagTypes(montiArcLanguage);

        fam.addModelingLanguage(montiArcLanguage);
        fam.addModelingLanguage(new JavaDSLLanguage());
        final ModelPath maMp = new ModelPath(maPath, Paths.get("src/main/resources/defaultTypes"), Paths.get(maPath.toAbsolutePath().toString() + File.separator + "defaultTypes/"));
        GlobalScope scope = new GlobalScope(maMp, fam);

        // resolve all referenced ComponentSymbols
        Optional<de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol> cmp = scope.<de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol>resolve(cmpName, de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol.KIND);
        Optional<ExpandedComponentInstanceSymbol> cmpInst = scope.<ExpandedComponentInstanceSymbol>resolve(cmpName, ExpandedComponentInstanceSymbol.KIND);

        if (cmp.isPresent()) {
            resolveComponentInstances(cmp.get().getSpannedScope());
        }
        if(cmpInst.isPresent()) {
            resolveComponentInstances(cmpInst.get().getEnclosingScope().getSubScopes().get(0));
        }

        return scope;
    }

    public static GlobalScope createViewSymTab(Path viewPath, String viewCmpName) {
        ModelingLanguageFamily fam = new ModelingLanguageFamily();
        EmbeddedMontiViewLanguage montiViewLanguage = new EmbeddedMontiViewLanguage();

        // ToDo: register tagschemas here
        LatencyTagSchema.registerTagTypes(montiViewLanguage);
        PowerTagSchema.registerTagTypes(montiViewLanguage);

        fam.addModelingLanguage(montiViewLanguage);
        fam.addModelingLanguage(new JavaDSLLanguage());
        final ModelPath viewMp = new ModelPath(viewPath, Paths.get("src/main/resources/defaultTypes"), Paths.get(viewPath.toAbsolutePath().toString() + File.separator + "defaultTypes/"));
        GlobalScope scope = new GlobalScope(viewMp, fam);

        // resolve all referenced ComponentSymbols
        Optional<ViewSymbol> cmp = scope.<ViewSymbol>resolve(viewCmpName, ViewSymbol.KIND);
        if(cmp.isPresent()) {
            resolveViewInstances(cmp.get().getSpannedScope());
        }
        return scope;
    }

    private static void resolveComponentInstances(Scope scope) {
        Collection<ExpandedComponentInstanceSymbol> instances = scope.resolveLocally(ExpandedComponentInstanceSymbol.KIND);

        for(ExpandedComponentInstanceSymbol instance : instances) {
            resolveComponentInstances(instance.getComponentType().getReferencedSymbol().getSpannedScope());
        }
    }

    private static void resolveViewInstances(Scope scope) {
        Collection<ViewComponentSymbol> instances = scope.resolveLocally(ViewComponentSymbol.KIND);

        for(ViewComponentSymbol instance : instances) {
            resolveComponentInstances(instance.getSpannedScope());
        }
    }


}
