/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;

import de.monticore.lang.embeddedmontiarc.helper.SymbolPrinter;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.*;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.symboltable.CommonSymbol;
import de.monticore.lang.monticar.helper.IndentPrinter;

import java.util.*;

public class EMAWitnessPrinter {
    public EMAWitnessPrinter parent;
    public List<EMAWitnessPrinter> children = new LinkedList();
    public String componentTags;
    public List<PortSymbol> ports = new LinkedList();
    public List<ConnectorSymbol> connectors = new LinkedList();

    public EMAWitnessPrinter(ElementInstance singleWitness) {
    }

    public EMAWitnessPrinter(Collection<ElementInstance> graphWitness) {
    }

    public EMAWitnessPrinter(List<List<ElementInstance>> pathsWitness) {
    }

    public static String printComponent (ComponentSymbol cmp) {
        IndentPrinter ip = new IndentPrinter();

        ip.print("package " + cmp.getPackageName() + ";\n");
        ip.println();

        ip.print("component " + cmp.getName() + "\n");
        ip.print("/* " + cmp.getTags() + " */\n");
        ip.print("{\n");
        ip.println();
        ip.indent();

        SymbolPrinter.printPorts(cmp.getPorts(), ip);
        ip.println();

        ip.unindent();
        ip.print("}\n");

        return ip.getContent();

    }

    public static String printElementSet(Collection<? extends ElementInstance> graphWitness) {
        ComponentSymbol topSymbol = getEnclosingSymbol(graphWitness.stream().findFirst().get());
        IndentPrinter ip = new IndentPrinter();
        ip.print("package " + topSymbol.getPackageName() + ";\n");
        ip.println();
        ip.print("component " + topSymbol.getName() + " {\n");
        ip.print("/* " + topSymbol.getTags() + " */\n");
        ip.println();
        ip.indent();

        Collection<PortSymbol> ports = new HashSet<>();
        for (ElementInstance e : graphWitness) {
            if (e instanceof PortSymbol) {
                ports.add((PortSymbol) e);
            }
        }
        SymbolPrinter.printPorts(ports, ip);
        ip.println();

        for (ElementInstance e : graphWitness) {
            if (e instanceof ExpandedComponentInstanceSymbol) {
                ip.print("instance " + e.getName() + " /* " + e.getTags() + " */" + ";\n");
            }
        }
        ip.println();

        for (ElementInstance e : graphWitness) {
            if (e instanceof ExpandedComponentInstanceSymbol) {
                ComponentSymbol ctdef = ((ExpandedComponentInstanceSymbol)e).getComponentType().getReferencedSymbol();
                ip.print("component " + ctdef.getName() + " {\n");
                ip.println();
                ip.print("]\n");
                ip.println();
            }
        }
        ip.println();

        for (ElementInstance e : graphWitness) {
            if (e instanceof ComponentSymbol) {
                SymbolPrinter.printComponent((ComponentSymbol) e);
                ip.println();
            }
        }
        ip.println();

        ip.unindent();
        ip.print("}\n");

        return ip.getContent();
    }

    private static ComponentSymbol getEnclosingSymbol(ElementInstance symbol) {
        CommonSymbol spanningSym = (CommonSymbol) symbol.getEnclosingScope().getSpanningSymbol().get();
        if (spanningSym instanceof ExpandedComponentInstanceSymbol) {
            return ((ExpandedComponentInstanceSymbol) spanningSym).getComponentType().getReferencedSymbol();
        } else {
            return (ComponentSymbol) spanningSym;
        }
    }
}
