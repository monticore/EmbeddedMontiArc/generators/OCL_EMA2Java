/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.ocl.nfp;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.*;
import de.monticore.lang.montiarc.tagging._symboltable.TagSymbol;
import de.monticore.lang.embeddedmontiview.embeddedmontiview._symboltable.ViewConnectorSymbol;
import de.monticore.lang.embeddedmontiview.embeddedmontiview._symboltable.ViewEffectorSymbol;
import de.monticore.lang.ocl.codegen.OCLEMAHelper;
import jline.internal.Log;
import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.alg.shortestpath.AllDirectedPaths;
import org.jscience.physics.amount.Amount;

import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;
import java.util.*;

public class NFPHelper {

    public static <T extends TagSymbol, Q extends Quantity> Amount<?> max(Collection<T> list) {
        Iterator itr = list.iterator();
        try {
            Amount<Q> res = (Amount<Q>) ((TagSymbol) itr.next()).getValues().get(0);
            while (itr.hasNext()) {
                Amount<Q> cmp = (Amount<Q>) ((TagSymbol) itr.next()).getValues().get(0);
                if (cmp.isLargerThan(res)) {
                    res = cmp;
                }
            }

            return res;
        } catch (Exception e) {
            Log.error("Only Tags supported with Amount as Values");
            return null;
        }
    }

    public static <T extends TagSymbol, Q extends Quantity> Amount<?> max(Collection<T> list, Amount<Q> a) {
        if (list.isEmpty()) {
            return a;
        } else {
            return max(list);
        }
    }

    public static <T extends TagSymbol, Q extends Quantity> Amount<Q> min(Collection<T> list) {
        Iterator itr = list.iterator();
        try {
            Amount<Q> res = (Amount<Q>) ((TagSymbol) itr.next()).getValues().get(0);
            while (itr.hasNext()) {
                Amount<Q> cmp = (Amount<Q>) ((TagSymbol) itr.next()).getValues().get(0);
                if (cmp.isLessThan(res)) {
                    res = cmp;
                }
            }

            return res;
        } catch (Exception e) {
            Log.error("Only Tags supported with Amount as Values");
            return null;
        }
    }

    public static <T extends TagSymbol, Q extends Quantity> Amount<Q> min(Collection<T> list, Amount<Q> a) {
        if (list.isEmpty()) {
            return a;
        } else {
            return min(list);
        }
    }

    public static <Q extends Quantity> Amount<Q> sum(Collection<Amount> list, String unit) {
        if (list.isEmpty()) {
            Amount zero = Amount.valueOf(0, Unit.valueOf(unit));
            return zero;
        }
        Iterator itr = list.iterator();
        try {
            Amount<Q> res = (Amount<Q>) itr.next();
            while (itr.hasNext()) {
                Amount<Q> val = (Amount<Q>) itr.next();
                res = res.plus(val);
            }
            return res;
        } catch (Exception e) {
            Log.error("Can only sum Amounts of same Unit");
            return null;
        }
    }

    public static List<List<ElementInstance>> paths(ExpandedComponentInstanceSymbol root, ViewConnectorSymbol viewConn, int cyclesMax) {
        ElementInstance[] elems = viewConnToModelElements(root, viewConn);

        if (elems[0] != null && elems[1] != null) {
            return paths(root, elems[0], elems[1], cyclesMax);
        } else {
            return new LinkedList<>();
        }
    }

    public static List<List<ElementInstance>> paths(ExpandedComponentInstanceSymbol root, ViewEffectorSymbol viewEff, int cyclesMax) {
        ElementInstance[] elems = viewEffToModelElements(root, viewEff);

        if (elems[0] != null && elems[1] != null) {
            return paths(root, elems[0], elems[1], cyclesMax);
        } else {
            return new LinkedList<>();
        }
    }

    public static List<List<ElementInstance>> paths(ExpandedComponentInstanceSymbol root, ElementInstance source, ElementInstance target, int cyclesMax) {
        List<List<ElementInstance>> res = new LinkedList();
        DirectedGraph<ElementInstance, DefaultEdge> graph = NFPHelperGraphHelper.buildGraph(root);
        List<GraphPath<ElementInstance, DefaultEdge>> allPaths = new LinkedList<>();
        try {
            allPaths = new AllDirectedPaths(graph).getAllPaths(source, target, false, 50);
        } catch (Exception e) {
            Log.error("NFPHelper: " + e.getMessage());
        }
        for (GraphPath<ElementInstance, DefaultEdge> path : allPaths) {

           // if (new TarjanSimpleCycles((DirectedGraph) path.getGraph()).findSimpleCycles().size() <= cyclesMax) {
                res.add(path.getVertexList());
           // }

        }
        return res;
    }

    public static Set<ElementInstance> graph(ExpandedComponentInstanceSymbol root, ViewConnectorSymbol viewConn) {
        ElementInstance[] elems = viewConnToModelElements(root, viewConn);

        if (elems[0] != null && elems[1] != null) {
            return graph(root, elems[0], elems[1]);
        } else {
            return new HashSet();
        }
    }

    public static Set<ElementInstance> graph(ExpandedComponentInstanceSymbol root, ViewEffectorSymbol viewEff) {
        ElementInstance[] elems = viewEffToModelElements(root, viewEff);

        if (elems[0] != null && elems[1] != null) {
            return graph(root, elems[0], elems[1]);
        } else {
            return new HashSet();
        }
    }

    public static Set<ElementInstance> graph(ExpandedComponentInstanceSymbol root, ElementInstance source, ElementInstance target) {
        Set<ElementInstance> res = new HashSet();
        DirectedGraph<ElementInstance, DefaultEdge> graph = NFPHelperGraphHelper.buildGraph(root);
        List<GraphPath<ElementInstance, DefaultEdge>> allPaths = new LinkedList<>();
        try {
            allPaths = new AllDirectedPaths(graph).getAllPaths(source, target, true, 1000);
        } catch (Exception e) {
            Log.error("NFPHelper: " + e.getMessage());
        }
        for (GraphPath<ElementInstance, DefaultEdge> path : allPaths) {
            try {
                res.addAll(path.getVertexList());
            } catch (Exception e) {
                Log.error("NFPHelper: " + e.getMessage());
            }
        }
        return res;
    }

    /**
     * Determines the corresponding source,target from a ViewConnector in a model
     *
     * @param root     ExpandedComponentInstance which gives the search space
     * @param viewConn the connector
     * @return source and target as ElementInstance[]
     */
    public static ElementInstance[] viewConnToModelElements(ExpandedComponentInstanceSymbol root, ViewConnectorSymbol viewConn) {
        ElementInstance[] res = new ElementInstance[2];
        res[0] = null;
        res[1] = null;
        // get all port instances, component instances, connectors
        Collection<ElementInstance> c = new HashSet<>();
        c.addAll(OCLEMAHelper.<PortSymbol>getAllSymbolInstances(root.getSpannedScope(), PortSymbol.KIND));
        // remove port definitions
        for (Object p : c) {
            if (((PortSymbol) p).getComponent().isPresent()) {
                c.remove(p);
            }
        }
        c.addAll(OCLEMAHelper.<ExpandedComponentInstanceSymbol>getAllSymbolInstances(root.getSpannedScope(), ExpandedComponentInstanceSymbol.KIND));

        for (ElementInstance e : c) {
            String source = viewConn.getSource();
            String target = viewConn.getTarget();
            String viewConnContext = viewConn.getEnclosingScope().getSpanningSymbol().get().getName();
            String elemContext;
            String elemName;
            if (source.contains(".")) { // Is of form a1.port1
                elemName = e.getEnclosingScope().getSpanningSymbol().get().getName() + "." + e.getName();
                ExpandedComponentInstanceSymbol contextSymbol = (ExpandedComponentInstanceSymbol) e.getEnclosingScope().getSpanningSymbol().get();
                if (contextSymbol.getEnclosingScope().getSpanningSymbol().isPresent()) {
                    elemContext = ((ExpandedComponentInstanceSymbol) contextSymbol.getEnclosingScope().getSpanningSymbol().get()).getComponentType().getName();
                } else {
                    elemContext = contextSymbol.getEnclosingScope().getName().get();
                }
                if (elemName.equals(source) && elemContext.equals(viewConnContext)) { // check same name and context
                    res[0] = e;
                }
            } else { // ist of form port1 or comp1
                elemName = e.getName();
                elemContext = ((ExpandedComponentInstanceSymbol) e.getEnclosingScope().getSpanningSymbol().get()).getComponentType().getName();
                if (elemName.equals(source)) {
                    if (elemContext.equals(viewConnContext)) { // check same name and context
                        res[0] = e;
                    }
                }
            }

            if (target.contains(".")) { // Is of form a1.port1
                elemName = e.getEnclosingScope().getSpanningSymbol().get().getName() + "." + e.getName();
                ExpandedComponentInstanceSymbol contextSymbol = (ExpandedComponentInstanceSymbol) e.getEnclosingScope().getSpanningSymbol().get();
                if (contextSymbol.getEnclosingScope().getSpanningSymbol().isPresent()) {
                    elemContext = ((ExpandedComponentInstanceSymbol) contextSymbol.getEnclosingScope().getSpanningSymbol().get()).getComponentType().getName();
                } else {
                    elemContext = contextSymbol.getEnclosingScope().getName().get();
                }
                if (elemName.equals(target) && elemContext.equals(viewConnContext)) { // check same name and context
                    res[1] = e;
                }
            } else { // ist of form port1 or comp1
                elemName = e.getName();
                elemContext = ((ExpandedComponentInstanceSymbol) e.getEnclosingScope().getSpanningSymbol().get()).getComponentType().getName();
                if (elemName.equals(target) && elemContext.equals(viewConnContext)) { // check same name and context
                    res[1] = e;
                }
            }

        }


        return res;
    }

    /**
     * Determines the corresponding source,target from a ViewEffector in a model
     *
     * @param root    ExpandedComponentInstance which gives the search space
     * @param viewEff the effector
     * @return source and target as ElementInstance[]
     */
    public static ElementInstance[] viewEffToModelElements(ExpandedComponentInstanceSymbol root, ViewEffectorSymbol viewEff) {
        ElementInstance[] res = new ElementInstance[2];
        res[0] = null;
        res[1] = null;
        // get all port instances, component instances, connectors
        Collection<ElementInstance> c = new HashSet<>();
        c.addAll(OCLEMAHelper.<PortSymbol>getAllSymbolInstances(root.getSpannedScope(), PortSymbol.KIND));
        // remove port definitions
        for (Object p : c) {
            if (((PortSymbol) p).getComponent().isPresent()) {
                c.remove(p);
            }
        }
        c.addAll(OCLEMAHelper.<ExpandedComponentInstanceSymbol>getAllSymbolInstances(root.getSpannedScope(), ExpandedComponentInstanceSymbol.KIND));

        for (ElementInstance e : c) {
            String source = viewEff.getSource();
            String target = viewEff.getTarget();
            String viewConnContext = viewEff.getEnclosingScope().getSpanningSymbol().get().getName();
            String elemContext;
            String elemName;
            if (source.contains(".")) { // Is of form a1.port1
                elemName = e.getEnclosingScope().getSpanningSymbol().get().getName() + "." + e.getName();
                ExpandedComponentInstanceSymbol contextSymbol = (ExpandedComponentInstanceSymbol) e.getEnclosingScope().getSpanningSymbol().get();
                if (contextSymbol.getEnclosingScope().getSpanningSymbol().isPresent()) {
                    elemContext = ((ExpandedComponentInstanceSymbol) contextSymbol.getEnclosingScope().getSpanningSymbol().get()).getComponentType().getName();
                } else {
                    elemContext = contextSymbol.getEnclosingScope().getName().get();
                }
                if (elemName.equals(source) && elemContext.equals(viewConnContext)) { // check same name and context
                    res[0] = e;
                }
            } else { // ist of form port1 or comp1
                elemName = e.getName();
                elemContext = ((ExpandedComponentInstanceSymbol) e.getEnclosingScope().getSpanningSymbol().get()).getComponentType().getName();
                if (elemName.equals(source)) {
                    if (elemContext.equals(viewConnContext)) { // check same name and context
                        res[0] = e;
                    }
                }
            }

            if (target.contains(".")) { // Is of form a1.port1
                elemName = e.getEnclosingScope().getSpanningSymbol().get().getName() + "." + e.getName();
                ExpandedComponentInstanceSymbol contextSymbol = (ExpandedComponentInstanceSymbol) e.getEnclosingScope().getSpanningSymbol().get();
                if (contextSymbol.getEnclosingScope().getSpanningSymbol().isPresent()) {
                    elemContext = ((ExpandedComponentInstanceSymbol) contextSymbol.getEnclosingScope().getSpanningSymbol().get()).getComponentType().getName();
                } else {
                    elemContext = contextSymbol.getEnclosingScope().getName().get();
                }
                if (elemName.equals(target) && elemContext.equals(viewConnContext)) { // check same name and context
                    res[1] = e;
                }
            } else { // ist of form port1 or comp1
                elemName = e.getName();
                elemContext = ((ExpandedComponentInstanceSymbol) e.getEnclosingScope().getSpanningSymbol().get()).getComponentType().getName();
                if (elemName.equals(target) && elemContext.equals(viewConnContext)) { // check same name and context
                    res[1] = e;
                }
            }

        }


        return res;
    }

    // return all paths trhough a component only containing subcomponents one level down
    public static List<List<ElementInstance>> directSubComponentPaths(ExpandedComponentInstanceSymbol cmp) {

        DirectedGraph<ElementInstance, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);
        for (ExpandedComponentInstanceSymbol subCmp : cmp.getSubComponents()) {
            graph.addVertex(subCmp);
        }

        for (ExpandedComponentInstanceSymbol subCmp : cmp.getSubComponents()) {
            for (PortSymbol p : subCmp.getOutgoingPorts()) {
                // Check if port instantiation
                if (p.getComponentInstance().isPresent()) {
                    Optional<ExpandedComponentInstanceSymbol> connectedComp = getConnectedComponent(p, cmp);
                    // check if port is connected to anaother sub component then add
                    if (connectedComp.isPresent() && connectedComp.get() != cmp) {
                        graph.addEdge(subCmp, connectedComp.get());
                    }
                }
            }
        }

        List<List<ElementInstance>> res = new LinkedList<>();
        for (PortSymbol sourcePort : cmp.getIncomingPorts()) {
            for (PortSymbol targetPort : cmp.getOutgoingPorts()) {
                if (sourcePort.getComponentInstance().isPresent() && targetPort.getComponentInstance().isPresent()) {
                    List<GraphPath<ElementInstance, DefaultEdge>> allPaths = new LinkedList<>();
                    Optional<ExpandedComponentInstanceSymbol> source = getConnectedSubComponent(sourcePort);
                    Optional<ExpandedComponentInstanceSymbol> target = getConnectedSubComponent(targetPort);
                    try {
                        if (source.isPresent() && target.isPresent()) {
                            allPaths = new AllDirectedPaths(graph).getAllPaths(source.get(), target.get(), true, 50);
                        }
                        for (GraphPath<ElementInstance, DefaultEdge> path : allPaths) {
                            res.add(path.getVertexList());
                        }
                    } catch (Exception e) {
                        Log.error("NFPHelper: " + e.getMessage());
                    }
                }
            }
        }

        return res;
    }

    // return the connected component to p looking for connectors in cmp
    private static Optional<ExpandedComponentInstanceSymbol> getConnectedComponent(PortSymbol p, ExpandedComponentInstanceSymbol cmp) {
        List<PortSymbol> connectedPorts = p.getTargetConnectedPorts(cmp);
        Optional<ExpandedComponentInstanceSymbol> connectedComp = Optional.empty();
        if (!connectedPorts.isEmpty()) {
            connectedComp = connectedPorts.get(0).getComponentInstance();
        }
        return connectedComp;
    }

    // returns ath subcomponent a port is connected to if existing
    private static Optional<ExpandedComponentInstanceSymbol> getConnectedSubComponent(PortSymbol p) {
        Optional<ExpandedComponentInstanceSymbol> connectedComp = Optional.empty();
        if (p.getComponentInstance().isPresent()) {
            ExpandedComponentInstanceSymbol root = p.getComponentInstance().get();
            for (ConnectorSymbol con : root.getConnectors()) {
                if (con.getSourcePort() == p) {
                    connectedComp = con.getTargetPort().getComponentInstance();
                } else if (con.getTargetPort() == p) {
                    connectedComp = con.getSourcePort().getComponentInstance();
                }
                if (connectedComp.isPresent() && connectedComp.get() != p.getComponentInstance().get()) {
                    return connectedComp;
                }
            }
        }

        return Optional.empty();
    }
}
