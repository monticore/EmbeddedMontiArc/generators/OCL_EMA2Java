@rem (c) https://github.com/MontiCore/monticore  

@echo off

echo test if jdk1.8 is set (need java compiler), if there is an error please update your environment variable https://docs.oracle.com/javase/8/docs/technotes/guides/install/windows_jdk_install.html#BABGDJFH

javac -version


set jar= %~dp0%oclma2java-4.0.1-SNAPSHOT-jar-with-dependencies.jar
set parent_dir= "%~dp0\"
set target= "%~dp0%target"
set model= "model.fas.DaimlerPublicDemonstrator_ADAS_v04"
set ocl= "ocl.rule4_WCET_MultiCore"


cd %JDK_PATH%

java -jar %JAR% %parent_dir% %model% %ocl% %target%



pause

