#!/bin/bash
# (c) https://github.com/MontiCore/monticore  


echo 'test if jdk1.8 is set (need java compiler), if there is an error please update your environment variable https://docs.oracle.com/javase/8/docs/technotes/guides/install/windows_jdk_install.html#BABGDJFH'

javac -version

current_dir=$(pwd)

#echo $current_dir

jar=$current_dir'/oclma2java-4.0.1-SNAPSHOT-jar-with-dependencies.jar'
target=$current_dir'/target'
model='model.fas.DaimlerPublicDemonstrator_ADAS_v04'
ocl='ocl.rule3_WCET_SingleCore'

java -jar $jar $current_dir $model $ocl $target
