/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.CompPower;
tags CompPower for fas.daimlerPublicDemonstrator_ADAS_v04.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.brakeAssistant {
	tag brake_Booster with CompPowerInst = 68978 pW;
	tag brake_Booster.relOp with CompPowerInst = 27432 pW;
	tag brake_Booster.switch with CompPowerInst = 41545 pW;
}
