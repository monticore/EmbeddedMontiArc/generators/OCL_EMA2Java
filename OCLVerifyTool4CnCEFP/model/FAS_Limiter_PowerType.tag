/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.CompPower;

tags CompPower for fas.DaimlerPublicDemonstrator_ADAS_v04.DEMO_FAS.DEMO_FAS.Subsystem.DEMO_FAS.DEMO_FAS_Funktion.Limiter {
	tag Limiter_Function with CompPower = 26240 nW;
	tag Limiter_Function.Limiter_Enabled with CompPower = 18400 nW;
	tag Limiter_Function.Limiter_Enabled.EdgeRising with CompPower = 4720 nW;
	tag Limiter_Function.Limiter_Enabled.EdgeRising.LogOp_A with CompPower = 1360 nW; 
	tag Limiter_Function.Limiter_Enabled.EdgeRising.LogOp_N with CompPower = 960 nW; 
	tag Limiter_Function.Limiter_Enabled.EdgeRising.Memory_U with CompPower = 0 nW; 
	tag Limiter_Function.Limiter_Enabled.EdgeRising.Switch_R with CompPower = 2400 nW; 
	tag Limiter_Function.Limiter_Enabled.IfBlock with CompPower = 800 nW; 
	tag Limiter_Function.Limiter_Enabled.Limiter_Active with CompPower = 1360 nW;
	tag Limiter_Function.Limiter_Enabled.Limiter_Active.Gain with CompPower = 160 nW; 
	tag Limiter_Function.Limiter_Enabled.Limiter_Active.LogicalOperator with CompPower = 1360 nW; 
	tag Limiter_Function.Limiter_Enabled.Limiter_Deactive with CompPower = 1360 nW;
	tag Limiter_Function.Limiter_Enabled.Limiter_Deactive.Gain with CompPower = 160 nW; 
	tag Limiter_Function.Limiter_Enabled.Limiter_Deactive.LogicalOperator with CompPower = 1360 nW; 
	tag Limiter_Function.Limiter_Enabled.LogicalOperator1 with CompPower = 960 nW; 
	tag Limiter_Function.Limiter_Enabled.LogicalOperator2 with CompPower = 960 nW; 
	tag Limiter_Function.Limiter_Enabled.RSFlipFlop with CompPower = 10560 nW;
	tag Limiter_Function.Limiter_Enabled.RSFlipFlop.LogOp_N with CompPower = 960 nW; 
	tag Limiter_Function.Limiter_Enabled.RSFlipFlop.Memory_Q with CompPower = 0 nW; 
	tag Limiter_Function.Limiter_Enabled.RSFlipFlop.Switch_R with CompPower = 2400 nW; 
	tag Limiter_Function.Limiter_Enabled.RSFlipFlop.Switch_S with CompPower = 2400 nW; 
	tag Limiter_Function.Limiter_Enabled.Terminator with CompPower = 0 nW; 
	tag Limiter_Function.Limiter_InitialSetValue with CompPower = 0 nW;
	tag Limiter_Function.Limiter_SetValue with CompPower = 3200 nW;
	tag Limiter_Function.Limiter_SetValue.RelOp1 with CompPower = 960 nW; 
	tag Limiter_Function.Limiter_SetValue.RelOp3 with CompPower = 960 nW; 
	tag Limiter_Function.Limiter_SetValue.V_LimSetValueMinus with CompPower = 640 nW;
	tag Limiter_Function.Limiter_SetValue.V_LimSetValueMinus.Sum with CompPower = 640 nW; 
	tag Limiter_Function.Limiter_SetValue.V_LimSetValuePlus with CompPower = 640 nW;
	tag Limiter_Function.Limiter_SetValue.V_LimSetValuePlus.Sum with CompPower = 640 nW; 
	tag Limiter_Function.Limiter_StartUpSetValue with CompPower = 0 nW;
	tag Limiter_Function.LogicalOperator with CompPower = 1360 nW; 
	tag Limiter_Function.MinMax with CompPower = 2240 nW; 
	tag Limiter_Function.SwitchBlock with CompPower = 2400 nW; 
}
