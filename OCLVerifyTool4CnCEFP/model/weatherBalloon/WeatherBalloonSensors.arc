/* (c) https://github.com/MontiCore/monticore */
package model.weatherBalloon;

component WeatherBalloonSensors {
    port
        in Byte[] controlSignalsIn,
        out Byte[] dataSaveInternalOut,
        out Byte[] dataAntennaOut;

    component Controller {
        port
          in Byte[] controlSignalsIn,
          out Byte[] dataSaveInternalOut,
          out Byte[] dataAntennaOut,
          out Byte[] sensorRequest1,
          out Byte[] sensorRequest2,
          out Byte[] sensorRequest3,
          out Byte[] sensorRequest4,         
          in Byte[] sensorResponse1,
          in Byte[] sensorResponse2,
          in Byte[] sensorResponse3,
          in Byte[] sensorResponse4;
    }

    component GPS {
      port 
        in Byte[] sensorIn,
        out Byte[] sensorOut;
    }
    
    component Temperature {
      port 
        in Byte[] sensorIn,
        out Byte[] sensorOut;
    }
    
    component Humidity {
      port 
        in Byte[] sensorIn,
        out Byte[] sensorOut;
    }
    
    component Controller control;
    component GPS gps1, gps2;
    component Temperature temp1;
    component Humidity humid1;


    connect controlSignalsIn -> control.controlSignalsIn;
    connect control.dataSaveInternalOut -> dataSaveInternalOut;
    connect control.dataAntennaOut -> dataAntennaOut;
    connect control.sensorRequest1 -> gps1.sensorIn;
    connect control.sensorRequest2 -> gps2.sensorIn;
    connect control.sensorRequest3 -> temp1.sensorIn;
    connect control.sensorRequest4 -> humid1.sensorIn;    
    connect gps1.sensorOut -> control.sensorResponse1;
    connect gps2.sensorOut -> control.sensorResponse2;
    connect temp1.sensorOut -> control.sensorResponse3;
    connect humid1.sensorOut -> control.sensorResponse4;


}
