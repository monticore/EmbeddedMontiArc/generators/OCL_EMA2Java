/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.CompEnergy;
tags CompEnergy for fas.daimlerPublicDemonstrator_ADAS_v04.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.brakeAssistant {
	tag brake_Booster with CompEnergyInst = 68977 pJ;
	tag brake_Booster.relOp with CompEnergyInst = 27432 pJ;
	tag brake_Booster.switch with CompEnergyInst = 41545 pJ;
}
