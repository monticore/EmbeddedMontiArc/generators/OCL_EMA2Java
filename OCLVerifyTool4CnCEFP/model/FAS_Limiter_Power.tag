/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.CompPower;

tags CompPower for fas.daimlerPublicDemonstrator_ADAS_v04.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter {
	tag limiter_Function with CompPowerInst = 26241 nW;
	tag limiter_Function.limiter_Enabled with CompPowerInst = 18401 nW;
	tag limiter_Function.limiter_Enabled.edgeRising with CompPowerInst = 4721 nW;
	tag limiter_Function.limiter_Enabled.edgeRising.logOp_A with CompPowerInst = 1361 nW; 
	tag limiter_Function.limiter_Enabled.edgeRising.logOp_N with CompPowerInst = 961 nW; 
	tag limiter_Function.limiter_Enabled.edgeRising.memory_U with CompPowerInst = 1 nW; 
	tag limiter_Function.limiter_Enabled.edgeRising.switch_R with CompPowerInst = 2401 nW; 
	tag limiter_Function.limiter_Enabled.ifBlock with CompPowerInst = 801 nW; 
	tag limiter_Function.limiter_Enabled.limiter_Active with CompPowerInst = 1361 nW;
	tag limiter_Function.limiter_Enabled.limiter_Active.gain with CompPowerInst = 161 nW; 
	tag limiter_Function.limiter_Enabled.limiter_Active.logicalOperator with CompPowerInst = 1361 nW; 
	tag limiter_Function.limiter_Enabled.limiter_Deactive with CompPowerInst = 1361 nW;
	tag limiter_Function.limiter_Enabled.limiter_Deactive.gain with CompPowerInst = 161 nW; 
	tag limiter_Function.limiter_Enabled.limiter_Deactive.logicalOperator with CompPowerInst = 1361 nW; 
	tag limiter_Function.limiter_Enabled.logicalOperator1 with CompPowerInst = 961 nW; 
	tag limiter_Function.limiter_Enabled.logicalOperator2 with CompPowerInst = 961 nW; 
	tag limiter_Function.limiter_Enabled.rSFlipFlop with CompPowerInst = 10561 nW;
	tag limiter_Function.limiter_Enabled.rSFlipFlop.logOp_N with CompPowerInst = 961 nW; 
	tag limiter_Function.limiter_Enabled.rSFlipFlop.memory_Q with CompPowerInst = 1 nW; 
	tag limiter_Function.limiter_Enabled.rSFlipFlop.switch_R with CompPowerInst = 2401 nW; 
	tag limiter_Function.limiter_Enabled.rSFlipFlop.switch_S with CompPowerInst = 2401 nW; 
	tag limiter_Function.limiter_Enabled.terminator with CompPowerInst = 1 nW; 
	tag limiter_Function.limiter_InitialSetValue with CompPowerInst = 1 nW;
	tag limiter_Function.limiter_SetValue with CompPowerInst = 3201 nW;
	tag limiter_Function.limiter_SetValue.relOp1 with CompPowerInst = 961 nW; 
	tag limiter_Function.limiter_SetValue.relOp3 with CompPowerInst = 961 nW; 
	tag limiter_Function.limiter_SetValue.v_LimSetValueMinus with CompPowerInst = 641 nW;
	tag limiter_Function.limiter_SetValue.v_LimSetValueMinus.sum with CompPowerInst = 641 nW; 
	tag limiter_Function.limiter_SetValue.v_LimSetValuePlus with CompPowerInst = 641 nW;
	tag limiter_Function.limiter_SetValue.v_LimSetValuePlus.sum with CompPowerInst = 641 nW; 
	tag limiter_Function.limiter_StartUpSetValue with CompPowerInst = 1 nW;
	tag limiter_Function.logicalOperator with CompPowerInst = 1361 nW; 
	tag limiter_Function.minMax with CompPowerInst = 2241 nW; 
	tag limiter_Function.switchBlock with CompPowerInst = 2401 nW; 
}
