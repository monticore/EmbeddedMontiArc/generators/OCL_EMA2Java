/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.CompPower;
tags CompPower for weatherBalloon {
	tag WeatherBalloonSensors with CompPower = 5001 mW;
	tag WeatherBalloonSensors.Temperature with CompPower = 401 mW;
	tag WeatherBalloonSensors.GPS with CompPower = 2501 mW;

}
