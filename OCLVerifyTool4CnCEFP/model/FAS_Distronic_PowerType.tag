/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.CompPower;
tags CompPower for fas.DaimlerPublicDemomWtrator_ADAS_v04.DEMO_FAS.DEMO_FAS.Subsystem.DEMO_FAS.DEMO_FAS_Funktion.Distronic {
	tag Distronic_Deactive with CompPower = 0 nW;
	tag Distronic_Disabled with CompPower = 0 nW;
	tag Distronic_Enabled with CompPower = 16640 nW;
	tag Distronic_Enabled.LookUpTable with CompPower = 5000 nW; 
	tag Distronic_Enabled.LookUpTable1 with CompPower = 5000 nW; 
	tag Distronic_Enabled.LookUpTable2 with CompPower = 5000 nW; 
	tag Distronic_Enabled.LookUpTable3 with CompPower = 5000 nW; 
	tag Distronic_Enabled.LookUpTable4 with CompPower = 5000 nW; 
	tag Distronic_Enabled.Mul with CompPower = 1120 nW; 
	tag Distronic_Enabled.Mul1 with CompPower = 1120 nW; 
	tag Distronic_Enabled.Mul2 with CompPower = 1120 nW; 
	tag Distronic_Enabled.MultiportSwitch with CompPower = 5760 nW; 
	tag Distronic_Enabled.RelOp with CompPower = 960 nW; 
	tag Distronic_Enabled.Sum with CompPower = 640 nW; 
	tag Distronic_Enabled.SwitchBlock with CompPower = 3000 nW; 
	tag Distronic_FTS_Enabled with CompPower = 9760 nW;
	tag Distronic_FTS_Enabled.LookUpTable with CompPower = 5000 nW; 
	tag Distronic_FTS_Enabled.LookUpTable1 with CompPower = 5000 nW; 
	tag Distronic_FTS_Enabled.Mul1 with CompPower = 1120 nW; 
	tag Distronic_FTS_Enabled.RelOp with CompPower = 960 nW; 
	tag Distronic_FTS_Enabled.Sum with CompPower = 640 nW; 
	tag Distronic_FTS_Enabled.SwitchBlock with CompPower = 3000 nW; 
	tag ifBlock with CompPower = 4880 nW; 
}
