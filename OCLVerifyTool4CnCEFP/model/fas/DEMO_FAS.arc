/* (c) https://github.com/MontiCore/monticore */
package model.fas;
import java.lang.*;
import java.util.*;
  <<Type="SubSystem">> component DEMO_FAS {
    ports
      out Boolean cC_active_b,
      out Double cCSetValue_kmh,
      out Double limiterSetValue_kmh,
      out Boolean limiter_active_b,
      out Boolean opti_warn_b,
      out Boolean acusti_warn_b,
      out Double brakeForce_pc,
      out Double acceleration_pc,
      in Boolean parkingBrake_b,
      in Double brakeForce_pedal_pc,
      in Double acceleration_pedal_pc,
      in Boolean cruiseControl_b,
      in Boolean limiter_b,
      in Double leverUp_stat,
      in Double leverDown_stat,
      in Double v_Vehicle_kmh,
      in Double v_Sign_kmh,
      in Boolean sign_b,
      in Double distance_stat,
      in Double v_Obj_rel_kmh;
    <<Type="SubSystem">> component DEMO_FAS {
      ports
        in Boolean _ParkingBrake_bIn1,
        in Double _BrakeForce_pedal_pcIn2,
        in Double _Acceleration_pedal_pcIn3,
        in Boolean _CruiseControl_bIn4,
        in Boolean _Limiter_bIn5,
        in Double _LeverUp_statIn6,
        in Double _LeverDown_statIn7,
        in Double _V_Vehicle_kmhIn8,
        in Double _V_Sign_kmhIn9,
        in Boolean _Sign_bIn10,
        in Double _Distance_statIn11,
        in Double _V_Obj_rel_kmhIn12,
        in Double _Distance_Object_mIn13,
        out Boolean _CC_active_bOut1,
        out Double _Acceleration_pcOut2,
        out Double _BrakeForce_pcOut3,
        out Double _CCSetValue_kmhOut4,
        out Double _LimiterSetValue_kmhOut5,
        out Boolean _Limiter_active_bOut6,
        out Boolean _Opti_warn_bOut7,
        out Boolean _Acusti_warn_bOut8;
      <<Type="SubSystem">> component Subsystem {
        ports
          in Boolean _ParkingBrake_bIn1,
          in Double _BrakeForce_pedal_pcIn2,
          in Double _Acceleration_pedal_pcIn3,
          in Boolean _CruiseControl_bIn4,
          in Boolean _Limiter_bIn5,
          in Double _LeverUp_statIn6,
          in Double _LeverDown_statIn7,
          in Double _V_Vehicle_kmhIn8,
          in Double _V_Sign_kmhIn9,
          in Boolean _Sign_bIn10,
          in Double _Distance_statIn11,
          in Double _V_Obj_rel_kmhIn12,
          in Double _Distance_Object_mIn13,
          out Boolean _CC_active_bOut1,
          out Double _Acceleration_pcOut2,
          out Double _BrakeForce_pcOut3,
          out Double _CCSetValue_kmhOut4,
          out Double _LimiterSetValue_kmhOut5,
          out Boolean _Limiter_active_bOut6,
          out Boolean _Opti_warn_bOut7,
          out Boolean _Acusti_warn_bOut8;
        <<Type="SubSystem">> component DEMO_FAS {
          ports
            in Boolean _ParkingBrake_bIn1,
            in Double _BrakeForce_pedal_pcIn2,
            in Double _Acceleration_pedal_pcIn3,
            in Boolean _CruiseControl_bIn4,
            in Boolean _Limiter_bIn5,
            in Double _LeverUp_statIn6,
            in Double _LeverDown_statIn7,
            in Double _V_Vehicle_kmhIn8,
            in Double _V_Sign_kmhIn9,
            in Boolean _Sign_bIn10,
            in Double _Distance_statIn11,
            in Double _V_Obj_rel_kmhIn12,
            in Double _Distance_Object_mIn13,
            out Boolean _CC_active_bOut1,
            out Double _Acceleration_pcOut2,
            out Double _BrakeForce_pcOut3,
            out Double _CCSetValue_kmhOut4,
            out Double _LimiterSetValue_kmhOut5,
            out Boolean _Limiter_active_bOut6,
            out Boolean _Opti_warn_bOut7,
            out Boolean _Acusti_warn_bOut8;
          <<Type="SubSystem">> component DEMO_FAS_Funktion {
            ports
              in Boolean parkingBrake_b,
              in Double brakeForce_pedal_pc,
              in Double acceleration_pedal_pc,
              in Boolean cruiseControl_b,
              in Boolean limiter_b,
              in Double leverUp_stat,
              in Double leverDown_stat,
              in Double v_Vehicle_kmh,
              in Double v_Vehicle_kmh1,
              in Double v_Sign_kmh,
              in Boolean sign_b,
              in Double distance_stat,
              in Double distance_stat1,
              in Double v_Obj_rel_kmh,
              in Double v_Obj_rel_kmh1,
              in Double v_Obj_rel_kmh2,
              in Double distance_Object_m,
              in Double distance_Object_m1,
              in Double distance_Object_m2,
              in Double distance_Object_m3,
              out Boolean cC_active_b,
              out Double acceleration_pc,
              out Double brakeForce_pc,
              out Double cCSetValue_kmh,
              out Double limiterSetValue_kmh,
              out Boolean limiter_active_b,
              out Boolean opti_warn_b,
              out Boolean acusti_warn_b;
            <<Type="SubSystem">> component BrakeAssistant {
              ports
                in Double brakeForce_pedal_pcIn1,
                out Double brakeForceBoosted_pcOut1;
              <<Type="SubSystem">> component Brake_Booster {
                ports
                  in Double brakeForce_pedal_pcIn1,
                  out Double brakeForceBoosted_pcOut1;
                <<Type="Constant",Value="100">> component Constant {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="70">> component DEMO_FAS_BrakeBooster_Threshold {
                  ports
                    out Double out1;
                }
                <<Operator=">=",Type="RelationalOperator">> component RelOp {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double brakeForceBoosted_pcOut1;
                  effect ifIn -> brakeForceBoosted_pcOut1;
                  effect condition -> brakeForceBoosted_pcOut1;
                  effect elseIn -> brakeForceBoosted_pcOut1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Constant constant;
                component DEMO_FAS_BrakeBooster_Threshold dEMO_FAS_BrakeBooster_Threshold;
                component RelOp relOp;
                component SwitchBlock switchBlock;
                component VERSION_INFO vERSION_INFO;
                component Condition condition;
                connect condition.out1 -> switchBlock.condition;
                connect relOp.out1 -> condition.in1;
                connect switchBlock.brakeForceBoosted_pcOut1 -> brakeForceBoosted_pcOut1;
                connect constant.out1 -> switchBlock.ifIn;
                connect dEMO_FAS_BrakeBooster_Threshold.out1 -> relOp.in2;
                connect brakeForce_pedal_pcIn1 -> relOp.in1;
                connect brakeForce_pedal_pcIn1 -> switchBlock.elseIn;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              component Brake_Booster brake_Booster;
              component VERSION_INFO vERSION_INFO;
              connect brake_Booster.brakeForceBoosted_pcOut1 -> brakeForceBoosted_pcOut1;
              connect brakeForce_pedal_pcIn1 -> brake_Booster.brakeForce_pedal_pcIn1;
            }
            <<Type="SubSystem">> component Distancewarner {
              ports
                in Double v_Vehicle_msIn1,
                in Double distance_Object_mIn2,
                out Boolean opti_warn_bOut1,
                out Boolean acusti_warn_bOut2;
              <<Type="SubSystem">> component Distancewarner_Function {
                ports
                  in Double v_Vehicle_msIn1,
                  in Double distance_Object_mIn2,
                  out Boolean opti_warn_bOut1,
                  out Boolean acusti_warn_bOut2;
                <<Type="Constant",Value="1">> component DEMO_FAS_Accusti_Threshold_Time {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="1.5">> component DEMO_FAS_Opti_Threshold_Time {
                  ports
                    out Double out1;
                }
                <<Type="Product",Inputs="**">> component Mul {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double opti_warn_bOut1;
                  effect in1 -> opti_warn_bOut1;
                  effect in2 -> opti_warn_bOut1;
                }
                <<Type="Product",Inputs="**">> component Mul1 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Operator="<=",Type="RelationalOperator">> component RelOp {
                  ports
                    in Double in1,
                    in Double opti_warn_bIn2,
                    out Boolean opti_warn_bOut1;
                  effect in1 -> opti_warn_bOut1;
                  effect opti_warn_bIn2 -> opti_warn_bOut1;
                }
                <<Operator="<=",Type="RelationalOperator">> component RelOp2 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean acusti_warn_bOut1;
                  effect in1 -> acusti_warn_bOut1;
                  effect in2 -> acusti_warn_bOut1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                component DEMO_FAS_Accusti_Threshold_Time dEMO_FAS_Accusti_Threshold_Time;
                component DEMO_FAS_Opti_Threshold_Time dEMO_FAS_Opti_Threshold_Time;
                component Mul mul;
                component Mul1 mul1;
                component RelOp relOp;
                component RelOp2 relOp2;
                component VERSION_INFO vERSION_INFO;
                connect mul1.out1 -> relOp2.in2;
                connect dEMO_FAS_Accusti_Threshold_Time.out1 -> mul1.in2;
                connect relOp2.acusti_warn_bOut1 -> acusti_warn_bOut2;
                connect distance_Object_mIn2 -> relOp.in1;
                connect distance_Object_mIn2 -> relOp2.in1;
                connect mul.opti_warn_bOut1 -> relOp.opti_warn_bIn2;
                connect relOp.opti_warn_bOut1 -> opti_warn_bOut1;
                connect dEMO_FAS_Opti_Threshold_Time.out1 -> mul.in2;
                connect v_Vehicle_msIn1 -> mul1.in1;
                connect v_Vehicle_msIn1 -> mul.in1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              component Distancewarner_Function distancewarner_Function;
              component VERSION_INFO vERSION_INFO;
              connect distance_Object_mIn2 -> distancewarner_Function.distance_Object_mIn2;
              connect v_Vehicle_msIn1 -> distancewarner_Function.v_Vehicle_msIn1;
              connect distancewarner_Function.acusti_warn_bOut2 -> acusti_warn_bOut2;
              connect distancewarner_Function.opti_warn_bOut1 -> opti_warn_bOut1;
            }
            <<Type="SubSystem">> component Distronic {
              ports
                in Boolean cC_active_bIn1,
                in Double v_Vehicle_msIn2,
                in Double distance_Object_mIn3,
                in Double distance_statIn4,
                in Double v_Obj_rel_kmhIn5,
                in Boolean fTS_active_bIn6,
                in Double fTS_Abstand_soll_mIn7,
                out Double brakeForce_Distronic_pcOut1,
                out Double decelerator_pcOut2;
              <<Type="Constant",Value="1">> component Constant {
                ports
                  out Double out1;
              }
              <<Type="SubSystem">> component Distronic_Deactive {
                ports
                  out Double brakeForce_Distronic_pcOut1,
                  out Double decelerator_pcOut2;
                <<Type="Constant",Value="0">> component Constant {
                  ports
                    out Double brakeForce_Distronic_pcOut1;
                }
                <<Type="Constant",Value="100">> component Constant1 {
                  ports
                    out Double decelerator_pcOut1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                component Constant constant;
                component Constant1 constant1;
                component VERSION_INFO vERSION_INFO;
                connect constant1.decelerator_pcOut1 -> decelerator_pcOut2;
                connect constant.brakeForce_Distronic_pcOut1 -> brakeForce_Distronic_pcOut1;
              }
              <<Type="SubSystem">> component Distronic_Disabled {
                ports
                  out Double brakeForce_Distronic_pcOut1,
                  out Double decelerator_pcOut2;
                <<Type="Constant",Value="0">> component Constant {
                  ports
                    out Double brakeForce_Distronic_pcOut1;
                }
                <<Type="Constant",Value="100">> component Constant1 {
                  ports
                    out Double decelerator_pcOut1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                component Constant constant;
                component Constant1 constant1;
                component VERSION_INFO vERSION_INFO;
                connect constant1.decelerator_pcOut1 -> decelerator_pcOut2;
                connect constant.brakeForce_Distronic_pcOut1 -> brakeForce_Distronic_pcOut1;
              }
              <<Type="SubSystem">> component Distronic_Enabled {
                ports
                  in Double v_Vehicle_msIn1,
                  in Double distance_Object_mIn2,
                  in Double distance_statIn3,
                  in Double v_Obj_rel_kmhIn4,
                  out Double brakeForce_Distronic_pcOut1,
                  out Double decelerator_pcOut2;
                <<Type="Constant",Value="100">> component Constant {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="2.5">> component Constant1 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="3">> component DEMO_FAS_Distance_Treshold_Far {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="2.5">> component DEMO_FAS_Distance_Treshold_Med {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="2">> component DEMO_FAS_Distance_Treshold_Near {
                  ports
                    out Double out1;
                }
                <<BreakpointsForDimension1="[0,5, 20,50]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[100,100, 10,0]",BreakpointsSpecification="Explicit values">> component LookUpTable {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<BreakpointsForDimension1="[-1,0,10]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[0,0.15,1]",BreakpointsSpecification="Explicit values">> component LookUpTable1 {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<BreakpointsForDimension1="[0,20,50,100]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[0,50,90,100]",BreakpointsSpecification="Explicit values">> component LookUpTable2 {
                  ports
                    in Double in1,
                    out Double decelerator_pcOut1;
                  effect in1 -> decelerator_pcOut1;
                }
                <<BreakpointsForDimension1="[-10,0]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[1,0.9]",BreakpointsSpecification="Explicit values">> component LookUpTable3 {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<BreakpointsForDimension1="[0,10]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[50,0]",BreakpointsSpecification="Explicit values">> component LookUpTable4 {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="Product",Inputs="**">> component Mul {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Product",Inputs="**">> component Mul1 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double brakeForce_Distronic_pcOut1;
                  effect in1 -> brakeForce_Distronic_pcOut1;
                  effect in2 -> brakeForce_Distronic_pcOut1;
                }
                <<Type="Product",Inputs="**">> component Mul2 {
                  ports
                    in Double decelerator_pcIn1,
                    in Double in2,
                    out Double decelerator_pcOut1;
                  effect decelerator_pcIn1 -> decelerator_pcOut1;
                  effect in2 -> decelerator_pcOut1;
                }
                <<Type="MultiPortSwitch",DataPortOrder="One-based contiguous">> component MultiportSwitch {
                  ports
                    in Double in1,
                    in Double in2,
                    in Double in3,
                    in Double in4,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                  effect in3 -> out1;
                  effect in4 -> out1;
                }
                <<Operator="<",Type="RelationalOperator">> component RelOp {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Sum",ListOfSigns="+-+">> component Sum {
                  ports
                    in Double in1,
                    in Double in2,
                    in Double in3,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                  effect in3 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double brakeForce_Distronic_pcOut1;
                  effect ifIn -> brakeForce_Distronic_pcOut1;
                  effect condition -> brakeForce_Distronic_pcOut1;
                  effect elseIn -> brakeForce_Distronic_pcOut1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Constant constant;
                component Constant1 constant1;
                component DEMO_FAS_Distance_Treshold_Far dEMO_FAS_Distance_Treshold_Far;
                component DEMO_FAS_Distance_Treshold_Med dEMO_FAS_Distance_Treshold_Med;
                component DEMO_FAS_Distance_Treshold_Near dEMO_FAS_Distance_Treshold_Near;
                component LookUpTable lookUpTable;
                component LookUpTable1 lookUpTable1;
                component LookUpTable2 lookUpTable2;
                component LookUpTable3 lookUpTable3;
                component LookUpTable4 lookUpTable4;
                component Mul mul;
                component Mul1 mul1;
                component Mul2 mul2;
                component MultiportSwitch multiportSwitch;
                component RelOp relOp;
                component Sum sum;
                component SwitchBlock switchBlock;
                component VERSION_INFO vERSION_INFO;
                component Condition condition;
                connect condition.out1 -> switchBlock.condition;
                connect relOp.out1 -> condition.in1;
                connect lookUpTable4.out1 -> sum.in3;
                connect lookUpTable3.out1 -> mul2.in2;
                connect lookUpTable2.decelerator_pcOut1 -> mul2.decelerator_pcIn1;
                connect mul2.decelerator_pcOut1 -> decelerator_pcOut2;
                connect constant1.out1 -> relOp.in2;
                connect constant.out1 -> switchBlock.ifIn;
                connect distance_Object_mIn2 -> sum.in1;
                connect distance_Object_mIn2 -> relOp.in1;
                connect switchBlock.brakeForce_Distronic_pcOut1 -> brakeForce_Distronic_pcOut1;
                connect lookUpTable.out1 -> mul1.in1;
                connect distance_statIn3 -> multiportSwitch.in1;
                connect dEMO_FAS_Distance_Treshold_Far.out1 -> multiportSwitch.in2;
                connect dEMO_FAS_Distance_Treshold_Near.out1 -> multiportSwitch.in4;
                connect dEMO_FAS_Distance_Treshold_Med.out1 -> multiportSwitch.in3;
                connect multiportSwitch.out1 -> mul.in2;
                connect v_Vehicle_msIn1 -> mul.in1;
                connect v_Obj_rel_kmhIn4 -> lookUpTable3.in1;
                connect v_Obj_rel_kmhIn4 -> lookUpTable1.in1;
                connect v_Obj_rel_kmhIn4 -> lookUpTable4.in1;
                connect mul.out1 -> sum.in2;
                connect sum.out1 -> lookUpTable2.in1;
                connect sum.out1 -> lookUpTable.in1;
                connect lookUpTable1.out1 -> mul1.in2;
                connect mul1.brakeForce_Distronic_pcOut1 -> switchBlock.elseIn;
              }
              <<Type="SubSystem">> component Distronic_FTS_Enabled {
                ports
                  in Double distance_Object_mIn1,
                  in Double fTS_Abstand_soll_mIn2,
                  in Double v_Obj_rel_kmhIn3,
                  out Double brakeForce_Distronic_pcOut1,
                  out Double decelerator_pcOut2;
                <<Type="Constant",Value="100">> component Constant {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="2.5">> component Constant1 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="100">> component Constant2 {
                  ports
                    out Double decelerator_pcOut1;
                }
                <<BreakpointsForDimension1="[0,2, 10,20]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[100,100, 10,0]",BreakpointsSpecification="Explicit values">> component LookUpTable {
                  ports
                    in Double in1,
                    out Double brakeForce_Distronic_pcOut1;
                  effect in1 -> brakeForce_Distronic_pcOut1;
                }
                <<BreakpointsForDimension1="[-1,0,10]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[0,0.15,1]",BreakpointsSpecification="Explicit values">> component LookUpTable1 {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="Product",Inputs="**">> component Mul1 {
                  ports
                    in Double brakeForce_Distronic_pcIn1,
                    in Double in2,
                    out Double brakeForce_Distronic_pcOut1;
                  effect brakeForce_Distronic_pcIn1 -> brakeForce_Distronic_pcOut1;
                  effect in2 -> brakeForce_Distronic_pcOut1;
                }
                <<Operator="<",Type="RelationalOperator">> component RelOp {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Sum",ListOfSigns="+-">> component Sum {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double brakeForce_Distronic_pcOut1;
                  effect ifIn -> brakeForce_Distronic_pcOut1;
                  effect condition -> brakeForce_Distronic_pcOut1;
                  effect elseIn -> brakeForce_Distronic_pcOut1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Constant constant;
                component Constant1 constant1;
                component Constant2 constant2;
                component LookUpTable lookUpTable;
                component LookUpTable1 lookUpTable1;
                component Mul1 mul1;
                component RelOp relOp;
                component Sum sum;
                component SwitchBlock switchBlock;
                component VERSION_INFO vERSION_INFO;
                component Condition condition;
                connect condition.out1 -> switchBlock.condition;
                connect relOp.out1 -> condition.in1;
                connect constant2.decelerator_pcOut1 -> decelerator_pcOut2;
                connect constant1.out1 -> relOp.in2;
                connect mul1.brakeForce_Distronic_pcOut1 -> switchBlock.elseIn;
                connect constant.out1 -> switchBlock.ifIn;
                connect lookUpTable.brakeForce_Distronic_pcOut1 -> mul1.brakeForce_Distronic_pcIn1;
                connect lookUpTable1.out1 -> mul1.in2;
                connect v_Obj_rel_kmhIn3 -> lookUpTable1.in1;
                connect fTS_Abstand_soll_mIn2 -> sum.in2;
                connect sum.out1 -> lookUpTable.in1;
                connect distance_Object_mIn1 -> sum.in1;
                connect distance_Object_mIn1 -> relOp.in1;
                connect switchBlock.brakeForce_Distronic_pcOut1 -> brakeForce_Distronic_pcOut1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              <<Type="Switch">> component SwitchBlock {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="u1 & u2 & ~u3",Type="Condition">> component Condition {
                ports
                  in Double in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock1 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="u1 & u2 & u3 ",Type="Condition">> component Condition1 {
                ports
                  in Double in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock2 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="u1 & ~u2",Type="Condition">> component Condition2 {
                ports
                  in Double in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock3 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="!(u1 & u2 & ~u3)&&!(u1 & u2 & u3 )&&!(u1 & ~u2)",Type="Condition">> component Condition3 {
                ports
                  in Double in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
              }
              <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                ports
                  in Double valueIn,
                  out Double valueOut;
              }
              <<Type="Switch">> component SwitchBlock4 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="u1 & u2 & ~u3",Type="Condition">> component Condition4 {
                ports
                  in Double in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock5 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="u1 & u2 & u3 ",Type="Condition">> component Condition5 {
                ports
                  in Double in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock6 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="u1 & ~u2",Type="Condition">> component Condition6 {
                ports
                  in Double in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock7 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="!(u1 & u2 & ~u3)&&!(u1 & u2 & u3 )&&!(u1 & ~u2)",Type="Condition">> component Condition7 {
                ports
                  in Double in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
              }
              <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                ports
                  in Double valueIn,
                  out Double valueOut;
              }
              component Constant constant;
              component Distronic_Deactive distronic_Deactive;
              component Distronic_Disabled distronic_Disabled;
              component Distronic_Enabled distronic_Enabled;
              component Distronic_FTS_Enabled distronic_FTS_Enabled;
              component VERSION_INFO vERSION_INFO;
              component SwitchBlock switchBlock;
              component Condition condition;
              component SwitchBlock1 switchBlock1;
              component Condition1 condition1;
              component SwitchBlock2 switchBlock2;
              component Condition2 condition2;
              component SwitchBlock3 switchBlock3;
              component Condition3 condition3;
              component UnitDelay unitDelay;
              component SwitchBlock4 switchBlock4;
              component Condition4 condition4;
              component SwitchBlock5 switchBlock5;
              component Condition5 condition5;
              component SwitchBlock6 switchBlock6;
              component Condition6 condition6;
              component SwitchBlock7 switchBlock7;
              component Condition7 condition7;
              component UnitDelay1 unitDelay1;
              connect switchBlock.out1 -> brakeForce_Distronic_pcOut1;
              connect switchBlock4.out1 -> decelerator_pcOut2;
              connect constant.out1 -> condition.in1;
              connect cC_active_bIn1 -> condition.in2;
              connect fTS_active_bIn6 -> condition.in3;
              connect condition.out1 -> switchBlock.condition;
              connect distronic_Enabled.brakeForce_Distronic_pcOut1 -> switchBlock.ifIn;
              connect constant.out1 -> condition1.in1;
              connect cC_active_bIn1 -> condition1.in2;
              connect fTS_active_bIn6 -> condition1.in3;
              connect condition1.out1 -> switchBlock1.condition;
              connect distronic_FTS_Enabled.brakeForce_Distronic_pcOut1 -> switchBlock1.ifIn;
              connect switchBlock1.out1 -> switchBlock.elseIn;
              connect constant.out1 -> condition2.in1;
              connect cC_active_bIn1 -> condition2.in2;
              connect fTS_active_bIn6 -> condition2.in3;
              connect condition2.out1 -> switchBlock2.condition;
              connect distronic_Disabled.brakeForce_Distronic_pcOut1 -> switchBlock2.ifIn;
              connect switchBlock2.out1 -> switchBlock1.elseIn;
              connect constant.out1 -> condition3.in1;
              connect cC_active_bIn1 -> condition3.in2;
              connect fTS_active_bIn6 -> condition3.in3;
              connect condition3.out1 -> switchBlock3.condition;
              connect distronic_Deactive.brakeForce_Distronic_pcOut1 -> switchBlock3.ifIn;
              connect switchBlock3.out1 -> switchBlock2.elseIn;
              connect switchBlock.out1 -> unitDelay.valueIn;
              connect unitDelay.valueOut -> switchBlock3.elseIn;
              connect constant.out1 -> condition4.in1;
              connect cC_active_bIn1 -> condition4.in2;
              connect fTS_active_bIn6 -> condition4.in3;
              connect condition4.out1 -> switchBlock4.condition;
              connect distronic_Enabled.decelerator_pcOut2 -> switchBlock4.ifIn;
              connect constant.out1 -> condition5.in1;
              connect cC_active_bIn1 -> condition5.in2;
              connect fTS_active_bIn6 -> condition5.in3;
              connect condition5.out1 -> switchBlock5.condition;
              connect distronic_FTS_Enabled.decelerator_pcOut2 -> switchBlock5.ifIn;
              connect switchBlock5.out1 -> switchBlock4.elseIn;
              connect constant.out1 -> condition6.in1;
              connect cC_active_bIn1 -> condition6.in2;
              connect fTS_active_bIn6 -> condition6.in3;
              connect condition6.out1 -> switchBlock6.condition;
              connect distronic_Disabled.decelerator_pcOut2 -> switchBlock6.ifIn;
              connect switchBlock6.out1 -> switchBlock5.elseIn;
              connect constant.out1 -> condition7.in1;
              connect cC_active_bIn1 -> condition7.in2;
              connect fTS_active_bIn6 -> condition7.in3;
              connect condition7.out1 -> switchBlock7.condition;
              connect distronic_Deactive.decelerator_pcOut2 -> switchBlock7.ifIn;
              connect switchBlock7.out1 -> switchBlock6.elseIn;
              connect switchBlock4.out1 -> unitDelay1.valueIn;
              connect unitDelay1.valueOut -> switchBlock7.elseIn;
              connect fTS_Abstand_soll_mIn7 -> distronic_FTS_Enabled.fTS_Abstand_soll_mIn2;
              connect distance_statIn4 -> distronic_Enabled.distance_statIn3;
              connect v_Vehicle_msIn2 -> distronic_Enabled.v_Vehicle_msIn1;
              connect v_Obj_rel_kmhIn5 -> distronic_Enabled.v_Obj_rel_kmhIn4;
              connect v_Obj_rel_kmhIn5 -> distronic_FTS_Enabled.v_Obj_rel_kmhIn3;
              connect distance_Object_mIn3 -> distronic_Enabled.distance_Object_mIn2;
              connect distance_Object_mIn3 -> distronic_FTS_Enabled.distance_Object_mIn1;
            }
            <<Type="SubSystem">> component EmergencyBrake {
              ports
                in Double v_Vehicle_msIn1,
                in Double distance_Object_mIn2,
                in Double v_Obj_rel_msIn3,
                out Double brakeForce_Emergency_pcOut1,
                out Boolean acusti_warn_bOut2;
              <<Type="Constant",Value="1">> component Constant {
                ports
                  out Double out1;
              }
              <<Type="SubSystem">> component EmergencyBrake_Function {
                ports
                  in Double v_Vehicle_msIn1,
                  in Double distance_Object_mIn2,
                  in Double v_Obj_rel_msIn3,
                  out Double brakeForce_Emergency_pcOut1,
                  out Boolean acusti_warn_bOut2;
                <<Type="Constant",Value="0">> component Constant1 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="0">> component Constant2 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="40">> component DEMO_FAS_Emerg_Brake_Force1 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="80">> component DEMO_FAS_Emerg_Brake_Force2 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="1">> component DEMO_FAS_Emerg_Brake_Time1 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="1.5">> component DEMO_FAS_Emerg_Brake_Time12 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="8">> component DEMO_FAS_Neg_Max_ms {
                  ports
                    out Double out1;
                }
                <<Type="Product",Inputs="*/">> component Div {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Product",Inputs="*/">> component Div1 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Function="max",Type="MinMax">> component MinMax {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double brakeForce_Emergency_pcOut1;
                  effect in1 -> brakeForce_Emergency_pcOut1;
                  effect in2 -> brakeForce_Emergency_pcOut1;
                }
                <<Operator="<",Type="RelationalOperator">> component RelOp {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean acusti_warn_bOut1;
                  effect in1 -> acusti_warn_bOut1;
                  effect in2 -> acusti_warn_bOut1;
                }
                <<Operator="<",Type="RelationalOperator">> component RelOp1 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Saturate",LowerLimit="1",UpperLimit="inf">> component Saturation1 {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="Saturate",LowerLimit="1",UpperLimit="inf">> component Saturation2 {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="Saturate",LowerLimit="1",UpperLimit="inf">> component Saturation3 {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="Sum",ListOfSigns="-+">> component Sum1 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Sum",ListOfSigns="-+">> component Sum2 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Constant1 constant1;
                component Constant2 constant2;
                component DEMO_FAS_Emerg_Brake_Force1 dEMO_FAS_Emerg_Brake_Force1;
                component DEMO_FAS_Emerg_Brake_Force2 dEMO_FAS_Emerg_Brake_Force2;
                component DEMO_FAS_Emerg_Brake_Time1 dEMO_FAS_Emerg_Brake_Time1;
                component DEMO_FAS_Emerg_Brake_Time12 dEMO_FAS_Emerg_Brake_Time12;
                component DEMO_FAS_Neg_Max_ms dEMO_FAS_Neg_Max_ms;
                component Div div;
                component Div1 div1;
                component MinMax minMax;
                component RelOp relOp;
                component RelOp1 relOp1;
                component Saturation1 saturation1;
                component Saturation2 saturation2;
                component Saturation3 saturation3;
                component Sum1 sum1;
                component Sum2 sum2;
                component SwitchBlock switchBlock;
                component SwitchBlock1 switchBlock1;
                component VERSION_INFO vERSION_INFO;
                component Condition condition;
                component Condition1 condition1;
                connect condition1.out1 -> switchBlock1.condition;
                connect condition.out1 -> switchBlock.condition;
                connect relOp.acusti_warn_bOut1 -> condition.in1;
                connect relOp1.out1 -> condition1.in1;
                connect saturation3.out1 -> relOp1.in1;
                connect saturation2.out1 -> relOp.in1;
                connect saturation1.out1 -> div1.in2;
                connect div1.out1 -> sum2.in2;
                connect div1.out1 -> sum1.in2;
                connect minMax.brakeForce_Emergency_pcOut1 -> brakeForce_Emergency_pcOut1;
                connect switchBlock1.out1 -> minMax.in2;
                connect switchBlock.out1 -> minMax.in1;
                connect constant2.out1 -> switchBlock1.elseIn;
                connect constant1.out1 -> switchBlock.elseIn;
                connect dEMO_FAS_Emerg_Brake_Force2.out1 -> switchBlock1.ifIn;
                connect dEMO_FAS_Emerg_Brake_Force1.out1 -> switchBlock.ifIn;
                connect relOp.acusti_warn_bOut1 -> acusti_warn_bOut2;
                connect sum1.out1 -> saturation3.in1;
                connect sum2.out1 -> saturation2.in1;
                connect dEMO_FAS_Emerg_Brake_Time12.out1 -> sum1.in1;
                connect dEMO_FAS_Emerg_Brake_Time1.out1 -> sum2.in1;
                connect v_Obj_rel_msIn3 -> saturation1.in1;
                connect distance_Object_mIn2 -> div1.in1;
                connect div.out1 -> relOp1.in2;
                connect div.out1 -> relOp.in2;
                connect dEMO_FAS_Neg_Max_ms.out1 -> div.in2;
                connect v_Vehicle_msIn1 -> div.in1;
              }
    component EmergencyBrake_Function emergencyBrake_Function;
}
            component BrakeAssistant brakeAssistant;
            component Distancewarner distancewarner;
            component Distronic distronic;
            component EmergencyBrake emergencyBrake;

            }
    component DEMO_FAS_Funktion dEMO_FAS_Funktion;
}
    component DEMO_FAS dEMO_FAS;
}
    component Subsystem subsystem;
}
    component DEMO_FAS dEMO_FAS;
}
