/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.TagLatencyTagSchema;

tags TagLatency for fas.daimlerPublicDemonstrator_ADAS_v04.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat {
	tag dEMO_FAS_Tempomat with LatencyCmpInst = 0 ns;

	tag tempomat_Function with LatencyCmpInst = 112160 ns;

	tag tempomat_Function.cC_Enabled with LatencyCmpInst = 1440 ns;
	tag tempomat_Function.cC_Enabled.ifBlock with LatencyCmpInst = 800 ns;
	tag tempomat_Function.cC_Enabled.merge with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_Enabled.tempomat_Active with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_Enabled.tempomat_Active.sum with LatencyCmpInst = 640 ns;

	tag tempomat_Function.cC_Enabled.tempomat_Active.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_Enabled.tempomat_Active.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_Enabled.tempomat_Deactive with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_Enabled.tempomat_Deactive.constant with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_Enabled.tempomat_Deactive.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_Enabled.tempomat_Deactive.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_Enabled.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_Enabled.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_On_Off with LatencyCmpInst = 20960 ns;
	tag tempomat_Function.cC_On_Off.constant with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.constant1 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.constant2 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.constant3 with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_On_Off.edgeFalling with LatencyCmpInst = 3760 ns;
	tag tempomat_Function.cC_On_Off.edgeFalling.logOp_A with LatencyCmpInst = 1360 ns;
	tag tempomat_Function.cC_On_Off.edgeFalling.logOp_N with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.edgeFalling.memory_U with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.edgeFalling.switch_R with LatencyCmpInst = 2400 ns;


	tag tempomat_Function.cC_On_Off.edgeRising1 with LatencyCmpInst = 4720 ns;
	tag tempomat_Function.cC_On_Off.edgeRising1.logOp_A with LatencyCmpInst = 1360 ns;
	tag tempomat_Function.cC_On_Off.edgeRising1.logOp_N with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.edgeRising1.memory_U with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.edgeRising1.switch_R with LatencyCmpInst = 2400 ns;


	tag tempomat_Function.cC_On_Off.false1 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.false1.zero with LatencyCmpInst = 0 ns;


	tag tempomat_Function.cC_On_Off.false2 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.false2.zero with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_On_Off.logicalOperator with LatencyCmpInst = 2000 ns;
	tag tempomat_Function.cC_On_Off.logicalOperator1 with LatencyCmpInst = 1360 ns;
	tag tempomat_Function.cC_On_Off.logicalOperator2 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.logicalOperator3 with LatencyCmpInst = 1200 ns;
	tag tempomat_Function.cC_On_Off.logicalOperator4 with LatencyCmpInst = 1360 ns;
	tag tempomat_Function.cC_On_Off.logicalOperator5 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.logicalOperator6 with LatencyCmpInst = 1200 ns;
	tag tempomat_Function.cC_On_Off.logicalOperator7 with LatencyCmpInst = 960 ns;

	tag tempomat_Function.cC_On_Off.rSFlipFlop with LatencyCmpInst = 10560 ns;
	tag tempomat_Function.cC_On_Off.rSFlipFlop.logOp_N with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.rSFlipFlop.memory_Q with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.rSFlipFlop.one_S with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_On_Off.rSFlipFlop.switch_R with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_On_Off.rSFlipFlop.switch_S with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_On_Off.rSFlipFlop.zero_R with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_On_Off.relOp with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.relOp1 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.relOp2 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.relOp3 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_On_Off.terminator with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_On_Off.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_On_Off.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue with LatencyCmpInst = 89760 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue with LatencyCmpInst = 63600 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater with LatencyCmpInst = 44880 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.constant with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.constant1 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.constant2 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.constant3 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.constant4 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.constant5 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.constant6 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.constant7 with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE with LatencyCmpInst = 14240 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.logOp_C with LatencyCmpInst = 1360 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.memory_C with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.one_C with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.relOp_C with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.relOp_Y with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.sum_C with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.switch_C with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.switch_R with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.zero_C with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.countDown_RE.zero_Y with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.dEMO_FAS_Repeater_Time with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.logicalOperator with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.logicalOperator1 with LatencyCmpInst = 1600 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.logicalOperator2 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.logicalOperator3 with LatencyCmpInst = 2000 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.relOp1 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.relOp2 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.relOp3 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.relOp4 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.switchBlock with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.switchBlock1 with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.switchBlock2 with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.switchBlock3 with LatencyCmpInst = 2400 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.sysInit with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.sysInit.memory_Init with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.sysInit.zero_Init with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.unitDelay with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;



	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinus with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinus.parameter with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinus.sum with LatencyCmpInst = 640 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinus.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinus.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2 with LatencyCmpInst = 7280 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2.constant1 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2.dEMO_FAS_CC_Lvl2_Round with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2.mathFunction with LatencyCmpInst = 1840 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2.sum with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2.switchBlock1 with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2.switchBlock3 with LatencyCmpInst = 2400 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValueMinusLvl2.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlus with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlus.parameter with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlus.sum with LatencyCmpInst = 640 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlus.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlus.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlusLvl2 with LatencyCmpInst = 3120 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlusLvl2.dEMO_FAS_CC_Lvl2_Round with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlusLvl2.mathFunction with LatencyCmpInst = 1840 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlusLvl2.sum with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlusLvl2.sum1 with LatencyCmpInst = 640 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlusLvl2.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_Repeater.v_SetValuePlusLvl2.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;





	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater with LatencyCmpInst = 17920 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.constant1 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.constant2 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.constant3 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.constant4 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.relOp1 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.relOp2 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.relOp3 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.relOp4 with LatencyCmpInst = 960 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;



	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinus with LatencyCmpInst = 3040 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinus.parameter with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinus.sum with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinus.switchBlock with LatencyCmpInst = 2400 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinus.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinus.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2 with LatencyCmpInst = 7280 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2.constant1 with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2.dEMO_FAS_CC_Lvl2_Round with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2.mathFunction with LatencyCmpInst = 1840 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2.sum with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2.switchBlock1 with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2.switchBlock3 with LatencyCmpInst = 2400 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValueMinusLvl2.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlus with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlus.parameter with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlus.sum with LatencyCmpInst = 640 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlus.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlus.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlusLvl2 with LatencyCmpInst = 3120 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlusLvl2.dEMO_FAS_CC_Lvl2_Round with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlusLvl2.mathFunction with LatencyCmpInst = 1840 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlusLvl2.sum with LatencyCmpInst = 640 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlusLvl2.sum1 with LatencyCmpInst = 640 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlusLvl2.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.cC_ChangeSetValue_Lvl2_no_Repeater.v_SetValuePlusLvl2.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.dEMO_FAS_Repeater with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.ifBlock with LatencyCmpInst = 800 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_ChangeSetValue.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_InitialSetValue with LatencyCmpInst = 1760 ns;

	tag tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;



	tag tempomat_Function.cC_SetValue.cC_InitialSetValue.constant with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_InitialSetValue.ifBlock with LatencyCmpInst = 800 ns;
	tag tempomat_Function.cC_SetValue.cC_InitialSetValue.relOp with LatencyCmpInst = 960 ns;

	tag tempomat_Function.cC_SetValue.cC_InitialSetValue.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_InitialSetValue.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_OnSet_SetValue with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_OnSet_SetValue.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_OnSet_SetValue.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.cC_SetValue.cC_StartUpSetValue with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.cC_StartUpSetValue.constant with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_StartUpSetValue.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.cC_StartUpSetValue.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;



	tag tempomat_Function.cC_SetValue.constant with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.edgeRising with LatencyCmpInst = 4720 ns;
	tag tempomat_Function.cC_SetValue.edgeRising.logOp_A with LatencyCmpInst = 1360 ns;
	tag tempomat_Function.cC_SetValue.edgeRising.logOp_N with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.edgeRising.memory_U with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.edgeRising.switch_R with LatencyCmpInst = 2400 ns;

	tag tempomat_Function.cC_SetValue.false with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.logicalOperator with LatencyCmpInst = 1360 ns;
	tag tempomat_Function.cC_SetValue.logicalOperator1 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.logicalOperator2 with LatencyCmpInst = 1760 ns;
	tag tempomat_Function.cC_SetValue.logicalOperator3 with LatencyCmpInst = 1200 ns;
	tag tempomat_Function.cC_SetValue.logicalOperator4 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.logicalOperator5 with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.logicalOperator6 with LatencyCmpInst = 960 ns;

	tag tempomat_Function.cC_SetValue.rSFlipFlop with LatencyCmpInst = 10560 ns;
	tag tempomat_Function.cC_SetValue.rSFlipFlop.logOp_N with LatencyCmpInst = 960 ns;
	tag tempomat_Function.cC_SetValue.rSFlipFlop.memory_Q with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.rSFlipFlop.one_S with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.rSFlipFlop.switch_R with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.rSFlipFlop.switch_S with LatencyCmpInst = 2400 ns;
	tag tempomat_Function.cC_SetValue.rSFlipFlop.zero_R with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.relOp with LatencyCmpInst = 960 ns;

	tag tempomat_Function.cC_SetValue.sysInit with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.sysInit.memory_Init with LatencyCmpInst = 0 ns;
	tag tempomat_Function.cC_SetValue.sysInit.zero_Init with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.terminator with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.cC_SetValue.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag tempomat_Function.vERSION_INFO with LatencyCmpInst = 0 ns;

	tag tempomat_Function.vERSION_INFO.copyright with LatencyCmpInst = 0 ns;




	tag vERSION_INFO with LatencyCmpInst = 0 ns;

	tag vERSION_INFO.copyright with LatencyCmpInst = 0 ns;


}
