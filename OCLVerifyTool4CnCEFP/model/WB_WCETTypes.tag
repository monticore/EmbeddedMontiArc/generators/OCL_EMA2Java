/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.TagLatencyTagSchema;
tags TagLatency for weatherBalloon {
	tag WeatherBalloonSensors with LatencyCmp = 2001 ms;
	tag WeatherBalloonSensors.Temperature with LatencyCmp = 501 ms;
	tag WeatherBalloonSensors.GPS with LatencyCmp = 1001 ms;

}
