/* (c) https://github.com/MontiCore/monticore */
package model;
conforms to nfp.CompPower;
tags CompPower for fas.daimlerPublicDemonstrator_ADAS_v04.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.distronic {
	tag distronic_Deactive with CompPowerInst = 0 nW;
	tag distronic_Disabled with CompPowerInst = 0 nW;
	tag distronic_Enabled with CompPowerInst = 16640 nW;
	tag distronic_Enabled.lookUpTable with CompPowerInst = 5000 nW; 
	tag distronic_Enabled.lookUpTable1 with CompPowerInst = 5000 nW; 
	tag distronic_Enabled.lookUpTable2 with CompPowerInst = 5000 nW; 
	tag distronic_Enabled.lookUpTable3 with CompPowerInst = 5000 nW; 
	tag distronic_Enabled.lookUpTable4 with CompPowerInst = 5000 nW; 
	tag distronic_Enabled.mul with CompPowerInst = 1120 nW; 
	tag distronic_Enabled.mul1 with CompPowerInst = 1120 nW; 
	tag distronic_Enabled.mul2 with CompPowerInst = 1120 nW; 
	tag distronic_Enabled.multiportSwitch with CompPowerInst = 5760 nW; 
	tag distronic_Enabled.relOp with CompPowerInst = 960 nW; 
	tag distronic_Enabled.sum with CompPowerInst = 640 nW; 
	tag distronic_Enabled.switchBlock with CompPowerInst = 3000 nW; 
	tag distronic_FTS_Enabled with CompPowerInst = 9760 nW;
	tag distronic_FTS_Enabled.lookUpTable with CompPowerInst = 5000 nW; 
	tag distronic_FTS_Enabled.lookUpTable1 with CompPowerInst = 5000 nW; 
	tag distronic_FTS_Enabled.mul1 with CompPowerInst = 1120 nW; 
	tag distronic_FTS_Enabled.relOp with CompPowerInst = 960 nW; 
	tag distronic_FTS_Enabled.sum with CompPowerInst = 640 nW; 
	tag distronic_FTS_Enabled.switchBlock with CompPowerInst = 3000 nW; 
	tag ifBlock with CompPowerInst = 4880 nW; 
}
