@rem (c) https://github.com/MontiCore/monticore  
@echo off
java -cp target\ocl2java-4.0.1-SNAPSHOT-jar-with-dependencies.jar de.monticore.lang.ocl.codegen.CheckEmbeddedMontiArc %~f1 %2 %~f3
